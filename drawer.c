#include <windows.h>
#include <defs.h>


//-------------------------------------------------------------------------
// Function declarations

signed int sub_10001000(int a1, int a2);
signed int sub_10001050(char a1, int a2, int a3, int a4, int a5, int *a6, int *a7);
signed int sub_100011A0(int *a1, int *a2, int *a3, int *a4);
int sub_10001260(int a1, int a2, int a3, int a4);
int sub_100012A0(int a1, int a2, int a3, int a4, int a5);
int sub_100014A0(int a1, int a2, int a3, int a4, unsigned int a5);
// void sub_10001830(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8);
signed int sub_10001A00(int *a1, uint32_t *a2, uint32_t *a3, uint32_t *a4, int *a5);
signed int *sub_10001C40(signed int *a1, signed int *a2, signed int *a3, signed int a4, signed int a5);
// signed int tjsf_drawLine(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, uint32_t *a10, int a11, int a12, int a13);
// signed int tjsf_drawAALine(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, uint32_t *a10, int a11, int a12, int a13);
// signed int tjsf_drawAATriangle(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, signed int *a11, int a12, int a13, int a14, int a15);
int sub_10002C50(int *this, int a2, int a3, int a4, int a5, int a6, int a7, unsigned int a8);

//-------------------------------------------------------------------------
// Data declarations

int tjsgdv_layer_buffer = 0; // weak
int tjsgdv_layer_height = 0; // weak
int tjsgdv_layer_width_minus_one = 0; // weak
int tjsgdv_layer_height_minus_one = 0; // weak
int tjsgdv_layer_width = 0; // weak
int tjsgdv_layer_pitch = 0; // weak
int dword_1000CF4C[5] = {0, 0, 0, 0, 0}; // weak

//----- (10001000) --------------------------------------------------------
signed int sub_10001000(int x, int y)
{
  signed int result; // eax

  result = 0;
  if ( x >= 0 )
  {
    if ( x > tjsgdv_layer_width_minus_one )
      result = 2;
  }
  else
  {
    result = 1;
  }
  if ( y >= 0 )
  {
    if ( y > tjsgdv_layer_height_minus_one )
      result += 8;
  }
  else
  {
    result += 4;
  }
  return result;
}

//----- (10001050) --------------------------------------------------------
signed int sub_10001050(char a1, int x1, int y1, int x2, int y2, int *x_out, int *y_out)
{
  int v7; // eax
  signed int result; // eax
  int v9; // eax
  int v10; // eax
  int v11; // eax

  if ( a1 & 1 && (v7 = y1 + (y2 - y1) * (0 - x1) / (x2 - x1), v7 >= 0) && v7 <= tjsgdv_layer_height_minus_one )
  {
    x_out[0] = 0;
    y_out[0] = v7;
    result = 1;
  }
  else if ( a1 & 2
         && (v9 = y1 + (y2 - y1) * (tjsgdv_layer_width_minus_one - x1) / (x2 - x1), v9 >= 0)
         && v9 <= tjsgdv_layer_height_minus_one )
  {
    x_out[0] = tjsgdv_layer_width_minus_one;
    y_out[0] = v9;
    result = 1;
  }
  else if ( a1 & 4
         && (v10 = x1 + (x2 - x1) * (0 - y1) / (y2 - y1), v10 >= 0)
         && v10 <= tjsgdv_layer_width_minus_one )
  {
    x_out[0] = v10;
    y_out[0] = 0;
    result = 1;
  }
  else if ( a1 & 8
         && (v11 = x1 + (x2 - x1) * (tjsgdv_layer_height_minus_one - y1) / (y2 - y1), v11 >= 0)
         && v11 <= tjsgdv_layer_width_minus_one )
  {
    x_out[0] = v11;
    y_out[0] = tjsgdv_layer_height_minus_one;
    result = 1;
  }
  else
  {
    result = -1;
  }
  return result;
}

//----- (100011A0) --------------------------------------------------------
signed int sub_100011A0(int *x1, int *y1, int *x2, int *y2)
{
  signed int v5; // esi
  signed int result; // eax
  signed int v7; // [esp+18h] [ebp+8h]

  v5 = sub_10001000(x1[0], y1[0]);
  result = sub_10001000(x2[0], y2[0]);
  v7 = result;
  if ( v5 || result )
  {
    if ( result & v5 )
      return -1;
    if ( v5 )
    {
      if ( sub_10001050(v5, x1[0], y1[0], x2[0], y2[0], x1, y1) < 0 )
        return -1;
      result = v7;
    }
    if ( result && sub_10001050(result, x1[0], y1[0], x2[0], y2[0], x2, y2) < 0 )
      result = -1;
    else
      result = 1;
  }
  return result;
}

//----- (10001260) --------------------------------------------------------
void sub_10001260(int a1, int a2, int a3, int a4)
{
  tjsgdv_layer_buffer = a1;
  tjsgdv_layer_pitch = a2;
  tjsgdv_layer_width = a3;
  tjsgdv_layer_height = a4;
  tjsgdv_layer_width_minus_one = a3 - 1;
  tjsgdv_layer_height_minus_one = a4 - 1;
}

//----- (100012A0) --------------------------------------------------------
int sub_100012A0(int x1, int y1, int x2, int y2, int color)
{
  int v5; // ecx
  int v6; // ebp
  int v7; // edi
  int v8; // edx
  int result; // eax
  int v11; // esi
  int v12; // edx
  int v13; // ecx
  int v14; // esi
  int v15; // edx
  int v16; // edi
  int v17; // edx
  int v18; // ecx
  int v19; // esi
  int v20; // ebp
  int v21; // ebp
  bool v22; // zf
  signed int v23; // edx
  int v24; // [esp+0h] [ebp-18h]
  int v25; // [esp+0h] [ebp-18h]
  signed int v26; // [esp+4h] [ebp-14h]
  signed int v27; // [esp+8h] [ebp-10h]
  int v28; // [esp+Ch] [ebp-Ch]
  int v29; // [esp+Ch] [ebp-Ch]
  int v30; // [esp+10h] [ebp-8h]
  int v31; // [esp+10h] [ebp-8h]
  int v32; // [esp+14h] [ebp-4h]
  int v33; // [esp+14h] [ebp-4h]
  int v34; // [esp+20h] [ebp+8h]

  v27 = x2 <= x1 ? -1 : 1;
  if ( x2 <= x1 )
    v5 = x1 - x2;
  else
    v5 = x2 - x1;
  v6 = y1;
  v7 = y2;
  v24 = v5;
  v26 = y2 <= y1 ? -1 : 1;
  if ( y2 <= y1 )
    v8 = y1 - y2;
  else
    v8 = y2 - y1;
  result = tjsgdv_layer_pitch >> 2;
  v28 = v8;
  if ( v5 < v8 )
  {
    v14 = -v8;
    v15 = v8 + 1;
    v25 = v14;
    v31 = v15;
    v16 = v15 >> 1;
    if ( v15 >> 1 )
    {
      v33 = 2 * v5;
      v17 = y2 * result;
      v18 = y1 * result;
      v19 = v26 * result;
      v34 = v26 * v16 + y1;
      for (size_t i = 0; i < v16; i += 1)
      {
        v20 = v18 + x1;
        v18 += v19;
        ((uint32_t *)(tjsgdv_layer_buffer + 4 * v20))[0] = color;
        v21 = v17 + x2;
        v17 -= v19;
        ((uint32_t *)(tjsgdv_layer_buffer + 4 * v21))[0] = color;
        v25 += v33;
        if ( v25 >= 0 )
        {
          x1 += v27;
          x2 -= v27;
          v25 -= 2 * v28;
        }
      }
      v6 = v34;
      v15 = v31;
    }
    v23 = v15 & 0x80000001;
    v22 = v23 == 0;
    if ( v23 < 0 )
      v22 = (((uint8_t)v23 - 1) | 0xFFFFFFFE) == -1;
    if ( !v22 )
      goto LABEL_24;
  }
  else
  {
    v11 = -v5;
    v32 = v5 + 1;
    v29 = 2 * v8;
    v12 = v7 * result;
    v13 = y1 * result;
    v30 = v26 * result;
    for (size_t i = 0; i < ((v5 + 1) >> 1); i += 1)
    {
      ((uint32_t *)(tjsgdv_layer_buffer + 4 * (v13 + x1)))[0] = color;
      ((uint32_t *)(tjsgdv_layer_buffer + 4 * (v12 + x2)))[0] = color;
      x1 += v27;
      x2 -= v27;
      v6 = y1;
      v11 += v29;
      if ( v11 >= 0 )
      {
        v6 = v26 + y1;
        v13 += v30;
        v12 -= v30;
        y1 += v26;
        v11 -= 2 * v24;
      }
    }
    if ( v32 % 2 )
    {
LABEL_24:
      result = x1 + v6 * result;
      ((uint32_t *)(tjsgdv_layer_buffer + 4 * result))[0] = color;
      return result;
    }
  }
  return result;
}

//----- (100014A0) --------------------------------------------------------
int sub_100014A0(int x1, int y1, int x2, int y2, unsigned int color)
{
  int v7; // edx
  int v8; // eax
  int v9; // ecx
  int v10; // ebx
  int v11; // esi
  int v12; // edi
  int v13; // edx
  int result; // eax
  int v15; // esi
  int v16; // edi
  int v17; // ebx
  int v18; // edx
  int v19; // esi
  int v20; // ecx
  int v21; // ecx
  int v23; // ebx
  int v24; // ecx
  uint16_t v25; // dx
  int v26; // edx
  int v27; // ecx
  int v28; // ecx
  int v29; // ebx
  int v30; // ecx
  int v31; // [esp+10h] [ebp-14h]
  int v33; // [esp+14h] [ebp-10h]
  int v34; // [esp+14h] [ebp-10h]
  int v35; // [esp+18h] [ebp-Ch]
  int v37; // [esp+1Ch] [ebp-8h]
  int v38; // [esp+20h] [ebp-4h]
  int v39; // [esp+28h] [ebp+4h]
  int v40; // [esp+28h] [ebp+4h]
  int v41; // [esp+2Ch] [ebp+8h]
  int v42; // [esp+2Ch] [ebp+8h]
  signed int v43; // [esp+30h] [ebp+Ch]
  int v44; // [esp+34h] [ebp+10h]
  int v45; // [esp+34h] [ebp+10h]
  int v46; // [esp+38h] [ebp+14h]
  int v47; // [esp+38h] [ebp+14h]

  v31 = tjsgdv_layer_pitch >> 2;
  if ( x2 <= x1 )
  {
    v39 = x1 - x2;
    v7 = x1 - x2;
  }
  else
  {
    v7 = x2 - x1;
    v39 = x2 - x1;
  }
  if ( y2 <= y1 )
    v8 = y1 - y2;
  else
    v8 = y2 - y1;
  v9 = x1 << 16;
  v10 = x2 << 16;
  v11 = y1 << 16;
  v12 = y2 << 16;
  v43 = v8;
  if ( v7 >= v8 )
  {
    if ( !v7 )
      v39 = 1;
    result = ((y2 << 16) - (y1 << 16)) / v39;
    v23 = ((v10 - v9) >> 31) | 1;
    v34 = y1 << 16;
    v24 = v9 >> 16;
    v37 = v23;
    v42 = v24;
    v38 = ((y2 << 16) - v11) / v39;
    if ( v39 )
    {
      v25 = color;
      v47 = (color >> 16) & 0xFF;
      result = v25 >> 8;
      v40 = (uint8_t)v25;
      for (size_t i = 0; i < v39; i += 1)
      {
        v26 = v24 + v31 * (v11 >> 16);
        v11 = (uint16_t)v11;
        v45 = v26 + v31;
        v27 = *(uint32_t *)(tjsgdv_layer_buffer + 4 * v26);
        ((uint32_t *)(tjsgdv_layer_buffer + 4 * v26))[0] = (((result + (v11 * (BYTE1(v27) - result) >> 16)) | ((((uint8_t)(v11 * ((unsigned int)(uint8_t)(*(uint32_t *)(tjsgdv_layer_buffer + 4 * v26) >> 16) - v47) >> 16)
                                                                                        + (uint8_t)v47) | 0xFFFFFF00) << 8)) << 8) | (v40 + (v11 * ((*(uint32_t *)(tjsgdv_layer_buffer + 4 * v26) & 0xFF) - v40) >> 16));
        v28 = ((uint32_t *)(tjsgdv_layer_buffer + 4 * (v26 + v31)))[0];
        v29 = (0xFFFF - (uint16_t)v11) * (BYTE1(v28) - result);
        LOBYTE(v26) = v47 + ((0xFFFF - (uint16_t)v11) * ((unsigned int)BYTE2(v28) - v47) >> 16);
        v30 = (0xFFFF - (uint16_t)v11) * ((uint8_t)v28 - v40);
        v11 = v38 + v34;
        *(uint32_t *)(tjsgdv_layer_buffer + 4 * v45) = (((result + (v29 >> 16)) | ((v26 | 0xFFFFFF00) << 8)) << 8) | (v40 + (v30 >> 16));
        v24 = v37 + v42;
        v42 += v37;
        v34 += v38;
      }
    }
  }
  else
  {
    if ( !v8 )
      v43 = 1;
    v44 = v9;
    v13 = ((v12 - (y1 << 16)) >> 31) | 1;
    v35 = (v10 - v9) / v43;
    result = v43;
    if ( v43 )
    {
      v15 = v31 * (int16_t)y1;
      v16 = (color >> 16) & 0xFF;
      result = (uint16_t)color >> 8;
      v17 = (uint8_t)color;
      v33 = v31 * v13;
      v46 = (color >> 16) & 0xFF;
      v41 = v31 * (int16_t)y1;
      for (size_t i = 0; i < v43; i += 1)
      {
        v18 = v15 + (v9 >> 16);
        v19 = (uint16_t)v9;
        v20 = *(uint32_t *)(tjsgdv_layer_buffer + 4 * v18);
        *(uint32_t *)(tjsgdv_layer_buffer + 4 * v18) = (((result + (v19 * (BYTE1(v20) - result) >> 16)) | ((((uint8_t)(v19 * ((unsigned int)(uint8_t)(*(uint32_t *)(tjsgdv_layer_buffer + 4 * v18) >> 16) - v16) >> 16)
                                                                                        + (uint8_t)v16) | 0xFFFFFF00) << 8)) << 8) | (v17 + (v19 * ((*(uint32_t *)(tjsgdv_layer_buffer + 4 * v18) & 0xFF) - v17) >> 16));
        v21 = *(uint32_t *)(tjsgdv_layer_buffer + 4 * v18 + 4);
        *(uint32_t *)(tjsgdv_layer_buffer + 4 * v18 + 4) = (((result + ((0xFFFF - v19) * (BYTE1(v21) - result) >> 16)) | ((((uint8_t)((0xFFFF - v19) * ((unsigned int)(uint8_t)(*(uint32_t *)(tjsgdv_layer_buffer + 4 * v18 + 4) >> 16) - v46) >> 16) + (uint8_t)v46) | 0xFFFFFF00) << 8)) << 8) | (v17 + ((0xFFFF - v19) * ((*(uint32_t *)(tjsgdv_layer_buffer + 4 * v18 + 4) & 0xFF) - v17) >> 16));
        v9 = v35 + v44;
        v15 = v33 + v41;
        v44 += v35;
        v41 += v33;
        v16 = v46;
      }
    }
  }
  return result;
}

//----- (10001830) --------------------------------------------------------
void sub_10001830(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8)
{
  //UPDATE: a3 a4 a7 (a8 - a4 + 1) (unknown; possible 0?)
}

//----- (10001A00) --------------------------------------------------------
signed int sub_10001A00(int *a1, uint32_t *a2, uint32_t *a3, uint32_t *a4, int *a5)
{
  char v19; // [esp+24h] [ebp-18h]

  if ( !a1 )
    return 0;
  ("tTJSVariant::tTJSVariant()")(&v19);
  if ( !FIND_AND_GET_MEMBER(L"imageWidth", a1, &v19) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.imageWidth failed.");
  }
  *a2 = ("tTJSVariant::operator tjs_int() const")(&v19);
  if ( !FIND_AND_GET_MEMBER(L"imageHeight", a1, &v19) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.imageHeight failed.");
  }
  *a3 = ("tTJSVariant::operator tjs_int() const")(&v19);
  if ( !FIND_AND_GET_MEMBER(L"mainImageBufferForWrite", a1, &v19) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.mainImageBufferForWrite failed.");
  }
  *a4 = ("tTJSVariant::operator tjs_int() const")(&v19);
  if ( !FIND_AND_GET_MEMBER(L"mainImageBufferPitch", a1, &v19) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.mainImageBufferPitch failed.");
  }
  *a5 = ("tTJSVariant::operator tjs_int() const")(&v19);
  ("tTJSVariant::~ tTJSVariant()")(&v19);
  return 1;
}

//----- (10001C40) --------------------------------------------------------
signed int *sub_10001C40(signed int *a1, signed int *a2, signed int *a3, signed int a4, signed int a5)
{
  signed int *result; // eax

  result = a3;
  if ( (signed int)a3 >= a4 )
  {
    if ( a4 >= a5 )
    {
      *a1 = a5;
      *a2 = (signed int)a3;
    }
    else
    {
      *a1 = a4;
      if ( (signed int)a3 >= a5 )
      {
        *a2 = (signed int)a3;
      }
      else
      {
        result = a2;
        *a2 = a5;
      }
    }
  }
  else
  {
    if ( (signed int)a3 >= a5 )
    {
      result = a1;
      *a1 = a5;
    }
    else
    {
      *a1 = (signed int)a3;
      if ( a4 < a5 )
      {
        result = a2;
        *a2 = a5;
        return result;
      }
    }
    *a2 = a4;
  }
  return result;
}

//----- (10002010) --------------------------------------------------------
signed int tjsf_drawLine(int this, int flag, int membername, int hint, int result, int numparams, uint32_t *param, int objthis, int a12, int a13)
{
  int *tjslv_layer; // ebx
  int tjsldv_layer_height; // [esp+1Ch] [ebp-10h]
  int tjsldv_layer_width; // [esp+20h] [ebp-Ch]
  int tjsldv_layer_pitch; // [esp+24h] [ebp-8h]
  int tjsldv_layer_buffer; // [esp+28h] [ebp-4h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( numparams < 6 )
    return TJS_E_BADPARAMCOUNT;
  tjslv_layer = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  int x1 = ("tTVInteger tTJSVariant::AsInteger() const")(param[1]);
  int y1 = ("tTVInteger tTJSVariant::AsInteger() const")(param[2]);
  int x2 = ("tTVInteger tTJSVariant::AsInteger() const")(param[3]);
  int y2 = ("tTVInteger tTJSVariant::AsInteger() const")(param[4]);
  int color = ("tTVInteger tTJSVariant::AsInteger() const")(param[5]);
  sub_10001A00(tjslv_layer, &tjsldv_layer_width, &tjsldv_layer_height, &tjsldv_layer_buffer, &tjsldv_layer_pitch);
  sub_10001260(tjsldv_layer_buffer, tjsldv_layer_pitch, tjsldv_layer_width, tjsldv_layer_height);
  if ( sub_100011A0(&x1, &y1, &x2, &y2) >= 0 )
    sub_100012A0(x1, y1, x2, y2, color);
  // UPDATE: tjslv_layer x1 y1 x2 y2
  return TJS_S_OK;
}

//----- (100021B0) --------------------------------------------------------
signed int tjsf_drawAALine(int a1, int a2, int a3, int this, int flag, int membername, int hint, int result, int numparams, uint32_t *param, int objthis, int a12, int a13)
{
  int *tjslv_layer; // ebx
  int tjsldv_layer_height; // [esp+1Ch] [ebp-10h]
  int tjsldv_layer_width; // [esp+20h] [ebp-Ch]
  int tjsldv_layer_pitch; // [esp+24h] [ebp-8h]
  int tjsldv_layer_buffer; // [esp+28h] [ebp-4h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  tjslv_layer = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  int x1 = ("tTVInteger tTJSVariant::AsInteger() const")(param[1]);
  int y1 = ("tTVInteger tTJSVariant::AsInteger() const")(param[2]);
  int x2 = ("tTVInteger tTJSVariant::AsInteger() const")(param[3]);
  int y2 = ("tTVInteger tTJSVariant::AsInteger() const")(param[4]);
  int color = ("tTVInteger tTJSVariant::AsInteger() const")(param[5]);
  sub_10001A00(tjslv_layer, &tjsldv_layer_width, &tjsldv_layer_height, &tjsldv_layer_buffer, &tjsldv_layer_pitch);
  sub_10001260(tjsldv_layer_buffer, tjsldv_layer_pitch, tjsldv_layer_width, tjsldv_layer_height);
  if ( sub_100011A0(&x1, &y1, &x2, &y2) >= 0 )
    sub_100014A0(x1, y1, x2, y2, color);
  // UPDATE: tjslv_layer x1 y1 x2 y2
  return TJS_S_OK;
}

//----- (10002350) --------------------------------------------------------
signed int tjsf_drawAATriangle(int a1, int a2, int a3, int a4, int this, int flag, int membername, int hint, int result, int numparams, signed int *param, int objthis, int a13, int a14, int a15)
{
  int v19; // eax
  int v22; // eax
  int v25; // eax
  int v28; // eax
  int v31; // eax
  int v38; // ebp
  unsigned int v40; // ebx
  int v49; // ST14_4
  signed int *v54; // [esp+20h] [ebp-1Ch]
  int *tjslv_layer; // [esp+24h] [ebp-18h]
  int tjsldv_layer_height; // [esp+28h] [ebp-14h]
  int tjsldv_layer_width; // [esp+2Ch] [ebp-10h]
  int tjsldv_layer_pitch; // [esp+30h] [ebp-Ch]
  int tjsldv_layer_buffer; // [esp+34h] [ebp-8h]
  int v60; // [esp+38h] [ebp-4h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v19 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  v22 = ("tTVInteger tTJSVariant::AsInteger() const")(param[1]);
  v25 = ("tTVInteger tTJSVariant::AsInteger() const")(param[2]);
  v28 = ("tTVInteger tTJSVariant::AsInteger() const")(param[3]);
  numparams = ("tTVInteger tTJSVariant::AsInteger() const")(param[4]);
  ("tTVInteger tTJSVariant::AsInteger() const")(param[5]);
  v38 = ("tTVInteger tTJSVariant::AsInteger() const")(param[6]);
  v40 = ("tTVInteger tTJSVariant::AsInteger() const")(param[7]);
  sub_10001A00(tjslv_layer, &tjsldv_layer_width, &tjsldv_layer_height, &tjsldv_layer_buffer, &tjsldv_layer_pitch);
  sub_10001260(tjsldv_layer_buffer, tjsldv_layer_pitch, tjsldv_layer_width, tjsldv_layer_height);
  if ( dword_1000CF4C[0] )
    free(dword_1000CF4C[0]);
  if ( dword_1000CF4C[1] )
    free(dword_1000CF4C[1]);
  dword_1000CF4C[0] = malloc(4 * tjsldv_layer_height);
  dword_1000CF4C[1] = malloc(4 * tjsldv_layer_height);
  sub_10002C50(dword_1000CF4C, (int)v54, (int)param, v31, membername, v60, v38, v40);
  sub_10001C40((signed int *)&v54, &v31, v54, v31, v60);
  sub_10001C40((signed int *)&param, &membername, param, membername, v38);
  sub_10001830(4 * tjsldv_layer_height, v60, (int)tjslv_layer, (int)v54, (int)param, v31, membername, v49);
  return TJS_S_OK;
}

//----- (100026C0) --------------------------------------------------------
int sub_100026C0(int *this, int a2, int a3, int a4, int a5, int a6, int a7, unsigned int a8)
{
  int v8; // ebx
  int v9; // ebp
  int v10; // esi
  int v11; // edx
  int v12; // eax
  int v13; // ebp
  int v14; // esi
  int v15; // ecx
  int v16; // eax
  int v17; // esi
  int v18; // ecx
  int v19; // edi
  int v20; // eax
  bool v21; // sf
  int result; // eax
  int v24; // eax
  int v25; // ecx
  int v26; // edx
  int v27; // ebp
  int v28; // eax
  int v29; // ecx
  int v30; // edi
  int v31; // eax
  int v32; // ST1C_4
  int v33; // eax
  int v35; // eax
  int v37; // edx
  int v38; // ebp
  int v39; // eax
  int v40; // ecx
  int v41; // eax
  int v42; // edi
  int v43; // esi
  int v44; // eax
  int v45; // edi
  int v46; // ebp
  int v47; // esi
  int v48; // edx
  int v49; // eax
  int v50; // ecx
  int v51; // eax
  int v52; // esi
  int v53; // edx
  int v54; // eax
  int v55; // ecx
  int v56; // eax
  int v57; // [esp+10h] [ebp-1Ch]
  int v58; // [esp+10h] [ebp-1Ch]
  int v59; // [esp+14h] [ebp-18h]
  int v60; // [esp+18h] [ebp-14h]
  int *v61; // [esp+18h] [ebp-14h]
  int *v62; // [esp+18h] [ebp-14h]
  int v63; // [esp+1Ch] [ebp-10h]
  int v64; // [esp+1Ch] [ebp-10h]
  int v65; // [esp+20h] [ebp-Ch]
  int v66; // [esp+24h] [ebp-8h]
  int v67; // [esp+28h] [ebp-4h]
  int v68; // [esp+30h] [ebp+4h]
  int v69; // [esp+30h] [ebp+4h]
  int v70; // [esp+30h] [ebp+4h]
  int v71; // [esp+30h] [ebp+4h]
  int v72; // [esp+34h] [ebp+8h]
  int v73; // [esp+34h] [ebp+8h]
  int v74; // [esp+34h] [ebp+8h]
  int v75; // [esp+34h] [ebp+8h]
  int v76; // [esp+34h] [ebp+8h]
  int v77; // [esp+38h] [ebp+Ch]
  int v78; // [esp+38h] [ebp+Ch]
  int v79; // [esp+3Ch] [ebp+10h]
  int v80; // [esp+3Ch] [ebp+10h]
  int v81; // [esp+40h] [ebp+14h]
  int v82; // [esp+40h] [ebp+14h]
  int v85; // [esp+44h] [ebp+18h]
  int v86; // [esp+44h] [ebp+18h]
  int v87; // [esp+48h] [ebp+1Ch]
  int v88; // [esp+48h] [ebp+1Ch]
  int v89; // [esp+48h] [ebp+1Ch]
  int v90; // [esp+48h] [ebp+1Ch]

  v59 = this[0];
  v60 = this[1];
  v8 = tjsldv_layer_buffer;
  v9 = a2;
  v10 = a4;
  v63 = tjsldv_layer_buffer;
  v57 = tjsldv_layer_pitch / 4;
  if ( a4 <= a2 )
  {
    v68 = a2 - a4;
    v11 = v9 - a4;
  }
  else
  {
    v11 = a4 - a2;
    v68 = a4 - a2;
  }
  if ( a5 <= a3 )
  {
    v12 = a3 - a5;
    v77 = a3 - a5;
  }
  else
  {
    v12 = a5 - a3;
    v77 = a5 - a3;
  }
  v13 = v9 << 16;
  v14 = v10 << 16;
  v15 = a3 << 16;
  v72 = a3 << 16;
  if ( v11 >= v12 )
  {
    if ( !v11 )
      v68 = 1;
    v43 = v14 - v13;
    v44 = (a5 << 16) - v15;
    v45 = v15;
    v46 = v13 >> 16;
    v78 = v15;
    v65 = (v43 >> 31) | 1;
    v80 = v44 / v68;
    result = (v15 >> 16) + (v44 / v68 * abs(a6 - v46) >> 16);
    if ( result <= a7 )
    {
      if ( v68 )
      {
        v52 = (a8 >> 16) & 0xFF;
        v53 = (uint16_t)a8 >> 8;
        v54 = (uint8_t)a8;
        v90 = (a8 >> 16) & 0xFF;
        v76 = v54;
        for (size_t i = 0; i < v68; i += 1)
        {
          v71 = v45 >> 16;
          v55 = v46 + v57 * (v45 >> 16);
          v56 = *(uint32_t *)(v8 + 4 * v55);
          v8 = v63;
          *(uint32_t *)(v63 + 4 * v55) = (((v53 + ((uint16_t)v45 * (BYTE1(v56) - v53) >> 16)) | ((((uint8_t)((uint16_t)v45 * ((unsigned int)BYTE2(v56) - v52) >> 16) + (uint8_t)v52) | 0xFFFFFF00) << 8)) << 8) | (v76 + ((uint16_t)v45 * ((uint8_t)v56 - v76) >> 16));
          if ( *(uint32_t *)(v59 + 4 * (v45 >> 16) + 4) < v46 )
            *(uint32_t *)(v59 + 4 * v71 + 4) = v46;
          if ( *(uint32_t *)(v60 + 4 * v71 + 4) > v46 )
            *(uint32_t *)(v60 + 4 * v71 + 4) = v46;
          v46 += v65;
          v45 = v80 + v78;
          result = i - 1;
          v78 += v80;
          v52 = v90;
        }
      }
    }
    else if ( v68 )
    {
      v47 = (a8 >> 16) & 0xFF;
      v48 = (uint16_t)a8 >> 8;
      v49 = (uint8_t)a8;
      v89 = (a8 >> 16) & 0xFF;
      v75 = v49;
      for (size_t i = 0; i < v68; i += 1)
      {
        v70 = v45 >> 16;
        v50 = v46 + v57 * ((v45 >> 16) + 1);
        v51 = *(uint32_t *)(v8 + 4 * v50);
        v8 = v63;
        *(uint32_t *)(v63 + 4 * v50) = (((v48 + ((0xFFFF - (uint16_t)v45) * (BYTE1(v51) - v48) >> 16)) | ((((uint8_t)((0xFFFF - (uint16_t)v45) * ((unsigned int)BYTE2(v51) - v47) >> 16) + (uint8_t)v47) | 0xFFFFFF00) << 8)) << 8) | (v75 + ((0xFFFF - (uint16_t)v45) * ((uint8_t)v51 - v75) >> 16));
        if ( *(uint32_t *)(v59 + 4 * (v45 >> 16)) < v46 )
          *(uint32_t *)(v59 + 4 * v70) = v46;
        if ( *(uint32_t *)(v60 + 4 * v70) > v46 )
          *(uint32_t *)(v60 + 4 * v70) = v46;
        v46 += v65;
        v45 = v80 + v78;
        result = i - 1;
        v78 += v80;
        v47 = v89;
      }
    }
  }
  else
  {
    if ( !v12 )
      v77 = 1;
    v16 = v14 - v13;
    v17 = v13;
    v69 = v13;
    v18 = (((a5 << 16) - v15) >> 31) | 1;
    v19 = v72 >> 16;
    v79 = v16 / v77;
    result = v77;
    if ( v77 )
    {
      v73 = (uint8_t)a8;
      v81 = v57 * v19;
      v24 = 4 * v18;
      v58 = v57 * v18;
      v25 = v60;
      v66 = v24;
      v61 = (int *)(v60 + 4 * v19);
      v26 = (a8 >> 16) & 0xFF;
      v27 = (uint16_t)a8 >> 8;
      v28 = v59 - v25;
      v29 = v81;
      v87 = (a8 >> 16) & 0xFF;
      v85 = v28;
      for (size_t i = 0; i < v77; v77 += 1)
      {
        v30 = v17 >> 16;
        v31 = v29 + (v17 >> 16) + 1;
        v32 = v31;
        v33 = *(uint32_t *)(v8 + 4 * v31);
        *(uint32_t *)(v8 + 4 * v32) = (((v27 + ((0xFFFF - (uint16_t)v17) * (BYTE1(v33) - v27) >> 16)) | ((((uint8_t)v26 + (uint8_t)((0xFFFF - (uint16_t)v17) * ((unsigned int)BYTE2(v33) - v26) >> 16)) | 0xFFFFFF00) << 8)) << 8) | (v73 + ((0xFFFF - (uint16_t)v17) * ((uint8_t)v33 - v73) >> 16));
        if ( *(int *)((char *)v61 + v85) < v17 >> 16 )
          *(int *)((char *)v61 + v85) = v30;
        if ( *v61 > v30 )
          *v61 = v30;
        v17 = v79 + v69;
        v61 = (int *)((char *)v61 + v66);
        v29 = v58 + v81;
        result = v77 - 1;
        v69 += v79;
        v81 += v58;
        v26 = v87;
      }
    }
  }
  return result;
}

//----- (10002C50) --------------------------------------------------------
int sub_10002C50(int *this, int a2, int a3, int a4, int a5, int a6, int a7, unsigned int a8)
{
  int v8; // edi
  int v10; // edx
  int v11; // ecx
  int v12; // ebx
  int *v13; // eax
  int v14; // edx
  int v16; // ebx
  int v17; // ebp
  int result; // eax
  int v19; // edx
  int v20; // esi
  int v21; // ecx
  int *v22; // ebx
  int v23; // eax
  int v24; // ecx
  int v25; // [esp+10h] [ebp-10h]
  int v26; // [esp+14h] [ebp-Ch]
  int v27; // [esp+18h] [ebp-8h]
  int v28; // [esp+1Ch] [ebp-4h]
  int v29; // [esp+28h] [ebp+8h]

  v8 = a7;
  if ( a3 >= a5 )
  {
    if ( a3 >= a7 )
    {
      v25 = a3;
      if ( a5 >= a7 )
        goto LABEL_10;
    }
    else
    {
      v25 = a7;
    }
    v8 = a5;
  }
  else if ( a5 >= a7 )
  {
    v25 = a5;
    if ( a3 < a7 )
      v8 = a3;
  }
  else
  {
    v25 = a7;
    v8 = a3;
  }
LABEL_10:
  v10 = this[0];
  v11 = this[1];
  v12 = tjsldv_layer_width - 1;
  v27 = v11;
  v28 = tjsldv_layer_width - 1;
  if ( v8 <= v25 )
  {
    v13 = (int *)(v11 + 4 * v8);
    v14 = v10 - v11;
    for (size_t i = 0; i < (v25 - v8); i += 1)
    {
      *(int *)((char *)v13 + v14) = 0;
      *v13 = v12;
      ++v13;
    }
  }
  v16 = (a6 + a2 + a4) / 3;
  v17 = (a7 + a5 + a3) / 3;
  sub_100026C0(this, a2, a3, a4, a5, v16, v17, a8);
  sub_100026C0(this, a4, a5, a6, a7, v16, v17, a8);
  sub_100026C0(this, a6, a7, a2, a3, v16, v17, a8);
  v29 = tjsldv_layer_buffer;
  result = v25;
  v19 = tjsldv_layer_pitch / 4;
  if ( v8 <= v25 )
  {
    v20 = v8 * v19;
    v21 = v27 - this[0];
    v22 = (int *)(this[0] + 4 * v8);
    for (size_t i = 0; i < (v25 - v8); i += 1)
    {
      v23 = *(int *)((char *)v22 + v21);
      v24 = *v22;
      if ( v23 < 0 )
        v23 = 0;
      if ( v24 > v28 )
        v24 = v28;
      if ( v23 <= v24 )
        memset32((void *)(v29 + 4 * (v20 + v23)), a8, v24 - v23 + 1);
      ++v22;
      v20 += v19;
      v21 = v27 - this[0];
      result = i;
    }
  }
  return result;
}

