#include <windows.h>
#include <defs.h>


//-------------------------------------------------------------------------
// Function declarations

int sub_10001000(int a1, int a2, int a3);
int sub_100011B0(int a1, int a2, int a3);
int sub_100013D0(int a1, int a2, int a3);
int sub_100016C0(int a1, int a2, int a3);
signed int tjsf_initSlideOpen(int a1, int a2, int a3, int a4, int a5, int a6, int *a7);
// signed int tjsf_drawSlideOpen(int a1, int a2, int a3, double a4, int a5, int a6, int a7, int a8, int a9, int a10, int *a11);
signed int tjsf_finishSlideOpen(int a1, int a2, int a3);
signed int sub_100024B0(int *a1, uint32_t *a2, uint32_t *a3, uint32_t *a4, int *a5);

//-------------------------------------------------------------------------
// Data declarations

int tjsgv_type = 0; // weak
int tjsgdv_dest_height = 0; // weak
int tjsgdv_src_buffer = 0; // weak
int tjsgdv_current_type = 0; // weak
int tjsgv_dest = 0; // weak
int tjsgv_open = 0; // weak
int tjsgdv_dest_width = 0; // weak
int tjsgdv_dest_buffer = 0; // weak
int tjsgdv_pitch = 0; // weak
int tjsgdv_dest_pitch = 0; // weak
LPVOID tjsgdv_buffer = NULL; // idb

//----- (10001000) --------------------------------------------------------
int sub_10001000(int a1, int dest_width, int dest_height)
{
  int v3; // ebp
  int result; // eax
  int v5; // esi
  int v6; // ecx
  uint32_t *v7; // edi
  int v8; // ebx
  int *v9; // edx
  int v10; // eax
  int v11; // edi
  uint32_t *v12; // edx
  int v13; // edx
  int v14; // edi
  int v15; // ecx
  bool v16; // zf
  int v17; // ebx
  uint32_t *v18; // edx
  int v19; // edi
  int *v20; // ecx
  int v21; // ebp
  int v22; // ecx
  int v23; // edx
  int v24; // edi
  int v25; // ecx
  int v26; // [esp+10h] [ebp-10h]
  int v27; // [esp+14h] [ebp-Ch]
  int v28; // [esp+18h] [ebp-8h]
  unsigned int v29; // [esp+1Ch] [ebp-4h]
  int v30; // [esp+24h] [ebp+4h]
  int v31; // [esp+2Ch] [ebp+Ch]
  int v32; // [esp+2Ch] [ebp+Ch]

  v26 = tjsgdv_src_buffer;
  v29 = (unsigned int)tjsgdv_dest_pitch >> 2;
  v3 = tjsgdv_pitch;
  result = dest_width / 2;
  v5 = tjsgdv_dest_buffer;
  v28 = dest_width / 2;
  if ( tjsgv_open )
  {
    if ( dest_height > 0 )
    {
      result -= a1;
      v31 = result;
      for (v27 = 0; v27 < dest_height; v27 += 1)
      {
        v6 = 0;
        v7 = (uint32_t *)v5;
        v9 = (int *)(v3 + 4 * a1);
        v6 = result;
        for (v8 = 0; v8 < result; v8 += 1)
        {
          v10 = *v9;
          ++v9;
          *v7 = v10;
          ++v7;
        }
        result = v31;
        v12 = (uint32_t *)(v5 + 4 * v6);
        v6 = v28 + a1;
        for (v11 = 0; v11 < (v28 + a1 - v6); v11 += 1)
        {
          *v12 = *(uint32_t *)((char *)v12 + v26 - v5);
          ++v12;
        }
        result = v31;
        v13 = 4 * v6;
        for (v14 = 0; v14 < (dest_width - v6); v14 += 1)
        {
          v15 = v13;
          v13 += 4;
          *(uint32_t *)(v13 + v5 - 4) = *(uint32_t *)(v15 - 4 * a1 + v3);
        }
        v3 += 4 * v29;
        v5 += 4 * v29;
        v26 += 4 * v29;
      }
    }
  }
  else if ( dest_height > 0 )
  {
    result = a1;
    v17 = dest_width / 2 - a1;
    v32 = v17;
    for (v30 = 0; v30 < dest_height; v30 += 1)
    {
      v18 = (uint32_t *)v5;
      v20 = (int *)(v26 + 4 * result);
      for (v19 = 0; v19 < v17; v19 += 1)
      {
        v21 = *v20;
        ++v20;
        *v18 = v21;
        ++v18;
      }
      v22 = v28 + result;
      if ( v28 + result < dest_width )
      {
        v23 = 4 * v22;
        for (v24 = 0; v24 < (dest_width - v22); v24 += 1)
        {
          v25 = v23 - 4 * result;
          v23 += 4;
          *(uint32_t *)(v23 + v5 - 4) = *(uint32_t *)(v25 + v26);
        }
        v17 = v32;
      }
      v5 += 4 * v29;
      v26 += 4 * v29;
    }
  }
  return result;
}

//----- (100011B0) --------------------------------------------------------
int sub_100011B0(int a1, int dest_width, int dest_height)
{
  int v3; // ecx
  int v4; // ebp
  int v5; // edx
  unsigned int v6; // esi
  int v7; // edi
  int v8; // ebx
  uint32_t *v9; // edx
  int v10; // edx
  int v11; // ebp
  int v12; // eax
  int v13; // edx
  uint32_t *v14; // edx
  int v15; // edi
  int v16; // edi
  int result; // eax
  int v18; // ebp
  int v19; // esi
  uint32_t *v20; // eax
  int v21; // ebx
  int v22; // edx
  int v23; // eax
  int v24; // edi
  uint32_t *v25; // edx
  int v26; // edx
  int v27; // edi
  int v28; // ebp
  uint32_t *v29; // eax
  int v30; // esi
  unsigned int v31; // [esp+10h] [ebp-10h]
  int v32; // [esp+14h] [ebp-Ch]
  int v33; // [esp+18h] [ebp-8h]
  int v34; // [esp+1Ch] [ebp-4h]
  int v35; // [esp+1Ch] [ebp-4h]
  int v36; // [esp+1Ch] [ebp-4h]
  int v37; // [esp+24h] [ebp+4h]
  int v38; // [esp+24h] [ebp+4h]

  v3 = tjsgdv_dest_buffer;
  v4 = a1;
  v5 = dest_height / 2;
  v6 = (unsigned int)tjsgdv_dest_pitch >> 2;
  v31 = (unsigned int)tjsgdv_dest_pitch >> 2;
  v33 = dest_height / 2;
  if ( a1 > dest_height / 2 )
  {
    v4 = dest_height / 2;
    a1 = dest_height / 2;
  }
  if ( tjsgv_open )
  {
    v37 = 0;
    v7 = v5 - v4;
    v32 = tjsgdv_pitch + 4 * v4 * v6;
    if ( v5 - v4 <= 0 )
    {
    }
    else
    {
      v37 = v5 - v4;
      for (v34 = 0; v34 < (v5 - v4); v34 += 1)
      {
        v9 = (uint32_t *)v3;
        for (v8 = 0; v8 < dest_width; v8 += 1)
        {
          *v9 = *(uint32_t *)((char *)v9 + v32 - v3);
          ++v9;
        }
        v6 = v31;
        v3 += 4 * v6;
        v32 += 4 * v6;
      }
      v5 = dest_height / 2;
    }
    v10 = v4 + v5;
    v11 = v37;
    v12 = tjsgdv_src_buffer + 4 * v6 * v7;
    if ( v37 < v10 )
    {
      v13 = v10 - v37;
      v11 = v13 + v37;
      v38 = v13 + v37;
      for (v35 = 0; v35 < v13; v35 += 1)
      {
        v14 = (uint32_t *)v3;
        for (v15 = 0; v15 < dest_width; v15 += 1)
        {
          *v14 = *(uint32_t *)((char *)v14 + v12 - v3);
          ++v14;
        }
        v6 = v31;
        v11 = v38;
        v12 += 4 * v6;
        v3 += 4 * v6;
      }
    }
    v16 = tjsgdv_pitch + 4 * v6 * v33;
    result = dest_height;
    if ( v11 < dest_height )
    {
      for (v18 = 0; v18 < dest_height - v11; v18 += 1)
      {
        if ( dest_width > 0 )
        {
          v20 = (uint32_t *)v3;
          for (v19 = 0; v19 < dest_width; v19 += 1)
          {
            *v20 = *(uint32_t *)((char *)v20 + v16 - v3);
            ++v20;
          }
          v6 = v31;
        }
        result = 4 * v6;
        v16 += 4 * v6;
        v3 += 4 * v6;
      }
    }
  }
  else
  {
    v21 = v4 * v6;
    v22 = v5 - v4;
    v23 = tjsgdv_src_buffer + 4 * v4 * v6;
    if ( v22 > 0 )
    {
      for (v36 = 0; v36 < v22; v36 += 1)
      {
        if ( dest_width > 0 )
        {
          v25 = (uint32_t *)v3;
          for (v24 = 0; v24 < dest_width; v24 += 1)
          {
            *v25 = *(uint32_t *)((char *)v25 + v23 - v3);
            ++v25;
          }
          v4 = a1;
          v6 = v31;
        }
        v23 += 4 * v6;
        v3 += 4 * v6;
      }
    }
    v26 = v3 + 8 * v21;
    result = v4 + v33;
    v27 = tjsgdv_src_buffer + 4 * v6 * v33;
    if ( v4 + v33 < dest_height )
    {
      for (v28 = 0; v28 < (dest_height - result); v28 += 1)
      {
        v29 = (uint32_t *)v26;
        for (v30 = 0; v30 < dest_width; v30 += 1)
        {
          *v29 = *(uint32_t *)((char *)v29 + v27 - v26);
          ++v29;
        }
        v6 = v31;
        result = 4 * v6;
        v27 += 4 * v6;
        v26 += 4 * v6;
      }
    }
  }
  return result;
}

//----- (100013D0) --------------------------------------------------------
int sub_100013D0(int a1, int dest_width, int dest_height)
{
  int v4; // ebp
  int result; // eax
  int v6; // edi
  int v7; // ebx
  int v8; // ecx
  int v9; // edx
  int v10; // ebp
  int v11; // ebx
  int v12; // edx
  int v13; // ecx
  int v14; // ebx
  int dest_width; // ecx
  int v16; // ebp
  int v17; // ebx
  uint32_t *v18; // ecx
  int v19; // ebx
  int v20; // ecx
  int v21; // ebx
  int v22; // ecx
  int v23; // ebx
  int v24; // [esp+10h] [ebp-3Ch]
  int v26; // [esp+18h] [ebp-34h]
  int v27; // [esp+1Ch] [ebp-30h]
  int v28; // [esp+20h] [ebp-2Ch]
  int v29; // [esp+24h] [ebp-28h]
  int v30; // [esp+28h] [ebp-24h]
  int v31; // [esp+2Ch] [ebp-20h]
  int v32; // [esp+30h] [ebp-1Ch]
  int v33; // [esp+34h] [ebp-18h]
  int v34; // [esp+38h] [ebp-14h]
  int v35; // [esp+3Ch] [ebp-10h]
  int v36; // [esp+40h] [ebp-Ch]
  int v37; // [esp+44h] [ebp-8h]
  int v38; // [esp+58h] [ebp+Ch]
  uint32_t *v39; // [esp+58h] [ebp+Ch]

  v26 = tjsgdv_dest_buffer;
  v35 = tjsgdv_src_buffer;
  v24 = tjsgdv_dest_pitch / 4;
  v4 = dest_height / 2;
  v31 = dest_width / 2;
  v34 = dest_height / 2;
  v30 = dest_width / 2 + dest_height / 2 - a1;
  result = a1 / -2;
  v6 = a1 / 2;
  v38 = a1 / -2 + a1;
  v7 = a1 - a1 / 2;
  if ( !tjsgv_open )
    return result;
  v29 = tjsgdv_pitch + 4 * (v7 + v24 * v6);
  v27 = tjsgdv_pitch + 4 * (v7 + v24 * v6);
  v8 = v24 * result;
  v28 = tjsgdv_pitch + 4 * (v31 + v4 + v24 * result + v38);
  if ( v6 >= 0 )
  {
    if ( v6 < dest_height )
      goto LABEL_7;
    v6 -= dest_height;
  }
  else
  {
    v6 += dest_height;
  }
  v9 = v7 + v24 * v6;
  v29 = tjsgdv_pitch + 4 * v9;
  v27 = tjsgdv_pitch + 4 * v9;
LABEL_7:
  if ( result >= 0 )
  {
    v10 = a1 / -2 + a1;
    if ( result < dest_height )
      goto LABEL_12;
    v10 = dest_height + v38;
    result -= dest_height;
    v38 += dest_height;
  }
  else
  {
    v10 = v38 - dest_height;
    result += dest_height;
    v38 -= dest_height;
  }
  v8 = v24 * result;
  v28 = tjsgdv_pitch + 4 * (v31 + v34 + v24 * result + v10);
LABEL_12:
  if ( dest_height > 0 )
  {
    v33 = v8;
    v32 = dest_height * v24;
    for (v37 = 0; v37 < dest_height; v37 += 1)
    {
      v11 = v30;
      v12 = 0;
      v13 = v26;
      v12 = v30;
      for (v36 = 0; v36 < v30; v36 += 1)
      {
        v13 += 4;
        *(uint32_t *)(v13 - 4) = *(uint32_t *)(v27 - v26 + v13 - 4);
      }
      v11 = v30;
      v14 = v11 + 2 * a1;
      if ( v14 > dest_width )
        v14 = dest_width;
      if ( v12 < v14 )
      {
        v16 = v35 - v26;
        v18 = (uint32_t *)(v26 + 4 * v12);
        v12 += v17;
        for (v17 = 0; v17 < (v14 - v12); v17 += 1)
        {
          *v18 = *(uint32_t *)((char *)v18 + v16);
          ++v18;
          v16 = v35 - v26;
        }
        v10 = v38;
      }
      if ( v12 < dest_width )
      {
        v19 = v26 + 4 * v12;
        v39 = (uint32_t *)v28;
        for (v20 = 0; v20 < (dest_width - v12); v20 += 1)
        {
          v19 += 4;
          *(uint32_t *)(v19 - 4) = *v39;
          ++v39;
        }
      }
      --v30;
      v26 += 4 * v24;
      ++v6;
      v27 += 4 * v24;
      --v10;
      v29 += 4 * v24;
      ++result;
      v38 = v10;
      v28 = v28 + 4 * v24 - 4;
      v21 = v24 + v33;
      v33 += v24;
      v35 += 4 * v24;
      if ( v6 >= 0 )
      {
        if ( v6 < dest_height )
          goto LABEL_33;
        v6 -= dest_height;
        v22 = v29 - 4 * v32;
      }
      else
      {
        v6 += dest_height;
        v22 = 4 * v32 + v29;
      }
      v29 = v22;
      v27 = v22;
LABEL_33:
      if ( result < 0 )
      {
        v10 -= dest_height;
        result += dest_height;
        v38 = v10;
        v23 = v32 + v21;
LABEL_37:
        v33 = v23;
        v28 = tjsgdv_pitch + 4 * (v31 + v34 + v10 + v23);
        goto LABEL_38;
      }
      if ( result >= dest_height )
      {
        v10 += dest_height;
        result -= dest_height;
        v38 = v10;
        v23 = v21 - v32;
        goto LABEL_37;
      }
LABEL_38:
    }
  }
  return result;
}

//----- (100016C0) --------------------------------------------------------
int sub_100016C0(int a1, int dest_width, int dest_height)
{
  int v4; // ebx
  int v5; // edi
  unsigned int v6; // ebp
  int result; // eax
  int v8; // edx
  int v9; // ecx
  int v10; // ebp
  int v11; // ebx
  int v12; // ecx
  int v13; // ecx
  int dest_width; // ebp
  int v15; // ST44_4
  int v16; // ebp
  int v17; // ecx
  uint32_t *v18; // ecx
  int v19; // ebp
  int v20; // ecx
  int v21; // ebp
  int v22; // ecx
  int v23; // ebp
  int v24; // [esp+10h] [ebp-3Ch]
  unsigned int v25; // [esp+14h] [ebp-38h]
  int v26; // [esp+18h] [ebp-34h]
  int v27; // [esp+1Ch] [ebp-30h]
  int v28; // [esp+20h] [ebp-2Ch]
  int v29; // [esp+24h] [ebp-28h]
  int v30; // [esp+28h] [ebp-24h]
  int v31; // [esp+2Ch] [ebp-20h]
  int v32; // [esp+30h] [ebp-1Ch]
  int v33; // [esp+30h] [ebp-1Ch]
  int v34; // [esp+34h] [ebp-18h]
  int v35; // [esp+38h] [ebp-14h]
  int v36; // [esp+38h] [ebp-14h]
  int v37; // [esp+38h] [ebp-14h]
  uint32_t *v38; // [esp+38h] [ebp-14h]
  int v39; // [esp+3Ch] [ebp-10h]
  int v40; // [esp+40h] [ebp-Ch]
  int v41; // [esp+58h] [ebp+Ch]

  v27 = tjsgdv_dest_buffer;
  v39 = tjsgdv_src_buffer;
  v24 = tjsgdv_pitch;
  v4 = dest_height / 2;
  v30 = dest_width / 2;
  v34 = dest_height / 2;
  v29 = dest_width / 2 - dest_height / 2 - a1;
  v5 = a1 / -2;
  v6 = (unsigned int)tjsgdv_dest_pitch >> 2;
  v25 = (unsigned int)tjsgdv_dest_pitch >> 2;
  v32 = a1 / -2 + a1;
  result = a1 / 2;
  v8 = a1 - a1 / 2;
  if ( !tjsgv_open )
    return result;
  v28 = tjsgdv_pitch + 4 * (v32 + v6 * v5);
  v41 = tjsgdv_pitch + 4 * (v32 + v6 * v5);
  v9 = v25 * result;
  v26 = tjsgdv_pitch + 4 * (v30 + v8 + v25 * result - v4);
  if ( v5 >= 0 )
  {
    if ( v5 < dest_height )
      goto LABEL_7;
    v5 -= dest_height;
  }
  else
  {
    v5 += dest_height;
  }
  v28 = tjsgdv_pitch + 4 * (v32 + v25 * v5);
  v41 = tjsgdv_pitch + 4 * (v32 + v25 * v5);
LABEL_7:
  if ( result >= 0 )
  {
    if ( result < dest_height )
      goto LABEL_12;
    v8 -= dest_height;
    result -= dest_height;
  }
  else
  {
    v8 += dest_height;
    result += dest_height;
  }
  v9 = v25 * result;
  v26 = tjsgdv_pitch + 4 * (v30 + v8 + v25 * result - v34);
LABEL_12:
  if ( dest_height > 0 )
  {
    v33 = v9;
    v31 = dest_height * v25;
    for (v40 = 0; v40 < dest_height; v40 += 1)
    {
      v10 = v29;
      v11 = 0;
      if ( v29 > 0 )
      {
        v12 = v27;
        v11 = v29;
        for (v35 = 0; v35 < v29; v35 += 1)
        {
          v12 += 4;
          *(uint32_t *)(v12 - 4) = *(uint32_t *)(v12 + v41 - v27 - 4);
        }
        v10 = v29;
      }
      v13 = v10 + 2 * a1;
      v36 = v13;
      if ( v13 > dest_width )
      {
        v13 = dest_width;
        v36 = dest_width;
      }
      if ( v11 < v13 )
      {
        v15 = v27 + 4 * v11;
        v16 = v39 - v27;
        v17 = v36 - v11;
        v11 += v17;
        v18 = (uint32_t *)v15;
        for (v37 = 0; v37 < v17; v37 += 1)
        {
          *v18 = *(uint32_t *)((char *)v18 + v16);
          ++v18;
          v16 = v39 - v27;
        }
      }
      if ( v11 < dest_width )
      {
        v19 = v27 + 4 * v11;
        v38 = (uint32_t *)v26;
        for (v20 = 0; v20 < (dest_width - v11); v20 += 1)
        {
          v19 += 4;
          *(uint32_t *)(v19 - 4) = *v38;
          ++v38;
        }
      }
      ++v29;
      v27 += 4 * v25;
      ++v5;
      v41 += 4 * v25;
      ++v8;
      v28 += 4 * v25;
      ++result;
      v26 += 4 * v25 + 4;
      v21 = v25 + v33;
      v33 += v25;
      v39 += 4 * v25;
      if ( v5 >= 0 )
      {
        if ( v5 < dest_height )
          goto LABEL_33;
        v5 -= dest_height;
        v22 = v28 - 4 * v31;
      }
      else
      {
        v5 += dest_height;
        v22 = 4 * v31 + v28;
      }
      v28 = v22;
      v41 = v22;
LABEL_33:
      if ( result < 0 )
      {
        v8 += dest_height;
        result += dest_height;
        v23 = v31 + v21;
LABEL_37:
        v33 = v23;
        v26 = v24 + 4 * (v30 + v8 + v23 - v34);
        goto LABEL_38;
      }
      if ( result >= dest_height )
      {
        v8 -= dest_height;
        result -= dest_height;
        v23 = v21 - v31;
        goto LABEL_37;
      }
LABEL_38:
    }
  }
  return result;
}

//----- (10001C70) --------------------------------------------------------
signed int tjsf_initSlideOpen(int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  int v10; // esi
  int *tjslv_dest; // edi
  int *tjslv_src; // eax
  char *v18; // eax
  int v19; // edx
  int v20; // eax
  int v21; // ebp
  uint32_t *v22; // edi
  int v23; // ebx
  int v24; // ecx
  uint32_t *v25; // eax
  int v27; // eax
  unsigned int v33; // [esp+14h] [ebp-1Ch]
  char v34; // [esp+18h] [ebp-18h]
  int v36; // [esp+3Ch] [ebp+Ch]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v10 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  ("tTJSVariant::tTJSVariant()")(&v34);
  if ( !v10 ) {
    ("tTJSVariant::~ tTJSVariant()")(&v34);
    return TJS_E_FAIL;
  }
  if ( !FIND_AND_GET_MEMBER(L"open", v10, &v34) )
  {
    tjsgv_open = 1;
  }
  else
  {
    tjsgv_open = ("tTJSVariant::operator tjs_int() const")(&v34);
  }
  if ( !FIND_AND_GET_MEMBER(L"dest", v10, &v34) )
  {
    ("tTJSVariant::~ tTJSVariant()")(&v34);
    return TJS_E_FAIL;
  }
  tjslv_dest = (int *)("tTJSVariant::operator iTJSDispatch2 *()")(&v34);
  sub_100024B0(tjslv_dest, &tjsgdv_dest_width, &tjsgdv_dest_height, &tjsgdv_dest_buffer, &tjsgdv_dest_pitch);
  if ( !FIND_AND_GET_MEMBER(L"src", v10, &v34) )
  {
    ("tTJSVariant::~ tTJSVariant()")(&v34);
    return TJS_E_FAIL;
  }
  tjslv_src = (int *)("tTJSVariant::operator iTJSDispatch2 *()")(&v34);
  int tjsldv_src_width, tjsldv_src_height, tjsldv_src_pitch;
  sub_100024B0(tjslv_src, &tjsldv_src_width, &tjsldv_src_height, &tjsgdv_src_buffer, &tjsldv_src_pitch);
  if ( tjsldv_src_width != tjsgdv_dest_width || tjsldv_src_height != tjsgdv_dest_height || tjsldv_src_pitch != tjsgdv_dest_pitch )
  {
    ("tTJSVariant::~ tTJSVariant()")(&v34);
    return TJS_E_FAIL;
  }
  if ( tjsgv_open )
  {
    v18 = malloc(4 * tjsgdv_dest_height * tjsgdv_dest_width + 4);
    tjsgdv_buffer = v18;
    v20 = (int)((char *)v18 - ((unsigned __int8)v18 & 3) + 4);
    tjsgdv_pitch = v20;
    if ( tjsgdv_dest_pitch < 1 )
    {
      v20 -= tjsgdv_dest_pitch * (tjsgdv_dest_height - 1);
      tjsgdv_pitch = v20;
    }
    v21 = tjsgdv_dest_buffer;
    v22 = (uint32_t *)v20;
    v33 = (unsigned int)tjsgdv_dest_pitch >> 2;
    for (v36 = 0; v36 < tjsgdv_dest_height; v36 += 1)
    {
      v25 = v22;
      for (v24 = 0; v24 < tjsgdv_dest_width; v24 += ++)
      {
        *v25 = *(uint32_t *)((char *)v25 + v21 - (uint32_t)v22);
        ++v25;
      }
      v22 += v33;
      v21 += 4 * v33;
    }
  }
  if ( !FIND_AND_GET_MEMBER(L"type", v10, &v34) )
  {
    v27 = tjsgv_type;
  }
  else
  {
    v27 = ("tTJSVariant::operator tjs_int() const")(&v34);
    tjsgv_type = v27;
  }
  if ( v27 < 0 )
  {
    v27 = ("tjs_uint64 ::TVPGetTickCount()")() & 3;
    tjsgv_type = v27;
    if ( tjsgdv_current_type != v27 )
      goto LABEL_52;
    LOBYTE(v27) = v27 + 1;
  }
  v27 &= 3u;
  tjsgv_type = v27;
LABEL_52:
  if ( !tjsgv_open )
  {
    v27 &= 1u;
    tjsgv_type = v27;
  }
  tjsgdv_current_type = v27;
  ("tTJSVariant::~ tTJSVariant()")(&v34);
  return TJS_S_OK;
}

//----- (100020D0) --------------------------------------------------------
signed int tjsf_drawSlideOpen(int a1, int a2, int a3, double a4, int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  int v12; // esi
  double v15; // st6
  signed int v35; // [esp+98h] [ebp+18h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  a4 = ("tTVReal tTJSVariant::AsReal() const")(param[0]);
  if ( !tjsgv_open )
    a4 = 1.0 - a4;
  v15 = (double)(tjsgdv_dest_width / 2) * a4 + 0.5;
  v35 = (signed __int64)v15;
  switch ( tjsgv_type )
  {
    case 0:
      sub_10001000((signed __int64)v15, tjsgdv_dest_width, tjsgdv_dest_height);
      break;
    case 1:
      sub_100011B0((signed __int64)v15, tjsgdv_dest_width, tjsgdv_dest_height);
      break;
    case 2:
      sub_100013D0((signed __int64)((double)v35 * 1.75), tjsgdv_dest_width, tjsgdv_dest_height);
      break;
    case 3:
      sub_100016C0((signed __int64)((double)v35 * 1.75), tjsgdv_dest_width, tjsgdv_dest_height);
      break;
    default:
      break;
  }
  //UPDATE: tjsgv_dest 0 0 tjsgdv_dest_width (tjsgdv_dest_height?)
  return TJS_S_OK;
}

//----- (10002330) --------------------------------------------------------
signed int tjsf_finishSlideOpen(int this, int flag, int membername)
{
  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( tjsgdv_buffer )
  {
    free(tjsgdv_buffer);
    tjsgdv_buffer = 0;
  }
  return TJS_S_OK;
}

//----- (100024B0) --------------------------------------------------------
signed int sub_100024B0(int *a1, uint32_t *a2, uint32_t *a3, uint32_t *a4, int *a5)
{
  char v19; // [esp+24h] [ebp-18h]

  if ( !a1 )
    return 0;
  ("tTJSVariant::tTJSVariant()")(&v19);
  if ( !FIND_AND_GET_MEMBER(L"imageWidth", a1, &v19) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.imageWidth failed.");
  }
  *a2 = ("tTJSVariant::operator tjs_int() const")(&v19);
  if ( !FIND_AND_GET_MEMBER(L"imageHeight", a1, &v19) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.imageHeight failed.");
  }
  *a3 = ("tTJSVariant::operator tjs_int() const")(&v19);
  if ( !FIND_AND_GET_MEMBER(L"mainImageBufferForWrite", a1, &v19) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.mainImageBufferForWrite failed.");
  }
  *a4 = ("tTJSVariant::operator tjs_int() const")(&v19);
  if ( !FIND_AND_GET_MEMBER(L"mainImageBufferPitch", a1, &v19) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.mainImageBufferPitch failed.");
  }
  *a5 = ("tTJSVariant::operator tjs_int() const")(&v19);
  ("tTJSVariant::~ tTJSVariant()")(&v19);
  return 1;
}
