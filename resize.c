#include <windows.h>
#include <defs.h>


//-------------------------------------------------------------------------
// Function declarations

signed int __cdecl sub_4012F4(int a1, int *a2, int *a3, int *a4, int *a5);
signed int __cdecl tjsf_AreaAverageReducation(int a1, int a2, int a3, int a4, int a5, signed int a6, int *a7);

//----- (004012F4) --------------------------------------------------------
signed int __cdecl sub_4012F4(int a1, int *a2, int *a3, int *a4, int *a5)
{
  char v6; // [esp+2Ch] [ebp-Ch]

  __InitExceptBlockLDTC();
  if ( !a1 )
    return 0;
  ("tTJSVariant::tTJSVariant()")((int)&v6);
  if ( !FIND_AND_GET_MEMBER(L"imageWidth", a1, &v6) )
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")((int)L"invoking of Layer.imageWidth failed.");
  *a2 = ("tTJSVariant::operator tjs_int() const")((int)&v6);
  if ( !FIND_AND_GET_MEMBER(L"imageHeight", a1, &v6) )
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")((int)L"invoking of Layer.imageHeight failed.");
  *a3 = ("tTJSVariant::operator tjs_int() const")((int)&v6);
  if ( !FIND_AND_GET_MEMBER(L"mainImageBufferForWrite", a1, &v6) )
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")((int)L"invoking of Layer.mainImageBufferForWrite failed.");
  *a4 = ("tTJSVariant::operator tjs_int() const")((int)&v6);
  if ( !FIND_AND_GET_MEMBER(L"mainImageBufferPitch", a1, &v6) )
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")((int)L"invoking of Layer.mainImageBufferPitch failed.");
  *a5 = ("tTJSVariant::operator tjs_int() const")((int)&v6);
  ("tTJSVariant::~ tTJSVariant()")((int)&v6, 2);
  return 1;
}

//----- (0040188C) --------------------------------------------------------
signed int __cdecl tjsf_AreaAverageReducation(int this, int flag, int membername, int hint, int result, signed int numparams, int *param)
{
  int v8; // edx
  int v9; // eax
  int v10; // eax
  int v11; // eax
  int v12; // eax
  int v13; // eax
  int v14; // eax
  int *v15; // edx
  int v16; // eax
  int v17; // esi
  int v18; // ebx
  int v19; // eax
  int v20; // esi
  int v21; // eax
  int v22; // esi
  int v23; // eax
  int v24; // ST28_4
  int v25; // ST20_4
  int v26; // ST18_4
  int v27; // ST10_4
  int v28; // eax
  int v29; // [esp+Ch] [ebp-E8h]
  int k; // [esp+10h] [ebp-E4h]
  int v31; // [esp+14h] [ebp-E0h]
  int v32; // [esp+18h] [ebp-DCh]
  int v33; // [esp+1Ch] [ebp-D8h]
  int v34; // [esp+20h] [ebp-D4h]
  int v35; // [esp+24h] [ebp-D0h]
  int v36; // [esp+28h] [ebp-CCh]
  int v37; // [esp+2Ch] [ebp-C8h]
  int j; // [esp+40h] [ebp-B4h]
  unsigned int *v39; // [esp+44h] [ebp-B0h]
  int i; // [esp+48h] [ebp-ACh]
  int v41; // [esp+4Ch] [ebp-A8h]
  int v42; // [esp+50h] [ebp-A4h]
  int v43; // [esp+58h] [ebp-9Ch]
  int v44; // [esp+5Ch] [ebp-98h]
  int v45; // [esp+60h] [ebp-94h]
  int v46; // [esp+64h] [ebp-90h]
  int v47; // [esp+68h] [ebp-8Ch]
  int v48; // [esp+6Ch] [ebp-88h]
  int v49; // [esp+70h] [ebp-84h]
  int v50; // [esp+74h] [ebp-80h]
  int v51; // [esp+78h] [ebp-7Ch]
  int v52; // [esp+7Ch] [ebp-78h]
  int v53; // [esp+80h] [ebp-74h]
  int v54; // [esp+84h] [ebp-70h]
  int v55; // [esp+88h] [ebp-6Ch]
  int v56; // [esp+8Ch] [ebp-68h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( numparams < 2 )
    return TJS_E_BADPARAMCOUNT;
  v56 = ("tjs_uint64 ::TVPGetTickCount()")();
  v9 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  sub_4012F4(v9, &v51, &v49, &v55, &v53);
  v10 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[1]);
  sub_4012F4(v10, &v50, &v48, &v54, &v52);
  if ( numparams <= 2 )
    v11 = 0;
  else
    v11 = ("tTVInteger tTJSVariant::AsInteger() const")(param[2]);
  v47 = v11;
  if ( numparams <= 3 )
    v12 = 0;
  else
    v12 = ("tTVInteger tTJSVariant::AsInteger() const")(param[3]);
  v46 = v12;
  if ( numparams <= 4 )
    v13 = v50;
  else
    v13 = ("tTVInteger tTJSVariant::AsInteger() const")(param[4]);
  v45 = v13;
  if ( numparams <= 5 )
    v14 = v48;
  else
    v14 = ("tTVInteger tTJSVariant::AsInteger() const")(param[5]);
  v44 = v14;
  if ( v51 < v45 || v49 < v14 )
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")("\"AreaAverageReducation\"");
  if ( v47 < 0 || v46 < 0 || v45 + v47 > v50 || v44 + v46 > v48 )
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")("篲嘖奸のヸ晍ゼ轿がぺ瘟留るため、ユ儆璒ぜ偢歗ます。");
  v43 = v45 + v47;
  v42 = (unsigned __int64)((long double)v51 / (long double)v45 * 4096.0);
  v41 = (unsigned __int64)((long double)v49 / (long double)v44 * 4096.0);
  for ( i = v46; i < v44 + v46; ++i )
  {
    v39 = (unsigned int *)(4 * v47 + v54 + v52 * i);
    for ( j = v47; j < v43; ++j )
    {
      v37 = v42 * j >> 12;
      v36 = (v42 + v42 * j + 4095) >> 12;
      v35 = (v41 + v41 * i + 4095) >> 12;
      if ( v36 >= v51 )
        v36 = v51 - 1;
      if ( v35 >= v49 )
        v35 = v49 - 1;
      v34 = 0;
      v33 = 0;
      v32 = 0;
      v31 = 0;
      for ( k = v41 * i >> 12; k < v35; ++k )
      {
        v15 = (int *)(4 * v37 + v55 + v53 * k);
        v16 = k << 12;
        v17 = (k + 1) << 12;
        if ( k << 12 < v41 * i )
          v16 = v41 * i;
        if ( (k + 1) << 12 > v41 + v41 * i )
          v17 = v41 + v41 * i;
        v29 = v17 - v16;
        
        if ( v37 < v36 )
        {
          for (v18 = v42 * j >> 12; v18 < v36; ++v18)
          {
            v19 = v18 << 12;
            v20 = (v18 + 1) << 12;
            if ( v18 << 12 < v42 * j )
              v19 = v42 * j;
            if ( (v18 + 1) << 12 > v42 + v42 * j )
              v20 = v42 + v42 * j;
            v21 = v29 * (v20 - v19) >> 12;
            v34 += v21;
            v22 = *v15;
            v33 += v21 * (((unsigned int)*v15 >> 16) & 0xFF);
            v32 += v21 * ((unsigned __int16)*v15 >> 8);
            ++v15;
            v31 += v21 * (unsigned __int8)v22;
          }
        }
      }
      if ( v34 )
      {
        *v39 = (unsigned __int8)(v31 / v34) | ((unsigned __int8)(v32 / v34) << 8) | ((unsigned __int8)(v33 / v34) << 16) | 0xFF000000;
        ++v39;
      }
    }
  }
  v23 = ("tjs_uint64 ::TVPGetTickCount()")();
  //"area average reducation:RESULT (\1, \2)->(\3, \4), time = \5 (ms)" w/ TVPAddLog (v23 - v56) v44 v45 v49 v51
  return TJS_S_OK;
}
