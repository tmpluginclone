#include <windows.h>
#include <defs.h>

#include <stdarg.h>


//-------------------------------------------------------------------------
// Function declarations

signed int tjsf_setwallpaper(int a1, int a2, int a3, int a4, int a5, signed int a6, int *a7);
// signed int tjsf_getwallpaper(int a1, int a2, int a3, int a4, int a5, int a6);
int sub_10001E40(void *this, int a2, int a3);
// int sub_10001F00(int a1, int a2, int a3, ...);
// int sub_10001FC0(int a1, int a2, ...);
// int sub_10002080(int a1, int a2, ...);
// int sub_10002140(int a1, int a2, ...);

//-------------------------------------------------------------------------
// Data declarations

IID rclsid = { 1963230976u, 61215u, 4560u, { 152u, 136u, 0u, 96u, 151u, 222u, 172u, 249u } }; // idb
//75048700-EF1F-11D0-9888-006097DEACF9: UserAssist
IID riid = { 4103138048u, 4672u, 4561u, { 152u, 136u, 0u, 96u, 151u, 222u, 172u, 249u } }; // idb
//F490EB00-1240-11D1-9888-006097DEACF9: IActiveDesktop
int dword_1000A038 = 3; // weak
wchar_t *off_1000A03C[3] = { L".bmp", L".png", L".jpg" }; // weak
BYTE a2[] = { 50u, 0u }; // idb
BYTE Data[] = { 49u, 0u }; // idb
char byte_1000D2B4 = '\0'; // weak

//----- (10001200) --------------------------------------------------------
signed int tjsf_setwallpaper(int this, int flag, int membername, int hint, int result, signed int numparams, int *param)
{
  int v13; // esi
  int v23; // rax
  int v25; // rax
  __int64 v38; // rax
  uint8_t v39; // al
  __int64 v44; // rax
  uint8_t v45; // al
  int v46; // ebp
  int v48; // esi
  wchar_t *v55; // edi
  int *v57; // esi
  int v63; // esi
  int v70; // esi
  int v73; // eax
  const BYTE *v74; // eax
  const BYTE *v75; // eax
  void *v78; // eax
  int v79; // esi
  LPVOID ppv; // [esp+A8h] [ebp-44Ch]
  char v83; // [esp+ACh] [ebp-448h]
  char v84; // [esp+B0h] [ebp-444h]
  HKEY phkResult; // [esp+B4h] [ebp-440h]
  int v86; // [esp+B8h] [ebp-43Ch]
  char v87; // [esp+BCh] [ebp-438h]
  char v88; // [esp+C0h] [ebp-434h]
  char v89; // [esp+C4h] [ebp-430h]
  char v92; // [esp+D0h] [ebp-424h]
  char v93; // [esp+DCh] [ebp-418h]
  char v94; // [esp+E0h] [ebp-414h]
  char v95; // [esp+E4h] [ebp-410h]
  CHAR String; // [esp+E8h] [ebp-40Ch]
  char v97; // [esp+E9h] [ebp-40Bh]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  ("tTJSVariant::tTJSVariant()")(&v92);
  if ( ("tTJSVariantType tTJSVariant::Type()")(param[0]) == 1 )
  {
    v13 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
    if ( !FIND_AND_GET_MEMBER(L"storage", v13, &v92) )
    {
      ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"storage(設定する壁紙)が指定されていないよ。\n壁紙を消すときは「storage=\"\"」って指定してね。");
      ("tTJSVariant::~ tTJSVariant()")(&v92);
      return TJS_E_INVALIDPARAM;
    }
    if ( ("tTJSVariantType tTJSVariant::Type()")(&v92) != 2 )
    {
      ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"storageが文字列じゃないよ。");
      ("tTJSVariant::~ tTJSVariant()")(&v92);
      return TJS_E_INVALIDPARAM;
    }
    ("tTJSVariant::tTJSVariant()")(&v89);
    if ( !FIND_AND_GET_MEMBER(L"tile", v13, &v89) )
      goto LABEL_142;
    v23 = ("tTVInteger tTJSVariant::AsInteger() const")(&v89);
    v86 = 1;
    if ( !v23 )
LABEL_142:
      v86 = 0;
    if ( !FIND_AND_GET_MEMBER(L"magnify", v13, &v89) )
      goto LABEL_143;
    v25 = ("tTVInteger tTJSVariant::AsInteger() const")(&v89);
    ppv = (LPVOID)1;
    if ( !v25 )
LABEL_143:
      ppv = 0;
    ("tTJSVariant::~ tTJSVariant()")(&v89);
  }
  else
  {
    if ( ("tTJSVariantType tTJSVariant::Type()")(param[0]) != 2 )
    {
      ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"storage");
      ("tTJSVariant::~ tTJSVariant()")(&v92);
      return TJS_E_INVALIDPARAM;
    }
    ("tTJSVariant & tTJSVariant::operator =(const tTJSVariant &)")(&v92, param[0]);
    if ( numparams < 2 )
      goto LABEL_144;
    if ( ("tTJSVariantType tTJSVariant::Type()")(param[1]) != 4 )
      goto LABEL_144;
    LODWORD(v38) = ("tTVInteger tTJSVariant::AsInteger() const")(param[1]);
    if ( v38 )
      v39 = 1;
    else
LABEL_144:
      v39 = 0;
    v86 = v39;
    if ( numparams < 3 )
      goto LABEL_145;
    if ( ("tTJSVariantType tTJSVariant::Type()")(param[1]) != 4 )
      goto LABEL_145;
    LODWORD(v44) = ("tTVInteger tTJSVariant::AsInteger() const")(param[2]);
    if ( v44 )
      v45 = 1;
    else
LABEL_145:
      v45 = 0;
    ppv = (LPVOID)v45;
  }
  String = byte_1000D2B4;
  memset(&v97, 0, 0x3FCu);
  v48 = ("tTJSVariantString * tTJSVariant::AsString() const")(&v92);
  // Find wallpaper if extension
  v46 = 0;
  ("tTJSString::tTJSString(tTJSVariantString *)")(&v93, v48);
  sub_10001FC0(v48, (int)&v88, &v93);
  ("tTJSString::~ tTJSString()")(&v93);
  if ( ("tjs_int tTJSString::length() const")(&v88) > 0 )
  {
    ("void tTJSString::ToLowerCase()")(&v88);
    if ( dword_1000A038 - 1 >= 0 )
    {
      for (v46 = 0; i < dword_1000A038; v46 += 1)
      {
        if ( ("bool tTJSString::operator ==(const tjs_char *) const")(&v88, off_1000A03C[v46]) )
          break;
      }
    }
  }
  v57 = (int *)("tTJSVariantString * tTJSVariant::AsString() const")(&v92);
  ("tTJSString::tTJSString(tTJSVariantString *)")(&v94, v57);
  sub_10002080((int)v57, (int)&v95, &v94);
  ("tTJSString::~ tTJSString()")(&v94);
  ("tTJSString::tTJSString(const tjs_char *)")(&v83, L"\0");
  if ( v46 < dword_1000A038 )
  {
    v57 = (int *)&off_1000A03C[v46];
    while ( 1 )
    {
      sub_10001E40(&v95, (int)&v87, *v57);
      if ( ("bool ::TVPIsExistentStorage(const ttstr &)")(&v87) )
        break;
      ("tTJSString::~ tTJSString()")(&v87);
      ++v46;
      ++v57;
      if ( v46 >= dword_1000A038 )
        goto LABEL_116;
    }
    v63 = sub_10002140((int)v57, (int)&v84, &v87);
    ("tTJSString & tTJSString::operator =(const tTJSString &)")(&v83, v63);
    ("tTJSString::~ tTJSString()")(&v84);
    ("void ::TVPGetLocalName(ttstr &)")(&v83);
    v57 = (int *)("tjs_int tTJSString::GetNarrowStrLen() const")(&v83);
    ("void tTJSString::ToNarrowStr(tjs_nchar *,tjs_int) const")(&v83, &String, v57);
    ("tTJSString::~ tTJSString()")(&v87);
  }
LABEL_116:
  v70 = sub_10001F00((int)v57, (int)&v84, (int)L"search result: ", &v83);
  ("void ::TVPAddLog(const ttstr &)")(v70);
  ("tTJSString::~ tTJSString()")(&v84);
  RegOpenKeyA(HKEY_CURRENT_USER, "Control Panel\\desktop\\", &phkResult);
  v73 = lstrlenA(&String);
  RegSetValueExA(phkResult, "Wallpaper", 0, 1u, (const BYTE *)&String, v73 + 1);
  v74 = Data;
  if ( !v86 )
    v74 = (const BYTE *)"0";
  RegSetValueExA(phkResult, "TileWallpaper", 0, 1u, v74, 4u);
  v75 = ::flag;
  if ( !ppv )
    v75 = (const BYTE *)"0";
  RegSetValueExA(phkResult, "WallpaperStyle", 0, 1u, v75, 4u);
  RegCloseKey(phkResult);
  SystemParametersInfoA(0x14u, 0, &String, 3u);
  if ( v46 )
  {
    if ( ("bool tTJSString::operator !=(const tjs_char *) const")(&v83, L"\0") )
    {
      CoInitialize(0);
      if ( CoCreateInstance(&rclsid, 0, 1u, &riid, &ppv) >= 0 )
      {
        *(_DWORD *)&v89 = 12;
        if ( (*(int (__stdcall **)(LPVOID, char *, _DWORD))(*(_DWORD *)ppv + 44))(ppv, &v89, 0) >= 0 )
          (*(void (__stdcall **)(LPVOID, signed int))(*(_DWORD *)ppv + 12))(ppv, 7);
        (*(void (__stdcall **)(LPVOID))(*(_DWORD *)ppv + 8))(ppv);
      } //IActiveDesktop->ApplyChanges()
      CoUninitialize();
    }
  }
  if ( result )
  {
    ("tTJSVariant & tTJSVariant::operator =(const tjs_char *)")(result, L"success");
  }
  v78 = (void *)sub_10001F00(result, (int)&ppv, (int)L"setwallpaper(", &v83);
  v79 = sub_10001E40(v78, (int)&v84, (int)L") succeeded.");
  ("void ::TVPAddLog(const ttstr &)")(v79);
  ("tTJSString::~ tTJSString()")(&v84);
  ("tTJSString::~ tTJSString()")(&ppv);
  ("tTJSString::~ tTJSString()")(&v83);
  ("tTJSString::~ tTJSString()")(&v95);
  ("tTJSString::~ tTJSString()")(&v88);
  ("tTJSVariant::~ tTJSVariant()")(&v92);
  return TJS_S_OK;
}

//----- (10001C40) --------------------------------------------------------
signed int tjsf_getwallpaper(int a1, int this, int flag, int membername, int hint, int result)
{
  HKEY phkResult; // [esp+0h] [ebp-408h]
  DWORD cbData; // [esp+4h] [ebp-404h]
  BYTE Data; // [esp+8h] [ebp-400h]
  char v11; // [esp+9h] [ebp-3FFh]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( result )
  {
    Data = byte_1000D2B4;
    memset(&v11, 0, 0x3FCu);
    cbData = 1024;
    RegOpenKeyA(HKEY_CURRENT_USER, "Control Panel\\desktop\\", &phkResult);
    RegQueryValueExA(phkResult, "Wallpaper", 0, 0, &Data, &cbData);
    RegCloseKey(phkResult);
    ("tTJSVariant & tTJSVariant::operator =(const tjs_nchar *)")(result, &Data);
  }
  return TJS_S_OK;
}

//----- (10001E40) --------------------------------------------------------
int sub_10001E40(void *this, int a2, int a3)
{
  void *v4; // esi
  int v5; // esi

  v4 = this;
  v5 = ("tTJSString tTJSString::operator +(const tjs_char *) const")(&a3, v4, a3);
  ("tTJSString::tTJSString(const tTJSString &)")(a2, v5);
  ("tTJSString::~ tTJSString()")(&a3);
  return a2;
}

//----- (10001F00) --------------------------------------------------------
int sub_10001F00(int a1, int a2, int a3, ...)
{
  int v4; // esi
  int v10; // [esp+1Ch] [ebp+Ch]
  va_list va; // [esp+1Ch] [ebp+Ch]
  va_list va1; // [esp+20h] [ebp+10h]

  va_start(va1, a3);
  va_start(va, a3);
  v10 = va_arg(va1, _DWORD);
  v4 = ("tTJSString ::operator +(const tjs_char *,const tTJSString &)")((int *)va, a3, v10, a1);
  ("tTJSString::tTJSString(const tTJSString &)")(a3, v4);
  ("tTJSString::~ tTJSString()")(va1, v8, 1);
  return a3;
}

//----- (10001FC0) --------------------------------------------------------
int sub_10001FC0(int a1, int a2, ...)
{
  int v3; // esi
  int v6; // esi
  int v11; // [esp+18h] [ebp+8h]
  va_list va; // [esp+18h] [ebp+8h]
  va_list va1; // [esp+1Ch] [ebp+Ch]

  va_start(va1, a2);
  va_start(va, a2);
  v11 = va_arg(va1, _DWORD);
  v3 = ("ttstr ::TVPExtractStorageExt(const ttstr &)")((int *)va, v11, a1);
  v6 = v11;
  ("tTJSString::tTJSString(const tTJSString &)")(v11, v3);
  ("tTJSString::~ tTJSString()")(va1, 0, 1);
  return v6;
}

//----- (10002080) --------------------------------------------------------
int sub_10002080(int a1, int a2, ...)
{
  int v3; // esi
  int v6; // esi
  int v11; // [esp+18h] [ebp+8h]
  va_list va; // [esp+18h] [ebp+8h]
  va_list va1; // [esp+1Ch] [ebp+Ch]

  va_start(va1, a2);
  va_start(va, a2);
  v11 = va_arg(va1, _DWORD);
  v3 = ("ttstr ::TVPChopStorageExt(const ttstr &)")((int *)va, v11, a1);
  v6 = v11;
  ("tTJSString::tTJSString(const tTJSString &)")(v11, v3);
  ("tTJSString::~ tTJSString()")(va1, 0, 1);
  return v6;
}

//----- (10002140) --------------------------------------------------------
int sub_10002140(int a1, int a2, ...)
{
  int v3; // esi
  int v6; // esi
  int v11; // [esp+18h] [ebp+8h]
  va_list va; // [esp+18h] [ebp+8h]
  va_list va1; // [esp+1Ch] [ebp+Ch]

  va_start(va1, a2);
  va_start(va, a2);
  v11 = va_arg(va1, _DWORD);
  v3 = ("ttstr ::TVPGetPlacedPath(const ttstr &)")((int *)va, v11, a1);
  v6 = v11;
  ("tTJSString::tTJSString(const tTJSString &)")(v11, v3);
  ("tTJSString::~ tTJSString()")(va1, 0, 1);
  return v6;
}
