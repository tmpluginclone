#include <windows.h>
#include <math.h>
#include <defs.h>


//-------------------------------------------------------------------------
// Function declarations

void sub_10001010();
void sub_10001040();
double tjsdf_getrandombits128();
signed int tjsf_drawFireSpark(int a1, int a2, int a3, int a4, int a5, int a6, int *a7);
// signed int tjsf_initFireSpark(int a2, int a3, int a4, int a6, char a7, int a8, int a9, ...);
signed int tjsf_finishFireSpark(int a1, int a2, int a3);
// signed int tjsf_changeFireSpark(int a1, int a2, int a3, int a4, int a5, int a6, signed int a7, int *a8);

//-------------------------------------------------------------------------
// Data declarations

char tjsgv_effect = 1; // weak
int tjsgv_maxgen = 500; // weak
int tjsgv_basevelocity = 5504; // weak
int tjsgv_velocityrange = 2359; // weak
int tjsgv_baselifetime = 1500; // weak
int tjsgv_lifetimerange = 2500; // weak
int tjsgv_basetop = 4294967246; // weak
int tjsgv_heightrange = 4294967246; // weak
int tjsgv_basesizew = 2; // weak
int tjsgv_sizewrange = 4; // weak
int tjsgv_basesizeh = 2; // weak
int tjsgv_sizehrange = 4; // weak
double tjsgv_windvariancecapability = 0.0003; // weak
double tjsgv_maxwindvelocity =  0.2; // weak
int tjsgv_samesize = 1; // weak
char byte_1000F084[32] =
{
  0xFF,
  0xFF,
  0x00,
  0xFF,
  'À',
  '\0',
  0xFF,
  '€',
  '\0',
  0xFF,
  '@',
  0x00,
  0xFF,
  ' ',
  0x00,
  'À',
  0x00,
  0x00,
  0x00,
  0x00,
  0x00,
  0x00,
  0x00,
  0x00,
  0x00,
  0x00,
  0x00,
  0x00,
  0x00,
  0x00,
  0x00,
  0x00
}; // idb
int dword_1000F0A4 = 7; // weak
int64_t tjsgdv_curtick = 0ll; // weak
int tjsgdv_maxgennew = 0; // weak
int tjsgv_inlayerbuffer = 0; // weak
int dword_10012A4C = 0; // weak
int tjsgv_inlayer = 0; // weak
int dword_10012A58 = 0; // weak
int tjsgv_inlayerheight = 0; // weak
int tjsgdv_curtime = 0; // weak
int tjsgv_transtime = 0; // weak
double tjsgv_directionbase = 0.0; // weak
int dword_10012A78[256] =
{
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0
}; // idb
int tjsgdv_firesparkbuffer = 0; // weak
double tjsgv_directionrange =  0.0; // weak
int tjsgv_inlayerwidth = 0; // weak
char tjsgdv_istransitioning = 0; // weak
int tjsgdv_maxgenold = 0; // weak
int dword_10012E98 = 0; // weak
int tjsgv_inlayerpitch = 0; // weak
char tjsgdv_firesparkactive = 0; // weak
char tjsgv_pause = '\0'; // weak
int tjsgv_baseaccel = 0; // weak
int tjsgv_accelrange = 0; // weak
int tjsgv_windvelocity = 0; // weak
int tjsgdv_relatedwindvelocity = 0; // weak
int tjsgdv_fillcolor = 0; // weak


//----- (10001010) --------------------------------------------------------
void sub_10001010()
{
  tjsgv_directionbase = 3.14159265368979 * 0.5;
}

//----- (10001040) --------------------------------------------------------
void sub_10001040()
{
  tjsgv_directionrange = 3.14159265368979 * 0.15;
}

//----- (10001060) --------------------------------------------------------
double tjsdf_getrandombits128()
{
  int64_t v2; // [esp+0h] [ebp-14h]

  ("void ::TVPGetRandomBits128(void *)")((char *)&v2 + 4);
  return (double)HIDWORD(v2) * 2.328306437080797e-10;
}

//----- (100014D0) --------------------------------------------------------
signed int tjsf_drawFireSpark(int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  int v11; // eax
  int64_t v15; // rdi
  int v19; // ebp
  int v20; // ebx
  int *v21; // ecx
  int v22; // esi
  int v24; // edx
  int v26; // eax
  int v27; // eax
  int64_t v28; // rax
  int v29; // edi
  int v30; // ebx
  uint32_t *v32; // esi
  int64_t v33; // rax
  long double v34; // st7
  int64_t v35; // rax
  long double v36; // st7
  int64_t v37; // rax
  int *v46; // esi
  unsigned int v47; // ecx
  unsigned int v49; // edi
  int v52; // eax
  int v53; // edi
  int v54; // eax
  int v55; // edx
  int v56; // eax
  int v57; // ecx
  int v58; // eax
  int v59; // ebp
  int v60; // edx
  int v61; // eax
  int v62; // ecx
  int v63; // ebx
  char *v64; // edx
  int v65; // ebp
  int v66; // ebx
  double v71; // st7
  int v73; // [esp+24h] [ebp-D4h]
  signed int v74; // [esp+24h] [ebp-D4h]
  int v76; // [esp+28h] [ebp-D0h]
  signed int v79; // [esp+38h] [ebp-C0h]
  int v80; // [esp+3Ch] [ebp-BCh]
  int v81; // [esp+40h] [ebp-B8h]
  int v82; // [esp+44h] [ebp-B4h]
  int v83; // [esp+48h] [ebp-B0h]
  long double v84; // [esp+4Ch] [ebp-ACh]
  double v87; // [esp+5Ch] [ebp-9Ch]
  int v88; // [esp+64h] [ebp-94h]
  int v89; // [esp+68h] [ebp-90h]
  int v91; // [esp+70h] [ebp-88h]
  int v92; // [esp+74h] [ebp-84h]
  int v93; // [esp+78h] [ebp-80h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( numparams <= 0 )
  {
    v15 = ("tjs_uint64 ::TVPGetTickCount()")();
  }
  else
  {
    ("tTJSVariant::tTJSVariant()")(&v84);
    v11 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
    if ( FIND_AND_GET_MEMBER(L"tick", v11, &v84) )
    {
      v15 = ("tTJSVariant::operator tjs_int() const")(&v84);
    }
    else
    {
      v15 = ("tjs_uint64 ::TVPGetTickCount()")();
    }
    ("tTJSVariant::~ tTJSVariant()")(&v84);
  }
  if ( tjsgv_pause || !tjsgv_effect )
  {
    tjsgdv_curtick = v15;
  }
  else
  {
    v79 = v15 - tjsgdv_curtick;
    if ( (signed int)v15 - (signed int)tjsgdv_curtick >= 1 )
    {
      v19 = tjsgv_maxgen;
      v20 = tjsgv_inlayerpitch;
      v88 = tjsgv_inlayerbuffer;
      v91 = tjsgv_inlayerwidth;
      v82 = tjsgv_inlayerwidth;
      v92 = tjsgv_inlayerheight;
      v80 = tjsgv_inlayerheight;
      v89 = tjsgv_inlayerpitch;
      v81 = 0;
      v83 = 0;
      v73 = tjsgdv_firesparkbuffer;
      v21 = (int *)(tjsgdv_firesparkbuffer + 64);
      for (size_t i = 0; i < tjsgv_maxgen; i += 1)
      {
        if ( v21[3] )
        {
          v22 = v20 * v21[0] + v88 + 4 * v21[-1];
          for (size_t j = 0; j < v21[2]; j += 1)
          {
            v24 = v21[1];
            for (size_t k = 0; k < v24; k += 1)
            {
              ((uint32_t *)v22)[k] = tjsgdv_fillcolor;
            }
            v22 += v20;
          }
          v19 = tjsgv_maxgen;
          v26 = v21[-1];
          if ( v82 > v26 )
            v82 = v21[-1];
          if ( v80 > v21[0] )
            v80 = v21[0];
          v27 = v21[1] + v26;
          if ( v81 < v27 )
            v81 = v27;
          if ( v83 < v21[0] + v21[2] )
            v83 = v21[0] + v21[2];
        }
        v21 += 20;
      }
      if ( tjsgdv_istransitioning )
      {
        if ( (signed int)v15 - tjsgdv_curtime >= tjsgv_transtime )
        {
          tjsgdv_istransitioning = 0;
          v19 = tjsgdv_maxgennew;
          tjsgv_maxgen = tjsgdv_maxgennew;
          if ( tjsgdv_maxgennew < 1 )
          {
            free(tjsgdv_firesparkbuffer);
            tjsgdv_firesparkactive = 0;
            //UPDATE: tjsgv_inlayer 0 0 tjsgv_inlayerwidth tjsgv_inlayerheight
            return TJS_S_OK;
          }
        }
        else
        {
          v28 = (int64_t)((double)(tjsgdv_maxgennew - tjsgdv_maxgenold)
                               * ((double)((signed int)v15 - tjsgdv_curtime)
                                / (double)tjsgv_transtime));
          v19 = tjsgdv_maxgenold + v28;
          tjsgv_maxgen = tjsgdv_maxgenold + v28;
        }
      }
      v29 = dword_10012A58 + v79 * dword_10012E98;
      v30 = v19;
      if ( v29 & 0xFFFF0000 )
      {
        v29 >>= 16;
        v32 = (uint32_t *)(v73 + 8);
        for (size_t i = 0; i < v30; i += 1)
        {
          if ( !v32[17] )
          {
            v84 = tjsdf_getrandombits128() * tjsgv_directionrange + tjsgv_directionbase;
            v87 = tjsdf_getrandombits128() * (double)tjsgv_velocityrange + (double)tjsgv_basevelocity;
            v32[-2] = (int64_t)(tjsdf_getrandombits128() * (double)tjsgv_inlayerwidth);
            v33 = (int64_t)(tjsdf_getrandombits128() * (double)tjsgv_heightrange + (double)tjsgv_basetop);
            v34 = cos(v84);
            v32[-1] = v33;
            v35 = (int64_t)(v34 * v87);
            v36 = sin(v84);
            v32[0] = v35;
            v32[1] = (int64_t)(v36 * v87);
            v32[3] = 0;
            v32[2] = 0;
            v32[4] = (int64_t)(tjsdf_getrandombits128() * (double)tjsgv_lifetimerange + (double)tjsgv_baselifetime);
            v32[10] = (int64_t)(tjsdf_getrandombits128() * (double)tjsgv_accelrange + (double)tjsgv_baseaccel);
            if ( tjsgv_samesize )
            {
              v37 = (int64_t)(tjsdf_getrandombits128() * (double)tjsgv_sizewrange + (double)tjsgv_basesizew);
              v32[12] = v37;
              v32[11] = v37;
            }
            else
            {
              v32[11] = (int64_t)(tjsdf_getrandombits128() * (double)tjsgv_sizewrange + (double)tjsgv_basesizew);
              v32[12] = (int64_t)(tjsdf_getrandombits128() * (double)tjsgv_sizehrange + (double)tjsgv_basesizeh);
            }
            v32[6] = v15;
            v32[8] = v15;
            v32[17] = 1;
            --v29;
            ++dword_10012A4C;
            if ( v29 < 1 )
              break;
          }
          v32 += 20;
        }
        v29 = (uint16_t)v29;
      }
      dword_10012A58 = v29;
      v46 = (int *)(v73 + 8);
      for (size_t i = 0; i < v30; i += 1)
      {
        if ( v46[17] )
        {
          v47 = v46[6];
          v49 = v15 - v47;
          if ( v49 <= v46[4] )
          {
            v52 = v46[0];
            if ( tjsgv_windvelocity >= 0 )
            {
              if ( v52 < tjsgv_windvelocity )
                v46[0] = v52 + v79 * tjsgdv_relatedwindvelocity;
            }
            else if ( v52 > tjsgv_windvelocity )
            {
              v46[0] = v52 + v79 * tjsgdv_relatedwindvelocity;
            }
            v53 = v46[12];
            v54 = v46[2] + v79 * v46[0];
            v76 = v46[12];
            v46[2] = (uint16_t)v54;
            v55 = (v54 >> 16) + v46[-2];
            v56 = v79 * v46[1];
            v57 = v46[3];
            v46[-2] = v55;
            v58 = v57 + v56;
            v59 = v55;
            LOWORD(v57) = v58;
            v60 = (v58 >> 16) + v46[-1];
            v61 = v46[11];
            v46[3] = (uint16_t)v57;
            v62 = v91;
            v46[-1] = v60;
            v63 = v60;
            v74 = v61;
            if ( v59 >= 0 )
            {
              if ( v59 < v62 )
              {
                if ( v59 > v62 - v61 )
                  v74 = v62 - v59;
              }
              else
              {
                v59 -= v62;
              }
            }
            else
            {
              v59 += v62;
              if ( v61 + v59 >= v62 )
              {
                v61 = v62 - v59;
                v74 = v62 - v59;
              }
              if ( v61 < 1 )
                goto LABEL_103;
            }
            if ( v60 >= 0 )
            {
              if ( v60 < v92 )
              {
                if ( v60 >= v92 - v53 )
                {
                  v53 = v92 - v60;
                  v76 = v92 - v60;
                }
                goto LABEL_90;
              }
            }
            else
            {
              v53 += v60;
              v76 = v53;
              if ( v53 >= 1 )
              {
                v63 = 0;
LABEL_90:
                v93 = dword_10012A78[((signed int)((v49 << 15) / v46[4]) >> 7) & 0xFF];
                v64 = (char *)(v89 * v63 + v88 + 4 * v59);
                for (size_t i = 0; i < v53; i += 1)
                {
                  if ( v74 > 0 )
                  {
                    memset32(v64, v93, v74);
                    v53 = v76;
                  }
                  v64 += v89;
                }
                v46[13] = v59;
                v46[14] = v63;
                v46[15] = v74;
                v46[16] = v53;
                if ( v82 > v59 )
                  v82 = v59;
                if ( v80 > v63 )
                  v80 = v63;
                v65 = v74 + v59;
                if ( v81 < v65 )
                  v81 = v65;
                v66 = v53 + v63;
                if ( v83 < v66 )
                  v83 = v66;
                goto LABEL_103;
              }
            }
          }
          else
          {
            v46[17] = 0;
            --dword_10012A4C;
          }
        }
LABEL_103:
        v46 += 20;
      }
      //UPDATE: tjsgv_inlayer v82 v80 (v81 - v82) (v83 - v80)
      if ( (double)v79 * tjsgv_windvariancecapability > tjsdf_getrandombits128() )
      {
        v71 = tjsdf_getrandombits128();
        tjsgv_windvelocity = (int64_t)((v71 * tjsgv_maxwindvelocity + v71 * tjsgv_maxwindvelocity - tjsgv_maxwindvelocity) * 65535.0);
        tjsgdv_relatedwindvelocity = tjsgv_windvelocity >> 11;
      }
      tjsgdv_curtick = v15;
    }
  }
  return TJS_S_OK;
}

//----- (10001EC0) --------------------------------------------------------
signed int tjsf_initFireSpark(int a2, int a3, int a4, int this, int flag, int membername, int hint, int result, signed int numparams, int *param)
{
  int v15; // esi
  int v42; // ebp
  int v43; // edi
  int v47; // eax
  int v51; // esi
  char v55; // bl
  int v58; // eax
  double v61; // st7
  double v63; // st6
  signed int v64; // esi
  double v65; // st5
  int v66; // ebx
  char *v67; // eax
  uint32_t *v69; // eax
  char *v73; // [esp+B8h] [ebp-30h]
  char *v77; // [esp+C4h] [ebp-24h]
  int v79; // [esp+C8h] [ebp-20h]
  char v80; // [esp+D0h] [ebp-18h]
  char v81; // [esp+D4h] [ebp-14h]
  char v82; // [esp+D8h] [ebp-10h]
  int x; // ???

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( numparams < 1 )
    return TJS_E_BADPARAMCOUNT;
  if ( tjsgdv_firesparkactive )
  {
    free(tjsgdv_firesparkbuffer);
    tjsgdv_firesparkactive = 0;
  }
  tjsgv_inlayer = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  v15 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[1]);
  ("tTJSVariant::tTJSVariant()")(&v82);
  if ( !FIND_AND_GET_MEMBER(L"imageWidth", tjsgv_inlayer, &v82) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.imageWidth failed.");
  }
  tjsgv_inlayerwidth = ("tTJSVariant::operator tjs_int() const")(&v82);
  if ( !FIND_AND_GET_MEMBER(L"imageHeight", tjsgv_inlayer, &v82) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.imageHeight failed.");
  }
  tjsgv_inlayerheight = ("tTJSVariant::operator tjs_int() const")(&v82);
  if ( !FIND_AND_GET_MEMBER(L"mainImageBufferForWrite", tjsgv_inlayer, &v82) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.mainImageBufferForWrite failed.");
  }
  v77 = &v82;
  tjsgv_inlayerbuffer = ("tTJSVariant::operator tjs_int() const")();
  if ( !FIND_AND_GET_MEMBER(L"mainImageBufferPitch", tjsgv_inlayer, &v82) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.mainImageBufferPitch failed.");
  }
  v73 = &v81;
  tjsgv_inlayerpitch = ("tTJSVariant::operator tjs_int() const")();
  tjsgdv_fillcolor = *(uint32_t *)tjsgv_inlayerbuffer;
  if ( v15 )
  {
    if ( FIND_AND_GET_MEMBER(L"maxgen", v15, &v80) )
    {
      if ( ("tTJSVariant::operator tjs_int() const")(&v80) >= 500 )
      {
        tjsgv_maxgen = 500;
      }
      else
      {
        tjsgv_maxgen = ("tTJSVariant::operator tjs_int() const")(&v80);
      }
    }
    if ( FIND_AND_GET_MEMBER(L"basevelocity", v15, &v80) )
    {
      tjsgv_basevelocity = (int64_t)("tTJSVariant::operator tTVReal() const")(&v80) * 65535.0;
    }
    if ( FIND_AND_GET_MEMBER(L"velocityrange", v15, &v80) )
    {
      tjsgv_velocityrange = (int64_t)("tTJSVariant::operator tTVReal() const")(&v80) * 65535.0;
    }
    if ( FIND_AND_GET_MEMBER(L"baselifetime", v15, &v80) )
    {
      tjsgv_baselifetime = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"lifetimerange", v15, &v80) )
    {
      tjsgv_lifetimerange = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"baseaccel", v15, &v80) )
    {
      tjsgv_baseaccel = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"accelrange", v15, &v80) )
    {
      tjsgv_accelrange = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"directionbase", v15, &v80) )
    {
      tjsgv_directionbase = ("tTJSVariant::operator tTVReal() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"directionrange", v15, &v80) )
    { 
      tjsgv_directionrange = ("tTJSVariant::operator tTVReal() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"basetop", v15, &v80) )
    {
      tjsgv_basetop = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"heightrange", v15, &v80) )
    {
      tjsgv_heightrange = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"basesizew", v15, &v80) )
    {
      tjsgv_basesizew = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"sizewrange", v15, &v80) )
    {
      tjsgv_sizewrange = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"basesizeh", v15, &v80) )
    {
      tjsgv_basesizeh = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"sizehrange", v15, &v80) )
    {
      tjsgv_sizehrange = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"windvelocity", v15, &v80) )
    {
      tjsgv_windvelocity = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"windvariancecapability", v15, &v80) )
    {
      tjsgv_windvariancecapability = ("tTJSVariant::operator tTVReal() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"maxwindvelocity", v15, &v80) )
    {
      tjsgv_maxwindvelocity = ("tTJSVariant::operator tTVReal() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"samesize", v15, &v80) )
    {
      tjsgv_samesize = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"colortable", v15, &v80) )
    {
      ("tTJSString::tTJSString(const tTJSVariant &)")((signed int *)x, &v80);
      ("tTJSString::tTJSString()")(x);
      v42 = ("tjs_int tTJSString::length() const")(x);
      v43 = 0;
      for (size_t i = 0; i < v42; i += 1)
      {
        if ( ("tjs_char tTJSString::operator [](tjs_uint) const")(x, i) == 44 )
        {
          v47 = ("tjs_int64 tTJSString::AsInteger() const")(x);
          if ( v47 < 0 )
          {
            if ( v43 % 3 >= 3 )
              byte_1000F084[v43] = HIBYTE(tjsgdv_fillcolor);
            else
              byte_1000F084[v43] = (unsigned int)tjsgdv_fillcolor >> (16 - 8 * (v43 % 3));
          }
          else
          {
            byte_1000F084[v43] = v47;
          }
          ("tTJSString & tTJSString::operator =(const tjs_nchar *)")(x, "\0");
        }
        else if ( ("tjs_char tTJSString::operator [](tjs_uint) const")(x, i) != 32 )
        {
          v51 = ("tjs_char tTJSString::operator [](tjs_uint) const")(x, i);
          ("void tTJSString::operator +=(tjs_char)")(x, v51);
        }
      }
      ("tTJSString::tTJSString(const tjs_nchar *)")(x, "\0");
      v55 = ("bool tTJSString::operator !=(const tTJSString &) const")(x);
      ("tTJSString::~ tTJSString()")();
      if ( v55 )
      {
        v58 = ("tjs_int64 tTJSString::AsInteger() const")(x);
        if ( v58 < 0 )
        {
          if ( v43 % 3 >= 3 )
            byte_1000F084[v43] = HIBYTE(tjsgdv_fillcolor);
          else
            byte_1000F084[v43] = (unsigned int)tjsgdv_fillcolor >> (16 - 8 * (v43 % 3));
        }
        else
        {
          byte_1000F084[v43] = v58;
        }
        ++v43;
      }
      dword_1000F0A4 = v43 / 3;
      ("tTJSString::~ tTJSString()")();
      ("tTJSString::~ tTJSString()")(x);
    }
  }
  v61 = 0.0;
  v63 = (double)(dword_1000F0A4 - 1);
  dword_10012E98 = (tjsgv_maxgen << 16) / (tjsgv_baselifetime + tjsgv_lifetimerange);
  for (int *i = dword_10012A78; (size_t)i < (size_t)&tjsgdv_firesparkbuffer; i += 1)
  {
    v64 = (int64_t)(v63 * v61);
    v65 = v63 * v61 - (double)v64;
    v66 = (uint8_t)byte_1000F084[3 * v64];
    v79 = (uint8_t)byte_1000F084[(3 * v64) + 3] - v66;
    i[-1] = ((uint8_t)byte_1000F084[(3 * v64) + 2]
                + (unsigned int)(int64_t)((double)((uint8_t)byte_1000F084[(3 * v64) + 5]
                                                        - (uint8_t)byte_1000F084[(3 * v64) + 2])
                                               * v65)) | ((v66 + (unsigned int)(int64_t)((double)v79 * v65)) << 16) | (((uint8_t)byte_1000F084[(3 * v64) + 1] + (unsigned int)(int64_t)((double)((uint8_t)byte_1000F084[(3 * v64) + 4] - (uint8_t)byte_1000F084[(3 * v64) + 1]) * v65)) << 8);
    v61 += 0.00390625;
  }
  v67 = (char *)operator new(0x9C40u);
  tjsgdv_firesparkbuffer = (int)v67;
  v69 = v67 + 76;
  for (size_t i = 0; i < tjsgv_maxgen; i += 1)
  {
    v69[0] = 0;
    v69 += 20;
  }
  dword_10012A4C = 0;
  dword_10012A58 = 0;
  tjsgdv_curtick = ("tjs_uint64 ::TVPGetTickCount()")();
  tjsgdv_firesparkactive = 1;
  tjsgv_pause = 0;
  ("tTJSVariant::~ tTJSVariant()")(&v80);
  return TJS_S_OK;
}

//----- (10002DB0) --------------------------------------------------------
signed int tjsf_finishFireSpark(int this, int flag, int membername)
{
  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( tjsgdv_firesparkactive )
  {
    free(tjsgdv_firesparkbuffer);
    tjsgdv_firesparkactive = 0;
  }
  return TJS_S_OK;
}

//----- (10002DE0) --------------------------------------------------------
signed int tjsf_changeFireSpark(int a1, int this, int flag, int membername, int hint, int result, signed int numparams, int *param)
{
  int v11; // esi
  char *v32; // [esp+34h] [ebp-1Ch]
  int v33; // [esp+38h] [ebp-18h]
  char v34; // [esp+3Ch] [ebp-14h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( numparams < 1 )
    return TJS_E_BADPARAMCOUNT;
  if ( tjsgdv_firesparkactive )
  {
    v11 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
    ("tTJSVariant::tTJSVariant()")();
    if ( FIND_AND_GET_MEMBER(L"maxgen", v11, &v33) )
    {
      if ( ("tTJSVariant::operator tjs_int() const")(&v33) >= 500 )
      {
        tjsgdv_maxgennew = 500;
      }
      else
      {
        tjsgdv_maxgennew = ("tTJSVariant::operator tjs_int() const")(&v33);
      }
      if ( !FIND_AND_GET_MEMBER(L"transtime", v11, &v33) )
      {
        tjsgv_transtime = 5000;
      }
      else
      {
        tjsgv_transtime = ("tTJSVariant::operator tjs_int() const")(&v33);
      }
      tjsgdv_maxgenold = tjsgv_maxgen;
      tjsgdv_curtime = ("tjs_uint64 ::TVPGetTickCount()")();
      tjsgdv_istransitioning = 1;
    }
    if ( FIND_AND_GET_MEMBER(L"pause", v11, &v33) )
    {
      tjsgv_pause = ("tTJSVariant::operator tjs_int() const")(&v33) != 0;
    }
    if ( FIND_AND_GET_MEMBER(L"effect", v11, &v33) )
    {
      tjsgv_effect = ("tTJSVariant::operator tjs_int() const")(&v33) != 0;
    }
    if ( FIND_AND_GET_MEMBER(L"layer", v11, &v33) )
    {
      tjsgv_inlayer = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(&v33);
      if ( !FIND_AND_GET_MEMBER(L"imageWidth", tjsgv_inlayer, &v34) )
      {
        ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.imageWidth failed.");
      }
      tjsgv_inlayerwidth = ("tTJSVariant::operator tjs_int() const")(&v34);
      if ( !FIND_AND_GET_MEMBER(L"imageHeight", tjsgv_inlayer, &v34) )
      {
        ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.imageHeight failed.");
      }
      tjsgv_inlayerheight = ("tTJSVariant::operator tjs_int() const")(&v34);
      if ( !FIND_AND_GET_MEMBER(L"mainImageBufferForWrite", tjsgv_inlayer, &v34) )
      {
        ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.mainImageBufferForWrite failed.");
      }
      tjsgv_inlayerbuffer = ("tTJSVariant::operator tjs_int() const")(&v34);
      if ( !FIND_AND_GET_MEMBER(L"mainImageBufferPitch", tjsgv_inlayer, &v34) )
      {
        ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.mainImageBufferPitch failed.");
      }
      tjsgv_inlayerpitch = ("tTJSVariant::operator tjs_int() const")(&v34);
    }
    ("tTJSVariant::~ tTJSVariant()")(&v33);
  }
  return TJS_S_OK;
}
