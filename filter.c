#include <windows.h>
#include <math.h>
#include <defs.h>


//-------------------------------------------------------------------------
// Function declarations

// int sub_10001000(uint32_t a1, uint32_t a2, uint32_t **a4, int a5, int a6);
int32_t sub_10001A70(int *a1, uint32_t *a2, uint32_t *a3, uint32_t *a4, uint32_t *a5);
int64_t sub_10001CB0();
void sub_10001D10();
int32_t *sub_10001D20(int a1, int32_t *a2, double a3, int a4, int a5, int a6);
// double *tjsdf_parsewaves(int a1, double a2);
int sub_100027C0(uint32_t *a1, uint32_t *a2, int a3, int a4);
int sub_100029C0(uint32_t *a1, uint32_t *a2, int a3, int a4);
// int32_t tjsf_Smudge(int a1, int a3, int a4, int a5, int a6, int a7, int32_t a8, uint32_t *a9, uint32_t a10);
// int32_t tjsf_Blur(int a1, int a3, int a4, int a5, int a6, int a7, int32_t a8, uint32_t *a9, uint32_t a10);
// int32_t tjsf_Lens(double a1, int a2, int a3, int a4, int *a5, int a6, int32_t a7, int *a8);
int32_t tjsf_InitLens(int a1, int a2, int a3);
int32_t tjsf_ReleaseLens(int a1, int a2, int a3);
int32_t tjsf_Noise(int a1, int a2, int32_t a3, int a4, int a5, int a6, int *a7);
int32_t tjsf_Contrast(int a1, int a2, int a3, int a4, int a5, int a6, int a7);
// int32_t tjsf_initHaze(int a1, double a2, int a3, int a4, int a5, int a6, int a7, int a8, int *a9);
// int32_t tjsf_doHaze(double a1, int a2, int a3, int a4, int a5, int a6, int a7, int *a8);
int32_t tjsf_endHaze(int a1, int a2, int a3);
// int32_t tjsf_Stretch(int a1, char a2, int a3, int a4, int a5, int a6, int a7, int *a8);
// int32_t tjsf_Vortex(double a1, int a2, int a3, int a4, int a5, int a6, int a7, int *a8);

//-------------------------------------------------------------------------
// Data declarations

int tjsgdv_current_seed = 0; // weak
uint32_t * tjsgdv_lenstable = NULL; // idb
int tjsgv_time = 0; // weak
int tjsgv_intime = 0; // weak
int tjsgv_outtime = 0; // weak
double tjsgv_speed = 0.0; // weak
double tjsgv_cycle = 0.0; // weak
LPVOID tjsgv_waves = NULL; // idb
LPVOID tjsgv_lwaves = 0; // weak
int tjsgv_upper = 0; // weak
int tjsgv_center = 0; // weak
int tjsgv_lower = 0; // weak
int tjsgv_upperpow = 0; // weak
int tjsgv_centerpow = 0; // weak
int tjsgv_lowerpow = 0; // weak
int tjsgdv_hazeenabled = 0; // weak

//----- (10001000) --------------------------------------------------------
int sub_10001000(uint32_t a1, uint32_t a2, uint32_t **a4, int a5, int a6)
{
  uint32_t *v6; // ebx
  uint32_t v8; // edi
  uint32_t *v10; // esi
  uint32_t v11; // edx
  uint32_t *v12; // ecx
  uint32_t *v13; // edx
  uint32_t *v14; // esi
  uint32_t v20; // ebx
  int v21; // esi
  uint32_t v22; // esi
  uint32_t tjslv_dtop; // eax
  uint32_t v24; // esi
  uint32_t v25; // ebx
  uint32_t *v37; // esi
  uint32_t v39; // ebx
  uint32_t v40; // edx
  int *v41; // esi
  int v42; // edx
  int v43; // ebx
  bool v44; // zf
  int32_t v45; // eax
  int *v46; // edx
  uint32_t v47; // ebx
  int v48; // eax
  int v49; // eax
  uint32_t v50; // ecx
  int *v51; // eax
  uint32_t v52; // esi
  uint32_t v53; // ebx
  uint32_t *v54; // eax
  uint32_t v55; // edx
  uint32_t *v56; // ecx
  uint32_t v57; // esi
  uint32_t v58; // ebx
  uint32_t v59; // edi
  int *v60; // eax
  int v61; // edi
  int v62; // esi
  uint32_t *v63; // ecx
  int32_t v64; // edx
  int *v65; // edi
  uint32_t v66; // edi
  int v67; // edx
  uint32_t v68; // esi
  uint32_t v69; // edx
  int v70; // esi
  int *v71; // edi
  int v72; // eax
  uint32_t v73; // edi
  uint32_t v74; // esi
  int result; // eax
  int v76[2]; // [esp+Ch] [ebp-3Ch]
  uint32_t v77; // [esp+14h] [ebp-34h]
  uint32_t v78; // [esp+18h] [ebp-30h]
  uint32_t *v79; // [esp+1Ch] [ebp-2Ch]
  uint32_t v80; // [esp+20h] [ebp-28h]
  uint32_t v81; // [esp+24h] [ebp-24h]
  uint32_t v82; // [esp+28h] [ebp-20h]
  uint32_t v83; // [esp+2Ch] [ebp-1Ch]
  int v84; // [esp+30h] [ebp-18h]
  uint32_t v85; // [esp+34h] [ebp-14h]
  uint32_t v86; // [esp+38h] [ebp-10h]
  uint32_t *v87; // [esp+3Ch] [ebp-Ch]
  uint32_t v88; // [esp+40h] [ebp-8h]
  uint32_t v89; // [esp+44h] [ebp-4h]
  uint32_t v90; // [esp+50h] [ebp+8h]
  uint32_t v92; // [esp+54h] [ebp+Ch]
  uint32_t v93; // [esp+54h] [ebp+Ch]
  int32_t v94; // [esp+54h] [ebp+Ch]
  uint32_t v95; // [esp+54h] [ebp+Ch]
  uint32_t v96; // [esp+54h] [ebp+Ch]
  uint32_t v97; // [esp+54h] [ebp+Ch]
  uint32_t *v98; // [esp+58h] [ebp+10h]
  uint32_t v99; // [esp+58h] [ebp+10h]
  uint32_t v101; // [esp+58h] [ebp+10h]
  uint32_t v102; // [esp+58h] [ebp+10h]
  uint32_t v103; // [esp+58h] [ebp+10h]
  uint32_t v104; // [esp+58h] [ebp+10h]
  uint32_t v105; // [esp+58h] [ebp+10h]

  v6 = (uint32_t *)a2;
  v84 = a5 - 2;
  v8 = a1;
  v85 = a1;
  v86 = a2;
  v80 = a6 - 2;
  v10 = a4[0];
  v10[0] = v6[0];
  v81 = v6[v8 + 1];
  v11 = v6[0];
  v12 = v6;
  v82 = 4 * v8;
  v79 = &v6[v8];
  v92 = v79[0];
  v88 = (uint32_t)(v6 + 1);
  v78 = v6[1];
  v83 = (((v81 >> 3) & 0x1FE01FE0)
       + ((v92 >> 3) & 0x1FE01FE0)
       + ((v78 >> 3) & 0x1FE01FE0)
       + 5 * ((v11 >> 3) & 0x1FE01FE0)) & 0xFF00FF00;
  ((uint32_t *)v86)[0] = (32 * ((v81 & 0xFF00FF) + (v92 & 0xFF00FF) + (v78 & 0xFF00FF) + 5 * (v11 & 0xFF00FF)) >> 8) & 0xFF00FF | v83;
  v13 = (uint32_t *)v88;
  v14 = v10 + 1;
  v87 = v6;
  for (size_t i = 0; i < v84; i += 1)
  {
    v14[0] = v13[0];
    v81 = v13[v8 - 1];
    v88 = v13[v8 + 1];
    v20 = v13[v8];
    v83 = v13[0];
    v21 = (int)(v14 + 1);
    v98 = (uint32_t *)v21;
    v93 = (uint32_t *)(v21)[-2];
    v89 = v13[1];
    v22 = (((v88 >> 3) & 0x1FE01FE0)
         + ((v89 >> 3) & 0x1FE01FE0)
         + ((v93 >> 3) & 0x1FE01FE0)
         + ((v81 >> 3) & 0x1FE01FE0)
         + ((v20 >> 3) & 0x1FE01FE0)
         + 3 * ((v83 >> 3) & 0x1FE01FE0)) & 0xFF00FF00;
    v78 = v22;
    tjslv_dtop = (32
         * ((v88 & 0xFF00FF)
          + (v89 & 0xFF00FF)
          + (v93 & 0xFF00FF)
          + (v81 & 0xFF00FF)
          + (v20 & 0xFF00FF)
          + 3 * (v83 & 0xFF00FF)) >> 8) & 0xFF00FF | v22;
    v14 = v98;
    v13[0] = tjslv_dtop;
    ++v13;
  }
  v14[0] = v13[0];
  v99 = v13[v82 / 4];
  v24 = v14[-1];
  v81 = v13[v82 / 4 - 1];
  v25 = v13[0];
  v78 = (((v99 >> 3) & 0x1FE01FE0)
       + ((v81 >> 3) & 0x1FE01FE0)
       + ((v24 >> 3) & 0x1FE01FE0)
       + 5 * ((*v13 >> 3) & 0x1FE01FE0)) & 0xFF00FF00;
  v13[0] = (32 * ((v99 & 0xFF00FF) + (v81 & 0xFF00FF) + (v24 & 0xFF00FF) + 5 * (v25 & 0xFF00FF)) >> 8) & 0xFF00FF | v78;
  for (size_t i = 0; i < v80; i += 1)
  {
    v12 = v79;
    v37 = a4[0];
    v37[0] = v79[0];
    v39 = v12[1];
    v79 = &v12[v82 / 4];
    v76[0] = a4[0][0];
    v81 = v12[v8 + 1];
    v101 = v37[1];
    v80 = v12[v82 / 4];
    v40 = v12[0];
    v41 = (int *)(v37 + 1);
    v77 = ((((uint32_t)v76[0] >> 3) & 0x1FE01FE0)
         + ((v101 >> 3) & 0x1FE01FE0)
         + ((v81 >> 3) & 0x1FE01FE0)
         + ((v80 >> 3) & 0x1FE01FE0)
         + ((v39 >> 3) & 0x1FE01FE0)
         + 3 * ((v12[0] >> 3) & 0x1FE01FE0)) & 0xFF00FF00;
    v42 = (v80 & 0xFF00FF) + (v39 & 0xFF00FF) + 3 * (v40 & 0xFF00FF);
    v43 = v84;
    v44 = v84 == 0;
    v12[0] = (32 * ((v76[0] & 0xFF00FF) + (v101 & 0xFF00FF) + (v81 & 0xFF00FF) + v42) >> 8) & 0xFF00FF | v77;
    v45 = 1;
    v87 = v12;
    v46 = (int *)(v12 + 1);
    v94 = 1;
    if ( !v44 )
    {
      for (size_t i = 0; i < v43; i += 1)
      {
        v76[v45] = v41[0];
        v41[0] = v46[0];
        v85 = v76[v45];
        v47 = v46[v8 - 1];
        v95 = v41[-1];
        v89 = v46[1];
        v102 = v41[1];
        v77 = v47;
        ++v41;
        v48 = ((uint8_t)v45 - 1) & 1;
        v81 = v48;
        v86 = v76[v48];
        v88 = v46[v8 + 1];
        v49 = v46[v8];
        v83 = (((v88 >> 3) & 0x1FE01FE0)
             + ((v89 >> 3) & 0x1FE01FE0)
             + ((v95 >> 3) & 0x1FE01FE0)
             + ((v102 >> 3) & 0x1FE01FE0)
             + ((v85 >> 3) & 0x1FE01FE0)
             + ((v86 >> 3) & 0x1FE01FE0)
             + ((v47 >> 3) & 0x1FE01FE0)
             + (((uint32_t)v46[v8] >> 3) & 0x1FE01FE0)) & 0xFF00FF00;
        v50 = (32
             * ((v88 & 0xFF00FF)
              + (v89 & 0xFF00FF)
              + (v95 & 0xFF00FF)
              + (v102 & 0xFF00FF)
              + (v85 & 0xFF00FF)
              + (v86 & 0xFF00FF)
              + (v47 & 0xFF00FF)
              + (v49 & 0xFF00FF)) >> 8) & 0xFF00FF | v83;
        v45 = v81;
        v46[0] = v50;
        ++v46;
      }
      v12 = v87;
      v94 = v45;
    }
    v51 = &v76[v45];
    v51[0] = v41[0];
    v77 = (uint32_t)v51;
    v41[0] = v46[0];
    v86 = v76[((uint8_t)v94 - 1) & 1];
    v52 = v41[-1];
    v53 = v46[v82 / 4 - 1];
    v103 = v46[v82 / 4];
    v85 = ((uint32_t *)v77)[0];
    v83 = v46[0];
    v77 = (((v103 >> 3) & 0x1FE01FE0)
         + ((v52 >> 3) & 0x1FE01FE0)
         + ((v85 >> 3) & 0x1FE01FE0)
         + ((v86 >> 3) & 0x1FE01FE0)
         + ((v53 >> 3) & 0x1FE01FE0)
         + 3 * ((v83 >> 3) & 0x1FE01FE0)) & 0xFF00FF00;
    v46[0] = (32
          * ((v103 & 0xFF00FF)
           + (v52 & 0xFF00FF)
           + (v85 & 0xFF00FF)
           + (v86 & 0xFF00FF)
           + (v53 & 0xFF00FF)
           + 3 * (v83 & 0xFF00FF)) >> 8) & 0xFF00FF | v77;
  }
  v54 = a4[0];
  v55 = v12[v82 / 4];
  v56 = &v12[v82 / 4];
  v57 = a4[0][0];
  v54[0] = v55;
  v58 = v56[1];
  v59 = v56[0];
  v77 = (uint32_t)(v56 + 1);
  v104 = v54[1];
  v60 = (int *)(v54 + 1);
  v90 = (((v57 >> 3) & 0x1FE01FE0)
       + ((v104 >> 3) & 0x1FE01FE0)
       + ((v58 >> 3) & 0x1FE01FE0)
       + 5 * ((v59 >> 3) & 0x1FE01FE0)) & 0xFF00FF00;
  v76[0] = v57;
  v61 = (v57 & 0xFF00FF) + (v104 & 0xFF00FF) + (v58 & 0xFF00FF) + 5 * (v59 & 0xFF00FF);
  v62 = v84;
  v44 = v84 == 0;
  v56[0] = ((uint32_t)(32 * v61) >> 8) & 0xFF00FF | v90;
  v63 = (uint32_t *)v77;
  v64 = 1;
  if ( !v44 )
  {
    for (size_t i = 0; i < v62; i += 1)
    {
      v65 = &v76[v64];
      v65[0] = v60[0];
      v66 = v65[0];
      v60[0] = v63[0];
      v96 = v60[-1];
      v89 = v63[1];
      v105 = v60[1];
      ++v60;
      v67 = ((uint8_t)v64 - 1) & 1;
      v68 = v76[v67];
      v81 = v67;
      v69 = v63[0];
      v83 = v69;
      v77 = (((v89 >> 3) & 0x1FE01FE0)
           + ((v96 >> 3) & 0x1FE01FE0)
           + ((v105 >> 3) & 0x1FE01FE0)
           + ((v66 >> 3) & 0x1FE01FE0)
           + ((v68 >> 3) & 0x1FE01FE0)
           + 3 * ((v69 >> 3) & 0x1FE01FE0)) & 0xFF00FF00;
      v63[0] = (32
            * ((v89 & 0xFF00FF)
             + (v96 & 0xFF00FF)
             + (v105 & 0xFF00FF)
             + (v66 & 0xFF00FF)
             + (v68 & 0xFF00FF)
             + 3 * (v69 & 0xFF00FF)) >> 8) & 0xFF00FF | v77;
      v64 = v81;
      ++v63;
    }
  }
  v70 = v60[0];
  v71 = &v76[v64];
  v72 = (int)(v60 + 1);
  v71[0] = v70;
  v73 = v71[0];
  ((uint32_t *)(v72))[-2] = v63[0];
  v74 = v76[((uint8_t)v64 - 1) & 1];
  v97 = ((uint32_t *)(v72))[-2];
  result = v97 & 0xFF00FF;
  v63[0] = (32 * ((v97 & 0xFF00FF) + (v73 & 0xFF00FF) + (v74 & 0xFF00FF) + 5 * (v63[0] & 0xFF00FF)) >> 8) & 0xFF00FF | (((v97 >> 3) & 0x1FE01FE0) + ((v73 >> 3) & 0x1FE01FE0) + ((v74 >> 3) & 0x1FE01FE0) + 5 * ((v63[0] >> 3) & 0x1FE01FE0)) & 0xFF00FF00;
  return result;
}

//----- (10001A70) --------------------------------------------------------
int32_t sub_10001A70(int *a1, uint32_t *a2, uint32_t *a3, uint32_t *a4, uint32_t *a5)
{
  char v19; // [esp+24h] [ebp-18h]

  if ( !a1 )
    return 0;
  ("tTJSVariant::tTJSVariant()")(&v19);
  if ( !FIND_AND_GET_MEMBER(L"imageWidth", a1, &v19) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.imageWidth failed.");
  }
  *a2 = ("tTJSVariant::operator tjs_int() const")(&v19);
  if ( !FIND_AND_GET_MEMBER(L"imageHeight", a1, &v19) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.imageHeight failed.");
  }
  *a3 = ("tTJSVariant::operator tjs_int() const")(&v19);
  if ( !FIND_AND_GET_MEMBER(L"mainImageBufferForWrite", a1, &v19) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.mainImageBufferForWrite failed.");
  }
  *a4 = ("tTJSVariant::operator tjs_int() const")(&v19);
  if ( !FIND_AND_GET_MEMBER(L"mainImageBufferPitch", a1, &v19) )
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"invoking of Layer.mainImageBufferPitch failed.");
  }
  *a5 = ("tTJSVariant::operator tjs_int() const")(&v19);
  ("tTJSVariant::~ tTJSVariant()")(&v19);
  return 1;
}

//----- (10001CB0) --------------------------------------------------------
int64_t sub_10001CB0()
{
  tjsgdv_lenstable = malloc(0x8000u);
  for (size_t i = 0; i < 0x2000; i += 1)
  {
    tjsgdv_lenstable[i] = (int64_t)(tan(asin((double)i * 0.0001220852154804053) * 0.5) * 65535.0);
  }
  return 0;
}

//----- (10001D10) --------------------------------------------------------
void sub_10001D10()
{
  free(tjsgdv_lenstable);
}

//----- (10001D20) --------------------------------------------------------
int32_t *sub_10001D20(int src_width, int32_t *dest_width, double zoom, int power, int unused1, int unused2)
{
  int32_t *v6; // ebp
  int v7; // esi
  long double v8; // st7
  int32_t *result; // eax
  int32_t v10; // edx
  uint32_t *v11; // edi
  int v12; // ebx
  int v13; // eax
  int32_t v14; // ecx
  int tjslv_src; // ebp
  long double v16; // st6
  int64_t v17; // rax
  int v19; // ecx
  int v20; // eax
  int v21; // ecx
  int v22; // edi
  int tjslv_dtop; // edx
  int v24; // ecx
  uint32_t v25; // ebp
  uint32_t *v26; // ecx
  int v27; // ebx
  int v28; // ebx
  long double v29; // st6
  int64_t v30; // rax
  int v32; // ebx
  int v33; // eax
  int v34; // ebp
  int v35; // ecx
  int v36; // edi
  int v37; // ST20_4
  int v38; // ebx
  int *v39; // ebp
  uint32_t v40; // edi
  int v41; // edx
  uint32_t v42; // ST10_4
  uint32_t v43; // ST14_4
  int v44; // eax
  int v45; // ST38_4
  int v46; // ebx
  int v47; // ebp
  int v48; // edx
  uint32_t *v49; // edi
  int v50; // ecx
  int32_t v51; // ebp
  int v52; // ebx
  long double v53; // st6
  int64_t v54; // rax
  int v56; // ebx
  int v57; // ecx
  int v58; // eax
  int v59; // edx
  int v60; // edi
  uint32_t *v61; // ebp
  int v62; // ebx
  long double v63; // st6
  int64_t v64; // rax
  int v66; // ecx
  int v67; // edi
  int v68; // ebp
  int v69; // ST20_4
  int v70; // edi
  uint32_t *v71; // ebp
  int32_t v72; // [esp+10h] [ebp-38h]
  int32_t v73; // [esp+10h] [ebp-38h]
  int32_t v74; // [esp+10h] [ebp-38h]
  int32_t v75; // [esp+10h] [ebp-38h]
  uint32_t *v76; // [esp+14h] [ebp-34h]
  uint32_t *v77; // [esp+14h] [ebp-34h]
  int v78; // [esp+18h] [ebp-30h]
  uint32_t *v79; // [esp+20h] [ebp-28h]
  int32_t v80; // [esp+24h] [ebp-24h]
  int v81; // [esp+28h] [ebp-20h]
  int v82; // [esp+28h] [ebp-20h]
  int32_t *v83; // [esp+2Ch] [ebp-1Ch]
  int v84; // [esp+30h] [ebp-18h]
  int v85; // [esp+30h] [ebp-18h]
  int v86; // [esp+3Ch] [ebp-Ch]
  int v87; // [esp+3Ch] [ebp-Ch]
  uint32_t v88; // [esp+40h] [ebp-8h]
  int32_t v89; // [esp+4Ch] [ebp+4h]
  int32_t v90; // [esp+4Ch] [ebp+4h]

  v78 = ((uint32_t *)src_width)[2];
  v6 = dest_width;
  v88 = (uint32_t)dest_width[3] >> 2;
  v7 = ((uint32_t *)src_width)[3] >> 2;
  v83 = dest_width;
  v8 = (double)dest_width[0] * zoom * 0.5;
  if ( unused1 < 0 )
    unused1 = dest_width[0] / 2;
  if ( unused2 < 0 )
    unused2 = dest_width[1] / 2;
  result = (int32_t *)unused2;
  v10 = 0;
  v79 = (uint32_t *)v6[2];
  v80 = 0;
  if ( unused2 > 0 )
  {
    v81 = 0;
    do
    {
      v11 = v79;
      v12 = v10 - unused2;
      v13 = v12 * v12;
      v14 = 0;
      v76 = v79;
      v86 = v10 - unused2;
      v84 = v12 * v12;
      v89 = 0;
      if ( unused1 > 0 )
      {
        do
        {
          tjslv_src = v14 - unused1;
          v16 = sqrt((double)(v13 + tjslv_src * tjslv_src));
          if ( v16 < v8 )
          {
            v72 = (int64_t)((double)(tjsgdv_lenstable[v16 / v8 * 8191.0]) * v8);
            if ( v72 )
            {
              v17 = (int64_t)((double)v72 / v16);
              for (size_t i = 0; i < power - 1; i += 1)
              {
                LODWORD(v17) = ((int32_t)v17 >> 1) * ((int32_t)v17 >> 1) >> 14;
              }
              v19 = v17;
              v20 = v12 * (int32_t)v17 >> 8;
              v21 = tjslv_src * v19 >> 8;
              v22 = (uint8_t)(-1 - v21);
              tjslv_dtop = v22 * (uint8_t)(-1 - v20) >> 8;
              v24 = v7 * (unused2 + (v20 >> 8)) + unused1 + (v21 >> 8);
              v25 = ((uint32_t *)(v78 + 4 * v24 + 4))[0];
              v26 = (uint32_t *)(v78 + 4 * v24);
              v27 = (uint8_t)(-1 - v20) - tjslv_dtop;
              v76[0] = ((int32_t)((256 - v22 - v27) * (v26[v7 + 1] & 0xFF00FF)
                                 + (v22 - tjslv_dtop) * (v26[v7] & 0xFF00FF)
                                 + v27 * (v25 & 0xFF00FF)
                                 + tjslv_dtop * (v26[0] & 0xFF00FF)) >> 8) & 0xFF00FF | ((256 - v22 - v27)
                                                                              * ((v26[v7 + 1] >> 8) & 0xFF00FF)
                                                                              + (v22 - tjslv_dtop)
                                                                              * ((v26[v7] >> 8) & 0xFF00FF)
                                                                              + v27 * ((v25 >> 8) & 0xFF00FF)
                                                                              + tjslv_dtop * ((v26[0] >> 8) & 0xFF00FF)) & 0xFF00FF00;
              v11 = v76 + 1;
              v12 = v86;
              ++v76;
            }
            else
            {
              v11[0] = ((uint32_t *)(v78 + 4 * (v89 + v81)))[0];
              ++v11;
              v76 = v11;
            }
            v14 = v89;
          }
          else
          {
            v11[0] = -16777216;
            ++v11;
            v76 = v11;
          }
          v89 = ++v14;
          v13 = v84;
        }
        while (v14 < unused1);
        v6 = v83;
        v10 = v80;
        v13 = v84;
      }
      if ( v14 < v6[0] )
      {
        do
        {
          v28 = v14 - unused1;
          v29 = sqrt((double)(v13 + v28 * v28));
          if ( v29 < v8 )
          {
            v73 = (int64_t)((double)(tjsgdv_lenstable[v29 / v8 * 8191.0]) * v8);
            if ( v73 )
            {
              v30 = (int64_t)((double)v73 / v29);
              for (size_t i = 0; i < power - 1; i += 1)
              {
                LODWORD(v30) = ((int32_t)v30 >> 1) * ((int32_t)v30 >> 1) >> 14;
              }
              v32 = v30 * v28;
              v33 = v86 * (int32_t)v30 >> 8;
              v34 = v32 >> 8;
              v35 = BYTE1(v32) * (uint8_t)(-1 - v33) >> 8;
              v36 = BYTE1(v32) - v35;
              v37 = v36;
              v38 = 256 - v36;
              v39 = (int *)(v78 + 4 * (v7 * (unused2 + (v33 >> 8)) + unused1 + (v34 >> 8)));
              v40 = v39[v7 + 1];
              v41 = (uint8_t)(-1 - v33) - v35;
              v42 = v39[v7];
              v43 = v39[1];
              v44 = v39[0];
              v45 = v41;
              v46 = v38 - v35 - v41;
              v47 = v37 * ((v40 >> 8) & 0xFF00FF)
                  + v46 * ((v42 >> 8) & 0xFF00FF)
                  + v35 * ((v43 >> 8) & 0xFF00FF)
                  + v41 * (((uint32_t)*v39 >> 8) & 0xFF00FF);
              v48 = v35 * (v43 & 0xFF00FF);
              v14 = v89;
              v76[0] = ((int32_t)(v37 * (v40 & 0xFF00FF) + v46 * (v42 & 0xFF00FF) + v48 + v45 * (v44 & 0xFF00FF)) >> 8) & 0xFF00FF | v47 & 0xFF00FF00;
              v6 = v83;
              v11 = v76 + 1;
              ++v76;
            }
            else
            {
              v14 = v89;
              v11[0] = ((uint32_t *)(v78 + 4 * (v89 + v81)))[0];
              ++v11;
              v76 = v11;
            }
          }
          else
          {
            v11[0] = -16777216;
            ++v11;
            v76 = v11;
          }
          v89 = ++v14;
          v13 = v84;
        }
        while (v14 < v6[0]);
        v10 = v80;
      }
      result = (int32_t *)unused2;
      v79 += v88;
      v80 = ++v10;
      v81 += v7;
    }
    while ( v10 < unused2 );
  }
  if ( v10 >= v6[1] )
    return result;
  v82 = v7 * v10;
  do
  {
    v49 = v79;
    v50 = (v10 - unused2) * (v10 - unused2);
    v87 = v10 - unused2;
    v51 = 0;
    v77 = v79;
    v85 = (v10 - unused2) * (v10 - unused2);
    v90 = 0;
    if ( unused1 > 0 )
    {
      while ( 1 )
      {
        v52 = v51 - unused1;
        v53 = sqrt((double)(v50 + v52 * v52));
        if ( v53 >= v8 )
          break;
        v74 = (int64_t)((double)(tjsgdv_lenstable[v53 / v8 * 8191.0]) * v8);
        if ( !v74 )
        {
          v49[0] = ((uint32_t *)(v78 + 4 * (v51 + v82)))[0];
          goto LABEL_45;
        }
        v54 = (int64_t)((double)v74 / v53);
        for (size_t i = 0; i < (power - 1); i += 1)
        {
          LODWORD(v54) = ((int32_t)v54 >> 1) * ((int32_t)v54 >> 1) >> 14;
        }
        v56 = v54 * v52;
        v57 = (int32_t)v54 * v87 >> 8;
        v58 = (uint8_t)(-1 - BYTE1(v56));
        v59 = v58 * (uint8_t)v57 >> 8;
        v60 = v58 - v59;
        v61 = (uint32_t *)(v78 + 4 * (v7 * (unused2 + (v57 >> 8)) + unused1 + (v56 >> 16)));
        v77[0] = ((((uint8_t)v57 - v59) * (v61[v7 + 1] & 0xFF00FF)
               + v59 * (v61[v7] & 0xFF00FF)
               + (256 - (uint8_t)v57 - v60) * (v61[1] & 0xFF00FF)
               + v60 * (v61[0] & 0xFF00FF)) >> 8) & 0xFF00FF | (((uint8_t)v57 - v59)
                                                            * ((v61[v7 + 1] >> 8) & 0xFF00FF)
                                                            + v59 * ((v61[v7] >> 8) & 0xFF00FF)
                                                            + (256 - (uint8_t)v57 - v60)
                                                            * ((v61[1] >> 8) & 0xFF00FF)
                                                            + v60 * ((v61[0] >> 8) & 0xFF00FF)) & 0xFF00FF00;
        v51 = v90;
        v49 = v77 + 1;
        ++v77;
LABEL_46:
        v90 = ++v51;
        if ( v51 >= unused1 )
        {
          v10 = v80;
          v50 = v85;
          goto LABEL_48;
        }
        v50 = v85;
      }
      v49[0] = -16777216;
LABEL_45:
      ++v49;
      v77 = v49;
      goto LABEL_46;
    }
LABEL_48:
    result = v83;
    if ( v51 >= v83[0] )
      goto LABEL_61;
    while ( 1 )
    {
      v62 = v51 - unused1;
      v63 = sqrt((double)(v50 + v62 * v62));
      if ( v63 >= v8 )
      {
        v49[0] = -16777216;
LABEL_58:
        ++v49;
        v77 = v49;
        goto LABEL_59;
      }
      v75 = (int64_t)((double)(tjsgdv_lenstable[v63 / v8 * 8191.0]) * v8);
      if ( !v75 )
      {
        v49[0] = ((uint32_t *)(v78 + 4 * (v51 + v82)))[0];
        goto LABEL_58;
      }
      v64 = (int64_t)((double)v75 / v63);
      for (size_t i = 0; i < (power - 1); i += 1)
      {
        LODWORD(v64) = ((int32_t)v64 >> 1) * ((int32_t)v64 >> 1) >> 14;
      }
      v66 = (int32_t)v64 * v87 >> 8;
      v67 = (uint8_t)((uint16_t)(v64 * v87) >> 8);
      HIDWORD(v64) = (uint8_t)((uint16_t)(v64 * v87) >> 8);
      v68 = (int32_t)v64 * v62 >> 8;
      LODWORD(v64) = (uint8_t)((uint16_t)(v64 * v62) >> 8);
      HIDWORD(v64) = (int32_t)v64 * HIDWORD(v64) >> 8;
      v69 = v67 - HIDWORD(v64);
      v70 = v64 - HIDWORD(v64);
      v71 = (uint32_t *)(v78 + 4 * (v7 * (unused2 + (v66 >> 8)) + unused1 + (v68 >> 8)));
      v77[0] = ((HIDWORD(v64) * (v71[v7 + 1] & 0xFF00FF)
             + v69 * (v71[v7] & 0xFF00FF)
             + v70 * (v71[1] & 0xFF00FF)
             + (256 - HIDWORD(v64) - v69 - v70) * (v71[0] & 0xFF00FF)) >> 8) & 0xFF00FF | (HIDWORD(v64)
                                                                                       * ((v71[v7 + 1] >> 8) & 0xFF00FF)
                                                                                       + v69
                                                                                       * ((v71[v7] >> 8) & 0xFF00FF)
                                                                                       + v70
                                                                                       * ((v71[1] >> 8) & 0xFF00FF)
                                                                                       + (256 - HIDWORD(v64) - v69 - v70)
                                                                                       * ((v71[0] >> 8) & 0xFF00FF)) & 0xFF00FF00;
      v51 = v90;
      v49 = v77 + 1;
      ++v77;
LABEL_59:
      result = v83;
      v90 = ++v51;
      if ( v51 >= v83[0] )
        break;
      v50 = v85;
    }
    v10 = v80;
LABEL_61:
    v79 += v88;
    v80 = ++v10;
    v82 += v7;
  }
  while ( v10 < result[1] );
  return result;
}

//----- (100025F0) --------------------------------------------------------
double *tjsdf_parsewaves(int a1, double a2)
{
  int v3; // esi
  int v4; // eax
  int v7; // eax
  int v10; // edi
  int v12; // eax
  double *v14; // ebp
  double *result; // eax
  double v18; // st7
  double *v19; // esi
  double v21; // st6
  double *v22; // ecx
  int tjslv_dtop; // edx
  int32_t v26; // [esp+1Ch] [ebp-30Ch]
  int32_t v27; // [esp+20h] [ebp-308h]
  double v29[96]; // [esp+28h] [ebp-300h]

  v3 = a1;
  v4 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(v3);
  (*(void (**)(int, uint32_t, wchar_t *, uint32_t, int, int))(*(uint32_t *)v4 + 16))(v4, 0, L"count", 0, v3, v4);
  v26 = ("tTJSVariant::operator tjs_int() const")(v3);
  if ( v26 > 32 )
  {
    v26 = 32;
  }
  for (size_t i = 0; i < v26; i += 1)
  {
    (*(void (**)(int, uint32_t, int, int, int))(*(uint32_t *)v4 + 20))(v4, 0, i, v3, v4);
    v10 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(v3);
    (*(void (**)(int, uint32_t, wchar_t *, uint32_t, int, int))(*(uint32_t *)v10 + 16))(v10, 0, L"count", 0, v3, v10);
    v27 = ("tTJSVariant::operator tjs_int() const")(v3);
    if ( v27 > 3 )
    {
      v27 = 3;
    }
    v14 = v29 + (3 * i);
    for (size_t j = 0; j < v27; j += 1)
    {
      (*(void (**)(int, uint32_t, int32_t, int, int))(*(uint32_t *)v10 + 20))(v10, 0, i, v3, v10);
      v14[j] = ("tTJSVariant::operator tTVReal() const")(v3);
    }
  }
  result = (double *)malloc(0x20000u);
  v18 = 0.0;
  v19 = result;
  for (size_t i = 0; i < 0x4000; i += 1)
  {
    v21 = 0.0;
    v22 = &v29;
    for (size_t j = 0; j < v26; j += 1)
    {
      v21 += cos(v18 * v22[0] + *v22[1] * v22[2]);
      v22 += 24;
      v18 += 0.001533980787885641;
    }
    v19[0] = v21 + v21;
    ++v19;
  }
  return result;
}

//----- (100027C0) --------------------------------------------------------
int sub_100027C0(uint32_t *a1, uint32_t *a2, int a3, int a4)
{
  int result; // eax
  int v5; // edi
  int v6; // esi
  int v7; // edi
  int *v8; // ecx
  uint32_t *v9; // edx
  int *v10; // ecx
  uint32_t *v11; // edx
  int v12; // edi

  result = a3;
  v5 = a3 >> 1;
  if ( a3 < 0 )
  {
    a2 -= v5;
    v6 = v5 + a4 - 2;
  }
  else
  {
    a1 += v5;
    v6 = a4 - v5 - 2;
  }
  a1[0] = a2[0];
  v7 = (v6 + 3) >> 2;
  v8 = a1 + 1;
  v9 = a2 + 1;
  if ( a3 & 1 )
  {
    v10 = v8 + 1;
    v11 = v9 + 1;
    v12 = v6 - 1;
    while ( 1 )
    {
      v10[0] = ((v11[0] >> 2) & 0x3F3F3F3F)
           + (((uint32_t)v10[0] >> 1) & 0x7F7F7F7F)
           + ((v11[-1] >> 2) & 0x3F3F3F3F);
      ++v10;
      ++v11;
      if ( !--v12 )
        break;
    }
    result = ((v11[0] >> 1) & 0x7F7F7F7F) + (((uint32_t)v10[0] >> 1) & 0x7F7F7F7F);
    v10[0] = result;
  }
  else
  {
    while ( 1 )
    {
      result = ((v9[0] >> 1) & 0x7F7F7F7F) + (((uint32_t)v8[0] >> 1) & 0x7F7F7F7F);
      v8[0] = result;
      ++v8;
      ++v9;
      if ( !--v6 )
        break;
    }
  }
  return result;
}

//----- (100029C0) --------------------------------------------------------
int sub_100029C0(uint32_t *a1, uint32_t *a2, int a3, int a4)
{
  int result; // eax
  int v5; // edi
  int v6; // esi
  int v7; // edi
  int *v8; // ecx
  int *v9; // edx
  int *v10; // ecx
  uint32_t *v11; // edx
  int v12; // edi

  result = a3;
  v5 = a3 >> 1;
  if ( a3 < 0 )
  {
    a2 -= v5;
    v6 = v5 + a4 - 2;
  }
  else
  {
    a1 += v5;
    v6 = a4 - v5 - 2;
  }
  a1[0] = a2[0];
  v7 = (v6 + 3) >> 2;
  v8 = a1 + 1;
  v9 = a2 + 1;
  if ( a3 & 1 )
  {
    v10 = v8 + 1;
    v11 = v9 + 1;
    v12 = v6 - 1;
    while ( 1 )
    {
      v10[0] = ((v11[0] >> 1) & 0x7F7F7F7F) + ((v11[-1] >> 1) & 0x7F7F7F7F);
      ++v10;
      ++v11;
      if ( !--v12 )
        break;
    }
    result = ((v11[0] >> 1) & 0x7F7F7F7F) + (((uint32_t)v10[0] >> 1) & 0x7F7F7F7F);
    v10[0] = result;
  }
  else
  {
    while ( 1 )
    {
      result = *v9;
      *v8 = *v9;
      ++v8;
      ++v9;
      if ( !--v6 )
        break;
    }
  }
  return result;
}

//----- (100033D0) --------------------------------------------------------
int32_t tjsf_Smudge(int a1, int this, int flag, int membername, int hint, int result, int32_t numparams, uint32_t *param, uint32_t objthis)
{
  int tjslv_level; // edi
  int v14; // esi
  int *tjslv_layer; // ebp
  uint32_t tjsldv_layer_buffer; // [esp+30h] [ebp-64h]
  void *v31; // [esp+34h] [ebp-60h]
  int v33; // [esp+3Ch] [ebp-58h]
  char v34; // [esp+40h] [ebp-54h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  tjslv_level = 1;
  if ( numparams < 1 )
    return TJS_E_BADPARAMCOUNT;
  v14 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  if ( !v14 )
    return TJS_E_FAIL;
  ("tTJSVariant::tTJSVariant()")(&v33, a1);
  int tjsldv_layer_width, tjsldv_layer_height, tjsldv_layer_pitch;
  if ( !FIND_AND_GET_MEMBER(L"layer", v14, &v34) )
  {
    tjslv_layer = hint;
  }
  else
  {
    tjslv_layer = ("tTJSVariant::operator iTJSDispatch2 *()")(&v34);
    sub_10001A70(tjslv_layer, &tjsldv_layer_width, &tjsldv_layer_height, &tjsldv_layer_buffer, &tjsldv_layer_pitch);
  }
  if ( FIND_AND_GET_MEMBER(L"level", v14, &v34) )
  {
    tjslv_level = ("tTJSVariant::operator tjs_int() const")(&v34);
  }
  ("tTJSVariant::~ tTJSVariant()")(&v33);
  v31 = 0;
  v33 = 0;
  v31 = malloc(4 * tjsldv_layer_width);
  for (size_t i = 0; i < tjslv_level; i += 1)
  {
    sub_10001000(tjsldv_layer_pitch >> 2, tjsldv_layer_buffer, (uint32_t **)&v31, tjsldv_layer_width, (int)tjsldv_layer_height);
  }
  for (size_t i = 0; i < 3; i += 1)
  {
    if ( v31[i] )
      free(v31[i]);
  }
  //UPDATE: tjslv_layer 0 0 tjsldv_layer_width tjsldv_layer_height
  return TJS_S_OK;
}

//----- (10003730) --------------------------------------------------------
int32_t tjsf_Blur(int a1, int this, int flag, int membername, int hint, int result, int32_t numparams, uint32_t *param, uint32_t objthis)
{
  int tjslv_level; // edi
  int v14; // esi
  int *tjslv_layer; // ebp
  int tjslv_type; // eax
  uint32_t tjsldv_layer_buffer; // [esp+3Ch] [ebp-64h]
  char v32; // [esp+40h] [ebp-60h]
  LPVOID lpMem; // [esp+4Ch] [ebp-54h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  tjslv_level = 1;
  if ( numparams < 1 )
    return TJS_E_BADPARAMCOUNT;
  v14 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  if ( !v14 )
    return TJS_E_FAIL;
  ("tTJSVariant::tTJSVariant()")(&tjsldv_layer_buffer, a1);
  int tjsldv_layer_width, tjsldv_layer_height, tjsldv_layer_pitch;
  if ( !FIND_AND_GET_MEMBER(L"layer", v14, &v32) )
  {
    tjslv_layer = (int *)hint;
  }
  else
  {
    tjslv_layer = ("tTJSVariant::operator iTJSDispatch2 *()")(&v32);
    sub_10001A70(tjslv_layer, &tjsldv_layer_width, &tjsldv_layer_height, &tjsldv_layer_buffer, &tjsldv_layer_pitch);
  }
  if ( FIND_AND_GET_MEMBER(L"level", v14, &v32) )
  {
    tjslv_level = ("tTJSVariant::operator tjs_int() const")(&v32);
  }
  if ( !FIND_AND_GET_MEMBER(L"type", v14, &v32) ) // not used
  {
    tjslv_type = 0;
  }
  else
  {
    tjslv_type = ("tTJSVariant::operator tjs_int() const")(&v32);
  }
  ("tTJSVariant::~ tTJSVariant()")(&tjsldv_layer_buffer);
  if ( !tjslv_type )
  {
    lpMem = malloc(4 * tjsldv_layer_width);
    for (size_t i = 0; tjslv_level > i; i += 1)
    {
      sub_10001000(tjsldv_layer_pitch >> 2, tjsldv_layer_buffer, (uint32_t **)&lpMem, tjsldv_layer_width, (int)tjsldv_layer_height);
    }
    free(lpMem);
  }
  //UPDATE: tjslv_layer 0 0 tjsldv_layer_width tjsldv_layer_height
  return TJS_S_OK;
}

//----- (10003AE0) --------------------------------------------------------
int32_t tjsf_Lens(double a1, int this, int flag, int membername, int *hint, int result, int32_t numparams, int *param)
{
  int32_t result; // eax
  int tjslv_power; // edi
  int v12; // esi
  int *tjslv_dest; // ebp
  double tjslv_zoom; // [esp+48h] [ebp-74h]
  char v30; // [esp+54h] [ebp-68h]
  char v31; // [esp+58h] [ebp-64h]
  int tjsldv_dest_width; // [esp+64h] [ebp-58h]
  int tjsldv_dest_height; // [esp+68h] [ebp-54h]
  char tjsldv_dest_buffer; // [esp+6Ch] [ebp-50h]
  char tjsldv_dest_pitch; // [esp+70h] [ebp-4Ch]
  char tjsldv_src_width; // [esp+74h] [ebp-48h]
  char tjsldv_src_height; // [esp+78h] [ebp-44h]
  char tjsldv_src_buffer; // [esp+7Ch] [ebp-40h]
  char tjsldv_src_pitch; // [esp+80h] [ebp-3Ch]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  tjslv_power = 1;
  if ( numparams < 1 )
    return TJS_E_BADPARAMCOUNT;
  LODWORD(tjslv_zoom) = 1072693248;
  v12 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  if ( v12 )
  {
    ("tTJSVariant::tTJSVariant()")(&v30);
    if ( !FIND_AND_GET_MEMBER(L"src", v12, &v31) )
    {
      tjslv_src = hint;
    }
    else
    {
      tjslv_src = ("tTJSVariant::operator iTJSDispatch2 *()")(&v31);
      sub_10001A70(tjslv_src, &tjsldv_src_width, &tjsldv_src_height, &tjsldv_src_buffer, &tjsldv_src_pitch);
    }
    if ( FIND_AND_GET_MEMBER(L"dest", v12, &v31) )
    {
      tjslv_dest = ("tTJSVariant::operator iTJSDispatch2 *()")(&v31);
      sub_10001A70(tjslv_dest, &tjsldv_dest_width, &tjsldv_dest_height, &tjsldv_dest_buffer, &tjsldv_dest_pitch);
    }
    if ( FIND_AND_GET_MEMBER(L"zoom", v12, &v31) )
    {
      tjslv_zoom = ("tTJSVariant::operator tTVReal() const")(&v31);;
    }
    if ( FIND_AND_GET_MEMBER(L"power", v12, &v31) )
    {
      tjslv_power = ("tTJSVariant::operator tjs_int() const")(&v31);
    }
    sub_10001D20((int)&tjsldv_src_width, &tjsldv_dest_width, tjslv_zoom, tjslv_power, -1, -1);
    //UPDATE: tjslv_dest 0 0 tjsldv_dest_width tjsldv_dest_height
    ("tTJSVariant::~ tTJSVariant()")(&v30);
    return TJS_S_OK;
  }
  return TJS_E_FAIL;
}

//----- (10003EE0) --------------------------------------------------------
int32_t tjsf_InitLens(int this, int flag, int membername)
{
  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  sub_10001CB0();
  return TJS_S_OK;
}

//----- (10003F00) --------------------------------------------------------
int32_t tjsf_ReleaseLens(int this, int flag, int membername)
{
  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  sub_10001D10();
  return TJS_S_OK;
}

//----- (10003F20) --------------------------------------------------------
int32_t tjsf_Noise(int this, int flag, int32_t membername, int hint, int result, int numparams, int *param)
{
  int tjslv_under; // ebx
  int v11; // edi
  int64_t tjslv_seed; // rax
  int tjslv_upper; // eax
  int tjslv_dtop; // ecx
  uint32_t v24; // edi
  uint32_t *v26; // esi
  uint32_t *v27; // edi
  int32_t v28; // ecx
  int v29; // eax
  int64_t v30; // rax
  uint32_t *v31; // edi
  int v32; // eax
  uint16_t v33; // dx
  uint32_t v34; // esi
  uint32_t *v35; // edx
  int v36; // ecx
  int v37; // eax
  int v38; // ecx
  int v39; // eax
  uint32_t *v40; // edx
  uint32_t v41; // ecx
  int v42; // eax
  uint32_t v43; // eax
  int v44; // eax
  uint32_t *v45; // edi
  int32_t v46; // ecx
  uint32_t v47; // eax
  uint32_t v48; // eax
  uint32_t *v49; // edi
  uint32_t v50; // eax
  uint32_t v51; // eax
  int32_t v52; // edx
  uint32_t *v53; // ecx
  uint32_t v54; // ecx
  uint32_t *v55; // esi
  uint32_t *v56; // edx
  int v57; // eax
  int v58; // ecx
  int v59; // eax
  uint32_t *v60; // esi
  bool v61; // zf
  int v62; // eax
  uint32_t v63; // edx
  uint32_t *v64; // eax
  uint32_t v74; // [esp-14h] [ebp-94h]
  uint32_t v75; // [esp-14h] [ebp-94h]
  char v80; // [esp+3Ch] [ebp-44h]
  int *tjslv_layer; // [esp+48h] [ebp-38h]
  char *v85; // [esp+58h] [ebp-28h]
  uint32_t *v87; // [esp+64h] [ebp-1Ch]
  int tjslv_monocro; // [esp+68h] [ebp-18h]
  uint32_t *tjsldv_layer_buffer; // [esp+6Ch] [ebp-14h]
  uint32_t *v90; // [esp+70h] [ebp-10h]

  tjslv_under = 0;
  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  tjslv_monocro = 1;
  int tjsldv_layer_width, tjsldv_layer_height, tjsldv_layer_pitch
  v11 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  if ( v11 )
  {
    ("tTJSVariant::tTJSVariant()")(&v80);
    if ( FIND_AND_GET_MEMBER(L"layer", v11, &v80) )
    {
      tjslv_layer = ("tTJSVariant::operator iTJSDispatch2 *()")(&v80);
      sub_10001A70(tjslv_layer, &tjsldv_layer_width, &tjsldv_layer_height, &tjsldv_layer_buffer, &tjsldv_layer_pitch);
    }
    if ( FIND_AND_GET_MEMBER(L"monocro", v11, &v80) )
    {
      tjslv_monocro = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( !FIND_AND_GET_MEMBER(L"seed", v11, &v80) )
    {
      tjslv_seed = ("tjs_uint64 ::TVPGetTickCount()")();
    }
    else
    {
      tjslv_seed = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( FIND_AND_GET_MEMBER(L"under", v11, &v80) )
    {
      tjslv_under = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( !FIND_AND_GET_MEMBER(L"upper", v11, &v80) )
    {
      tjslv_upper = 255;
    }
    else
    {
      tjslv_upper = ("tTJSVariant::operator tjs_int() const")(&v80);
    }
    if ( tjslv_upper < tjslv_under )
    {
      tjslv_dtop = tjslv_upper;
      tjslv_upper = tjslv_under;
      tjslv_under = tjslv_dtop;
    }
    v24 = tjslv_upper - tjslv_under;
    v26 = tjsldv_layer_buffer;
    tjsgdv_current_seed = tjslv_seed;
    v90 = tjsldv_layer_buffer;
    if ( tjslv_monocro )
    {
      if ( v24 == 255 )
      {
        for (tjslv_monocro = 0; tjslv_monocro < tjsldv_layer_height; tjslv_monocro += 1)
        {
          v90 = v26;
          v74 = v24;
          v27 = v26;
          v29 = tjsgdv_current_seed;
          for (v28 = tjsldv_layer_width; (uint32_t)v28 >= 3; v28 -= 3)
          {
            v30 = 1566083941ll * (uint32_t)v29;
            v29 = v30 + 1;
            HIDWORD(v30) = ((v29 & 0xFF000000) >> 24) | ((v29 & 0xFF000000) >> 16) | ((v29 & 0xFF000000) >> 8) | HIDWORD(v30) & 0xFF000000;
            v27[0] = HIDWORD(v30);
            v31 = v27 + 1;
            HIDWORD(v30) = ((v29 & 0xFF0000u) >> 16) | ((v29 & 0xFF0000u) >> 8) | v29 & 0xFF0000 | HIDWORD(v30) & 0xFF000000;
            v31[0] = HIDWORD(v30);
            ++v31;
            v31[0] = ((v29 & 0xFF00u) << 8 >> 16) | ((v29 & 0xFF00) << 8) | v29 & 0xFF00 | HIDWORD(v30) & 0xFF000000;
            v27 = v31 + 1;
          }
          v24 = v74;
          v32 = 1566083941 * v29 + 1;
          tjsgdv_current_seed = v32;
          v87 = (uint32_t *)v32;
          if ( v28 > 1 )
          {
            LOBYTE(v33) = 0;
            HIBYTE(v33) = BYTE2(v87);
            v24 = v32 & 0xFFFF0000;
            v26[0] = v32 & 0xFFFF0000 | ((v32 & 0xFF0000 | v33) >> 8) | 0xFF000000;
            v90 = v26 + 1;
          }
          if ( v28 > 0 )
            v90[0] = (uint16_t)(v32 & 0xFF00) | BYTE1(v32) | ((v32 & 0xFF00 | 0xFFFF0000) << 8);
          v26 = (uint32_t *)((char *)v26 + (uint32_t)tjsldv_layer_pitch);
        }
      }
      else
      {
        for (size_t i = 0; i < tjsldv_layer_height; i += 1)
        {
          v35 = v90;
          v36 = tjsldv_layer_width;
          if ( (int32_t)tjsldv_layer_width >= 2 )
          {
            for (size_t j = 0; j < (tjsldv_layer_width >> 1); j += 1)
            {
              v37 = 1566083941 * tjslv_seed + 1;
              v38 = v24 * (uint16_t)v37;
              tjsgdv_current_seed = v37;
              v39 = (uint8_t)(tjslv_under + (v24 * ((v37 >> 16) & 0xFFFF) >> 16));
              v35[0] = (uint8_t)(tjslv_under + BYTE2(v38)) | (((uint8_t)(tjslv_under + BYTE2(v38)) | ((((uint8_t)tjslv_under + BYTE2(v38)) | 0xFFFFFF00) << 8)) << 8);
              v40 = v35 + 1;
              v41 = v39 | ((v39 | ((v39 | 0xFFFFFF00) << 8)) << 8);
              v40[0] = v41;
              v35 = v40 + 1;
              tjslv_seed = tjsgdv_current_seed;
            }
            tjslv_seed = tjsgdv_current_seed;
            v36 = tjsldv_layer_width - 2 * (tjsldv_layer_width >> 1);
          }
          for (size_t j = 0; j < v36; j += 1)
          {
            v43 = 1566083941 * tjslv_seed + 1;
            tjsgdv_current_seed = v43;
            v44 = (uint8_t)(tjslv_under + (v24 * (v43 >> 16) >> 16));
            v35[0] = v44 | ((v44 | ((v44 | 0xFFFFFF00) << 8)) << 8);
            ++v35;
            tjslv_seed = tjsgdv_current_seed;
          }
          tjslv_seed = tjsgdv_current_seed;
          v90 = (uint32_t *)((char *)v90 + (uint32_t)tjsldv_layer_pitch);
        }
      }
    }
    else if ( v24 == 255 )
    {
      for (size_t i = 0; i < tjsldv_layer_height; i += 1)
      {
        v90 = v26;
        v75 = v24;
        v45 = v26;
        v47 = tjsgdv_current_seed;
        for (v46 = tjsldv_layer_width; (uint32_t)v46 >= 4; v46 -= 4)
        {
          v48 = 1566083941 * v47 + 1;
          v45[0] = (v48 >> 8) | 0xFF000000;
          v49 = v45 + 1;
          v50 = 1566083941 * v48 + 1;
          v49[0] = (v50 >> 8) | 0xFF000000;
          ++v49;
          v51 = 1566083941 * v50 + 1;
          v49[0] = (v51 >> 8) | 0xFF000000;
          ++v49;
          v47 = 1566083941 * v51 + 1;
          v49[0] = (v47 >> 8) | 0xFF000000;
          v45 = v49 + 1;
        }
        tjsgdv_current_seed = v47;
        v24 = v75;
        v52 = v46;
        if ( v46 <= 2 )
        {
          v53 = v90;
        }
        else
        {
          tjsgdv_current_seed = 1566083941 * tjsgdv_current_seed + 1;
          v53 = v26 + 1;
          v26[0] = ((uint32_t)tjsgdv_current_seed >> 8) | 0xFF000000;
          v90 = v26 + 1;
        }
        if ( v52 > 1 )
        {
          tjsgdv_current_seed = 1566083941 * tjsgdv_current_seed + 1;
          v53[0] = ((uint32_t)tjsgdv_current_seed >> 8) | 0xFF000000;
          ++v53;
          v90 = v53;
        }
        if ( v52 > 0 )
        {
          tjsgdv_current_seed = 1566083941 * tjsgdv_current_seed + 1;
          v53[0] = ((uint32_t)tjsgdv_current_seed >> 8) | 0xFF000000;
        }
        v26 = (uint32_t *)((char *)v26 + (uint32_t)tjsldv_layer_pitch);
      }
    }
    else
    {
      for (size_t i = 0; i < tjsldv_layer_height; i += 1)
      {
        v55 = v90;
        v87 = v90;
        v56 = tjsldv_layer_width;
        if ( (int32_t)tjsldv_layer_width >= 2 )
        {
          v85 = (char *)(tjsldv_layer_width - 2 * (tjsldv_layer_width >> 1));
          for (size_t j = 0; j < (tjsldv_layer_width >> 1); j += 1)
          {
            v57 = 1566083941 * tjslv_seed + 1;
            v58 = v57;
            v59 = 1566083941 * v57 + 1;
            tjsgdv_current_seed = 1566083941 * v59 + 1;
            v60 = v87;
            v87[0] = (uint8_t)(tjslv_under + (v24 * (uint16_t)v59 >> 16)) | ((tjslv_under + ((int32_t)(v24 * (uint16_t)v58) >> 16)) << 16) & 0xFFFF0000 | (((uint16_t)tjslv_under + (v24 * ((v58 >> 16) & 0xFFFF) >> 16)) << 8) & 0xFF00 | 0xFF000000;
            ++v60;
            v60[0] = (((uint16_t)tjslv_under + (v24 * (uint16_t)v59 >> 16)) << 8) & 0xFF00 | (uint8_t)(tjslv_under + (v24 * ((v59 >> 16) & 0xFFFF) >> 16)) | ((tjslv_under + ((int32_t)(v24 * ((v59 >> 16) & 0xFFFF)) >> 16)) << 16) & 0xFFFF0000 | 0xFF000000;
            v55 = v60 + 1;
            v87 = v55;
            tjslv_seed = tjsgdv_current_seed;
          }
          tjslv_seed = tjsgdv_current_seed;
          v56 = v85;
        }
        for (size_t j = 0; j < v56; j += 1)
        {
          v62 = 1566083941 * tjslv_seed + 1;
          tjsgdv_current_seed = 1566083941 * v62 + 1;
          v63 = (uint8_t)(tjslv_under + (v24 * (uint16_t)tjsgdv_current_seed >> 16)) | ((tjslv_under + ((int32_t)(v24 * (uint16_t)v62) >> 16)) << 16) & 0xFFFF0000 | (((uint16_t)tjslv_under + (v24 * ((v62 >> 16) & 0xFFFF) >> 16)) << 8) & 0xFF00;
          v55[0] = v63 | 0xFF000000;
          ++v55;
          tjslv_seed = tjsgdv_current_seed;
        }
        tjslv_seed = tjsgdv_current_seed;
        v90 = (uint32_t *)((char *)v90 + (uint32_t)tjsldv_layer_pitch);
      }
    }
    //UPDATE: tjslv_layer 0 0 tjsldv_layer_width tjsldv_layer_height
    ("tTJSVariant::~ tTJSVariant()")(&v80);
  }
  return TJS_S_OK;
}

//----- (10004860) --------------------------------------------------------
int32_t tjsf_Contrast(int this, int flag, int membername, int hint, int result, int numparams, int param)
{
  int v7; // edi
  int v11; // esi
  int v13; // edx
  int tjslv_level; // eax
  int32_t v18; // esi
  char *v20; // eax
  char *v21; // ebx
  int v22; // edx
  int tjslv_dtop; // ecx
  int v24; // eax
  uint32_t *v25; // edi
  int v26; // ecx
  int32_t v27; // ebx
  uint32_t *v28; // edi
  int v29; // eax
  bool v30; // zf
  int v33; // esi
  int v35; // edi
  int v36; // esi
  int v38; // edi
  char v39; // [esp+8h] [ebp-74h]
  char v40; // [esp+14h] [ebp-68h]
  char v41; // [esp+20h] [ebp-5Ch]
  char v42; // [esp+2Ch] [ebp-50h]
  char *v43; // [esp+38h] [ebp-44h]
  char *v44; // [esp+3Ch] [ebp-40h]
  char *v45; // [esp+40h] [ebp-3Ch]
  char *v46; // [esp+44h] [ebp-38h]
  char v47; // [esp+48h] [ebp-34h]
  int *tjslv_layer; // [esp+54h] [ebp-28h]
  uint32_t v49; // [esp+58h] [ebp-24h]
  int v50; // [esp+5Ch] [ebp-20h]
  int v51; // [esp+60h] [ebp-1Ch]
  uint32_t *v52; // [esp+64h] [ebp-18h]
  uint32_t *tjsldv_layer_buffer; // [esp+68h] [ebp-14h]
  uint32_t tjsldv_layer_pitch; // [esp+6Ch] [ebp-10h]
  char *v56; // [esp+8Ch] [ebp+10h]

  v7 = 0;
  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v11 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  if ( !v11 )
    return TJS_E_FAIL;
  ("tTJSVariant::tTJSVariant()")(&v47);
  int tjsldv_layer_width, tjsldv_layer_height;
  if ( FIND_AND_GET_MEMBER(L"layer", v11, &v47) )
  {
    tjslv_layer = ("tTJSVariant::operator iTJSDispatch2 *()")(&v47);
    sub_10001A70(tjslv_layer, &tjsldv_layer_width, &tjsldv_layer_height, &tjsldv_layer_buffer, &tjsldv_layer_pitch);
  }
  v49 = tjsldv_layer_pitch >> 2;
  if ( FIND_AND_GET_MEMBER(L"level", v11, &v47) )
  {
    tjslv_level = ("tTJSVariant::operator tjs_int() const")(&v47);
    v18 = tjslv_level;
    if ( tjslv_level )
    {
      if ( tjslv_level >= -127 )
      {
        if ( tjslv_level > 127 )
          v18 = 127;
      }
      else
      {
        v18 = -127;
      }
      v20 = (char *)malloc(0x400u);
      if ( v18 <= 0 )
      {
        v25 = v20;
        v26 = 0;
        for (size_t i = 0; i < 256; i += 1)
        {
          v25[i] = v26 / 255 - v18;
          v26 += 2 * v18 + 255;
        }
      }
      else
      {
        v22 = 255 - v18;
        if ( v18 > 0 )
        {
          memset(v20, 0, 4 * v18);
          v7 = v18;
        }
        if ( v7 < v22 )
        {
          v50 = 255 - 2 * v18;
          tjslv_dtop = 255 * v7;
          v52 = (uint32_t *)&v20[4 * v7];
          v7 = 255 - v18;
          for (size_t i = 0; i < (v22 - v7); i += 1)
          {
            v24 = (tjslv_dtop - 255 * v18) / v50;
            tjslv_dtop += 255;
            v52[i] = v24;
          }
        }
        if ( v7 < 256 )
          memset32(&v20[4 * v7], 255, 256 - v7);
      }
      v52 = tjsldv_layer_buffer;
      v49 *= 4;
      for (size_t i = 0; i < tjsldv_layer_height; i += 1)
      {
        v28 = v52;
        for (size_t j = 0; j < tjsldv_layer_width; j += 1)
        {
          v52[j] = *(uint32_t *)&v28[4 * (uint8_t)v28[j]] | ((*(uint32_t *)&v28[4 * ((v28[j] >> 8) & 0xFF)] | (*(uint32_t *)&v28[4 * ((v28[j] >> 16) & 0xFF)] << 8)) << 8) | v28[j] & 0xFF000000;
        }
        v52 = (uint32_t *)((char *)v52 + v49);
      }
      free(v20);
      //UPDATE: tjslv_layer 0 0 tjsldv_layer_width tjsldv_layer_height
    }
  }
  ("tTJSVariant::~ tTJSVariant()")(&v47);
  return TJS_S_OK;
}

//----- (10004C90) --------------------------------------------------------
int32_t tjsf_initHaze(int a1, double a2, int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  int v12; // esi
  int v17; // eax
  char v22; // [esp+8h] [ebp-18h]
  int tjslv_dtop; // [esp+Ch] [ebp-14h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  if ( tjsgdv_hazeenabled )
    return TJS_E_FAIL;
  v12 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  if ( v12 )
  {
    ("tTJSVariant::tTJSVariant()")(&v22, a1);
    if ( !FIND_AND_GET_MEMBER(L"time", v12, &tjslv_dtop) )
    {
      tjsgv_time = -1;
    }
    else
    {
      tjsgv_time = ("tTJSVariant::operator tjs_int() const")(&tjslv_dtop);
    }
    if ( !FIND_AND_GET_MEMBER(L"intime", v12, &tjslv_dtop) )
    {
      tjsgv_intime = 800;
    }
    else
    {
      tjsgv_intime = ("tTJSVariant::operator tjs_int() const")(&tjslv_dtop);
    }
    if ( !FIND_AND_GET_MEMBER(L"outtime", v12, &tjslv_dtop) )
    {
      v17 = tjsgv_intime;
    }
    else
    {
      v17 = ("tTJSVariant::operator tjs_int() const")(&tjslv_dtop);
    }
    tjsgv_outtime = v17;
    if ( !FIND_AND_GET_MEMBER(L"speed", v12, &tjslv_dtop) )
    {
      tjsgv_speed = 0.001570796326794897;
    }
    else
    {
      ("tTJSVariant::operator tTVReal() const")(&tjslv_dtop);
      tjsgv_speed = a2;
    }
    if ( !FIND_AND_GET_MEMBER(L"cycle", v12, &tjslv_dtop) )
    {
      tjsgv_cycle = 6.0;
    }
    else
    {
      tjsgv_cycle = ("tTJSVariant::operator tTVReal() const")(&tjslv_dtop);
    }
    if ( !FIND_AND_GET_MEMBER(L"upper", v12, &tjslv_dtop) )
    {
      tjsgv_upper = -1;
    }
    else
    {
      tjsgv_upper = ("tTJSVariant::operator tjs_int() const")(&tjslv_dtop);
    }
    if ( !FIND_AND_GET_MEMBER(L"center", v12, &tjslv_dtop) )
    {
      tjsgv_center = -1;
    }
    else
    {
      tjsgv_center = ("tTJSVariant::operator tjs_int() const")(&tjslv_dtop);
    }
    if ( !FIND_AND_GET_MEMBER(L"lower", v12, &tjslv_dtop) )
    {
      tjsgv_lower = -1;
    }
    else
    {
      tjsgv_lower = ("tTJSVariant::operator tjs_int() const")(&tjslv_dtop);
    }
    if ( !FIND_AND_GET_MEMBER(L"upperpow", v12, &tjslv_dtop) )
    {
      tjsgv_upperpow = 0xFFFFF;
    }
    else
    {
      ("tTJSVariant::operator tTVReal() const")(&tjslv_dtop);
      a2 *= 1048575.0;
      tjsgv_upperpow = (int64_t)a2;
    }
    if ( !FIND_AND_GET_MEMBER(L"centerpow", v12, &tjslv_dtop) )
    {
      tjsgv_centerpow = 0xFFFFF;
    }
    else
    {
      ("tTJSVariant::operator tTVReal() const")(&tjslv_dtop);
      a2 *= 1048575.0;
      tjsgv_centerpow = (int64_t)a2;
    }
    if ( !FIND_AND_GET_MEMBER(L"lowerpow", v12, &tjslv_dtop) )
    {
      tjsgv_lowerpow = 0xFFFFF;
    }
    else
    {
      ("tTJSVariant::operator tTVReal() const")(&tjslv_dtop);
      a2 *= 1048575.0;
      tjsgv_lowerpow = (int64_t)a2;
    }
    if ( FIND_AND_GET_MEMBER(L"waves", v12, &tjslv_dtop) )
    {
      tjsgv_waves = tjsdf_parsewaves((int)&tjslv_dtop, a2);
    }
    if ( !FIND_AND_GET_MEMBER(L"lwaves", v12, &tjslv_dtop) )
    {
      tjsgv_lwaves = 0;
    }
    else
    {
      tjsgv_lwaves = tjsdf_parsewaves((int)&tjslv_dtop, a2);
    }
    tjsgdv_hazeenabled = 1;
    ("tTJSVariant::~ tTJSVariant()")(&tjslv_dtop);
  }
  return TJS_S_OK;
}

//----- (100052E0) --------------------------------------------------------
int32_t tjsf_doHaze(double a1, int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  int v11; // esi
  int *tjslv_src; // eax
  int v20; // ebx
  uint32_t v21; // esi
  int v22; // ecx
  int v24; // esi
  int64_t v25; // rax
  int v26; // eax
  int32_t v27; // ebx
  int32_t v28; // edi
  double v29; // st7
  int v30; // esi
  int64_t v31; // rax
  int v32; // eax
  int v33; // edi
  int v34; // edi
  int32_t v35; // esi
  int v36; // ebx
  double v37; // st7
  int v38; // edi
  int64_t v39; // rax
  int v40; // eax
  int v41; // ecx
  double v42; // st7
  int v43; // edi
  int64_t v44; // rax
  int v45; // eax
  int v53; // [esp+28h] [ebp-D8h]
  int v54; // [esp+28h] [ebp-D8h]
  int v55; // [esp+28h] [ebp-D8h]
  int v56; // [esp+28h] [ebp-D8h]
  int v57; // [esp+2Ch] [ebp-D4h]
  int32_t v58; // [esp+2Ch] [ebp-D4h]
  int32_t v59; // [esp+2Ch] [ebp-D4h]
  int32_t tjslv_tick; // [esp+30h] [ebp-D0h]
  int v61; // [esp+30h] [ebp-D0h]
  int32_t v62; // [esp+30h] [ebp-D0h]
  int v63; // [esp+34h] [ebp-CCh]
  int32_t v64; // [esp+34h] [ebp-CCh]
  int v65; // [esp+34h] [ebp-CCh]
  int v66; // [esp+38h] [ebp-C8h]
  int v67; // [esp+38h] [ebp-C8h]
  int v68; // [esp+38h] [ebp-C8h]
  int v69; // [esp+3Ch] [ebp-C4h]
  int v70; // [esp+3Ch] [ebp-C4h]
  int v71; // [esp+3Ch] [ebp-C4h]
  uint32_t v72; // [esp+40h] [ebp-C0h]
  int v73; // [esp+44h] [ebp-BCh]
  int v74; // [esp+44h] [ebp-BCh]
  int v75; // [esp+48h] [ebp-B8h]
  int (*v76)(uint32_t *, uint32_t *, int, int); // [esp+4Ch] [ebp-B4h]
  double v77; // [esp+50h] [ebp-B0h]
  uint32_t v78; // [esp+5Ch] [ebp-A4h]
  int v79; // [esp+5Ch] [ebp-A4h]
  int v80; // [esp+64h] [ebp-9Ch]
  int v81; // [esp+64h] [ebp-9Ch]
  double tjslv_per; // [esp+68h] [ebp-98h]
  int tjsldv_dest_height; // [esp+74h] [ebp-8Ch]
  uint32_t v84; // [esp+78h] [ebp-88h]
  int tjsldv_dest_width; // [esp+88h] [ebp-78h]
  int tjsldv_dest_buffer; // [esp+90h] [ebp-70h]
  uint32_t tjsldv_dest_pitch; // [esp+94h] [ebp-6Ch]
  int *tjslv_dest; // [esp+ACh] [ebp-54h]
  char tjsldv_src_width; // [esp+B0h] [ebp-50h]
  char tjsldv_src_height; // [esp+B4h] [ebp-4Ch]
  int tjsldv_src_buffer; // [esp+B8h] [ebp-48h]
  uint32_t tjsldv_src_pitch; // [esp+BCh] [ebp-44h]
  uint32_t v97; // [esp+C0h] [ebp-40h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  if ( !tjsgdv_hazeenabled )
    return TJS_E_FAIL;
  v11 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  if ( v11 )
  {
    ("tTJSVariant::tTJSVariant()")(&x);
    if ( FIND_AND_GET_MEMBER(L"src", v11, &x) )
    {
      tjslv_src = ("tTJSVariant::operator iTJSDispatch2 *()")(&x);
      sub_10001A70(tjslv_src, &tjsldv_src_width, &tjsldv_src_height, &tjsldv_src_buffer, &tjsldv_src_pitch);
    }
    if ( FIND_AND_GET_MEMBER(L"dest", v11, &x) )
    {
      tjslv_dest = ("tTJSVariant::operator iTJSDispatch2 *()")(&v84[1]);
      sub_10001A70(tjslv_dest, &tjsldv_dest_width, &tjsldv_dest_height, &tjsldv_dest_buffer, &tjsldv_dest_pitch);
    }
    if ( !FIND_AND_GET_MEMBER(L"tick", v11, &x) )
    {
      tjslv_tick = 0;
    }
    else
    {
      tjslv_tick = ("tTJSVariant::operator tjs_int() const")(&x);
    }
    if ( !FIND_AND_GET_MEMBER(L"per", v11, &x) )
    {
      tjslv_per = 0.0;
    }
    else
    {
      tjslv_per = ("tTJSVariant::operator tTVReal() const")(&x);;
    }
    if ( !FIND_AND_GET_MEMBER(L"blend", v11, &x) )
    {
      v76 = sub_100029C0;
    }
    else
    {
      v76 = sub_100027C0;
      if ( !("tTJSVariant::operator tjs_int() const")(&x) )
        v76 = sub_100029C0;
    }
    ("tTJSVariant::~ tTJSVariant()")(&x);
    v20 = tjsldv_dest_buffer;
    v77 = (double)((int64_t)((double)tjslv_tick * tjsgv_speed * 651.8986469044033) & 0x3FFF);
    v72 = tjsldv_src_pitch >> 2;
    v21 = tjsldv_dest_pitch >> 2;
    v69 = tjsldv_src_buffer;
    v33 = tjsldv_dest_height;
    v78 = tjsldv_dest_pitch >> 2;
    if ( tjsgv_upper >= 0 || tjsgv_lower >= 0 )
    {
      if ( tjsgv_center < 0 )
      {
        v27 = tjsgv_upper < 0 ? 0 : tjsgv_upper;
        if ( tjsgv_lower <= tjsldv_dest_height )
        {
          v57 = tjsldv_dest_height;
          if ( tjsgv_lower >= 0 )
            v57 = tjsgv_lower;
        }
        else
        {
          v57 = tjsldv_dest_height;
        }
        v64 = tjsgv_upperpow;
        v79 = (tjsgv_lowerpow - tjsgv_upperpow) / (v57 - v27);
        v61 = tjsldv_dest_buffer + 4 * v21 * v27;
        v70 = tjsldv_src_buffer + 4 * v72 * v27;
        v67 = 4 * v21;
        v97 = 4 * v72;
        v54 = -v27;
        v73 = tjsldv_dest_height - v27 - 1;
        for (int i = v27; i < v57; i += 1)
        {
          v29 = (double)v64;
          if ( tjsgv_lwaves )
          {
            v31 = (int64_t)(v29 * tjsgv_lwaves[(int64_t)v77 & 0x3FFF] * tjslv_per + 0.5);
            if ( i - (int32_t)v31 >= 0 )
            {
              if ( i < (int32_t)v31 + tjsldv_dest_height )
                v32 = v72 * -(int32_t)v31;
              else
                v32 = v72 * v73;
            }
            else
            {
              v32 = v72 * v54;
            }
          }
          else
          {
            v32 = 0;
          }
          v76(
            (uint32_t *)v61,
            (uint32_t *)(v70 + 4 * v32),
            (int32_t)(int64_t)(v29 * tjsgv_waves[(int64_t)v77 & 0x3FFF] * tjslv_per) >> 20,
            tjsldv_dest_width);
          v77 = tjsgv_cycle + v77;
          v64 += v79;
          v70 += v97;
          v61 += v67;
          --v54;
          --v73;
        }
        v33 = v57 - v27;
        goto LABEL_102;
      }
      v65 = tjsldv_dest_height;
      v62 = tjsgv_upper < 0 ? 0 : tjsgv_upper;
      if ( tjsgv_lower <= tjsldv_dest_height )
        v65 = tjsgv_lower;
      v68 = tjsldv_dest_height;
      if ( tjsgv_center <= tjsldv_dest_height )
        v68 = tjsgv_center;
      v34 = tjsgv_centerpow;
      v58 = tjsgv_upperpow;
      v80 = (tjsgv_centerpow - tjsgv_upperpow) / (v68 - v62);
      v36 = tjsldv_dest_buffer + 4 * v78 * v62;
      v71 = tjsldv_src_buffer + 4 * v72 * v62;
      v74 = -v62;
      v55 = tjsldv_dest_height - v62 - 1;
      for (v35 = v62; v35 < v68; v35 += 1)
      {
        if ( tjsgv_lwaves )
        {
          v39 = (int64_t)(v58 * tjsgv_lwaves[(int64_t)v77 & 0x3FFF] * tjslv_per + 0.5);
          if ( v35 - (int32_t)v39 >= 0 )
          {
            if ( v35 < (int32_t)v39 + tjsldv_dest_height )
              v40 = v72 * -(int32_t)v39;
            else
              v40 = v72 * v55;
          }
          else
          {
            v40 = v72 * v74;
          }
        }
        else
        {
          v40 = 0;
        }
        v76(
          (uint32_t *)v36,
          (uint32_t *)(v71 + 4 * v40),
          (int32_t)(int64_t)(v58 * tjsgv_waves[(int64_t)v77 & 0x3FFF] * tjslv_per) >> 20,
          tjsldv_dest_width);
        v77 = tjsgv_cycle + v77;
        v58 += v80;
        v36 += 4 * v78;
        v71 += 4 * v72;
        --v74;
        --v55;
      }
      v41 = v65;
      v59 = v34;
      v81 = (tjsgv_lowerpow - v34) / (v65 - v68);
      v56 = -v35;
      for (;v35 < v65;v35 += 1)
      {
        if ( tjsgv_lwaves )
        {
          v44 = (int64_t)(v59 * tjsgv_lwaves[(int64_t)v77 & 0x3FFF] * tjslv_per + 0.5);
          if ( v35 - (int32_t)v44 >= 0 )
          {
            if ( v35 < (int32_t)v44 + tjsldv_dest_height )
              v45 = v72 * -(int32_t)v44;
            else
              v45 = v72 * (tjsldv_dest_height + v56 - 1);
          }
          else
          {
            v45 = v72 * v56;
          }
        }
        else
        {
          v45 = 0;
        }
        v76(
          (uint32_t *)v36,
          (uint32_t *)(v71 + 4 * v45),
          (int32_t)(int64_t)(v59 * tjsgv_waves[(int64_t)v77 & 0x3FFF] * tjslv_per) >> 20,
          tjsldv_dest_width);
        v77 = tjsgv_cycle + v77;
        v59 += v81;
        v36 += 4 * v78;
        v71 += 4 * v72;
        --v56;
      }
      v41 = v65;
      v27 = v62;
      v33 = v41 - v62;
    }
    else
    {
      v66 = 4 * v21;
      v63 = 0;
      v53 = tjsldv_dest_height - 1;
      for (int i = 0; i < tjsldv_dest_height; i += 1)
      {
        if ( tjsgv_lwaves )
        {
          v25 = (int64_t)(tjslv_per * tjsgv_lwaves[(int64_t)v77 & 0x3FFF]);
          if ( i - (int32_t)v25 >= 0 )
          {
            if ( i < (int32_t)v25 + tjsldv_dest_height )
              v26 = v72 * -(int32_t)v25;
            else
              v26 = v72 * v53;
          }
          else
          {
            v26 = v72 * v63;
          }
        }
        else
        {
          v26 = 0;
        }
        v76((uint32_t *)v20, (uint32_t *)(v69 + 4 * v26), (int64_t)(tjslv_per * tjsgv_waves[(int64_t)v77 & 0x3FFF]), tjsldv_dest_width);
        v77 = tjsgv_cycle + v77;
        v20 += v66;
        --v53;
        v33 = tjsldv_dest_height;
        v69 += 4 * v72;
        --v63;
      }
      v27 = 0;
    }
LABEL_102:
    //UPDATE: tjslv_dest 0 v27 tjsldv_dest_width v33
  }
  return TJS_S_OK;
}

//----- (10005D70) --------------------------------------------------------
int32_t tjsf_endHaze(int this, int flag, int membername)
{
  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( tjsgdv_hazeenabled )
  {
    if ( tjsgv_waves )
      free(tjsgv_waves);
    tjsgdv_hazeenabled = 0;
  }
  return TJS_S_OK;
}

//----- (10005DB0) --------------------------------------------------------
int32_t tjsf_Stretch(int a1, char this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  int v11; // esi
  int *tjslv_src; // eax
  int tjslv_swidth; // eax
  float tjslv_dtop; // ebp
  int tjslv_dwidth; // ebx
  int32_t tjslv_dheight; // edi
  double v30; // st7
  double v31; // st6
  float v32; // ST44_4
  float v33; // ST40_4
  double v34; // st7
  int v35; // ST34_4
  int64_t v36; // rax
  int v37; // esi
  double v38; // st6
  int v39; // ecx
  int64_t v40; // rax
  int32_t v41; // ST34_4
  int v42; // ST34_4
  int64_t v43; // rax
  double v44; // st7
  int64_t v45; // rax
  int v46; // ST34_4
  uint32_t v47; // edi
  int v48; // esi
  int v49; // ecx
  uint32_t v50; // eax
  int v51; // eax
  int v52; // esi
  int *v53; // ebp
  int v54; // edx
  int *v55; // eax
  int v56; // ecx
  int v57; // edx
  int v58; // ebx
  int v59; // ecx
  int v60; // esi
  int v61; // ST4C_4
  uint32_t v62; // ST58_4
  int v63; // ecx
  uint32_t v64; // ebp
  uint32_t v65; // esi
  uint32_t v66; // edi
  uint32_t v67; // ecx
  uint32_t *v68; // ebp
  bool v69; // zf
  int v70; // eax
  int v71; // edx
  uint32_t *v72; // esi
  int v73; // ebp
  int v74; // ebx
  int v75; // eax
  int v76; // ecx
  int v77; // eax
  int v78; // esi
  int v79; // edx
  int v80; // ecx
  int v81; // esi
  int v82; // ebp
  uint32_t *v83; // eax
  int v84; // ecx
  int v85; // edx
  int v86; // ecx
  int v87; // esi
  int v88; // ST54_4
  uint32_t v89; // ecx
  uint32_t v90; // esi
  uint32_t v91; // edi
  uint32_t v92; // eax
  int v93; // ecx
  uint32_t v94; // edx
  int v95; // ecx
  uint32_t v96; // ecx
  int v97; // esi
  int v98; // edx
  int v99; // ecx
  int v100; // esi
  int v101; // edx
  uint32_t *v102; // ecx
  uint32_t v103; // esi
  uint32_t v104; // eax
  int v110; // [esp+24h] [ebp-F0h]
  uint32_t v111; // [esp+34h] [ebp-E0h]
  int v112; // [esp+38h] [ebp-DCh]
  int v113; // [esp+38h] [ebp-DCh]
  int32_t v114; // [esp+3Ch] [ebp-D8h]
  int v115; // [esp+3Ch] [ebp-D8h]
  float v116; // [esp+40h] [ebp-D4h]
  int v117; // [esp+40h] [ebp-D4h]
  int v118; // [esp+40h] [ebp-D4h]
  int v119; // [esp+40h] [ebp-D4h]
  int v120; // [esp+44h] [ebp-D0h]
  int v121; // [esp+44h] [ebp-D0h]
  uint32_t v122; // [esp+44h] [ebp-D0h]
  int v123; // [esp+44h] [ebp-D0h]
  int v124; // [esp+48h] [ebp-CCh]
  int v125; // [esp+48h] [ebp-CCh]
  int64_t tjslv_dleft; // [esp+4Ch] [ebp-C8h]
  int v128; // [esp+54h] [ebp-C0h]
  int v130; // [esp+58h] [ebp-BCh]
  int v131; // [esp+5Ch] [ebp-B8h]
  int v132; // [esp+60h] [ebp-B4h]
  int *v134; // [esp+60h] [ebp-B4h]
  float v135; // [esp+64h] [ebp-B0h]
  float v136; // [esp+68h] [ebp-ACh]
  char v137; // [esp+6Ch] [ebp-A8h]
  uint32_t v138; // [esp+78h] [ebp-9Ch]
  int v139; // [esp+7Ch] [ebp-98h]
  uint32_t v140; // [esp+80h] [ebp-94h]
  int tjslv_opa; // [esp+84h] [ebp-90h]
  uint32_t v142; // [esp+88h] [ebp-8Ch]
  uint32_t *v143; // [esp+8Ch] [ebp-88h]
  int v144; // [esp+90h] [ebp-84h]
  int v145; // [esp+94h] [ebp-80h]
  int v146; // [esp+98h] [ebp-7Ch]
  int v147; // [esp+9Ch] [ebp-78h]
  uint32_t v148; // [esp+A0h] [ebp-74h]
  uint32_t tjsldv_dest_width; // [esp+A4h] [ebp-70h]
  int tjsldv_dest_height; // [esp+A8h] [ebp-6Ch]
  int tjsldv_dest_buffer; // [esp+ACh] [ebp-68h]
  int tjsldv_dest_pitch; // [esp+B0h] [ebp-64h]
  uint32_t tjsldv_src_width; // [esp+B4h] [ebp-60h]
  int tjsldv_src_height; // [esp+B8h] [ebp-5Ch]
  int tjsldv_src_buffer; // [esp+BCh] [ebp-58h]
  int tjsldv_src_pitch; // [esp+C0h] [ebp-54h]
  uint32_t v157; // [esp+C4h] [ebp-50h]
  int *tjslv_dest; // [esp+C8h] [ebp-4Ch]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v110 = a1;
  v11 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  if ( v11 )
  {
    ("tTJSVariant::tTJSVariant()")(&v135, v110);
    if ( FIND_AND_GET_MEMBER(L"src", v11, &v136) )
    {
      tjslv_src = ("tTJSVariant::operator iTJSDispatch2 *()")(&v136);
      sub_10001A70(tjslv_src, &tjsldv_src_width, &tjsldv_src_height, &tjsldv_src_buffer, &tjsldv_src_pitch);
    }
    if ( FIND_AND_GET_MEMBER(L"sleft", v11, &v136) )
    {
      ("tTJSVariant::operator tjs_int() const")(&v136); // not used
    }
    if ( FIND_AND_GET_MEMBER(L"stop", v11, &v136) )
    {
      ("tTJSVariant::operator tjs_int() const")(&v136); //not used
    }
    if ( !FIND_AND_GET_MEMBER(L"swidth", v11, &v136) )
    {
      tjslv_swidth = tjsldv_src_width - 1;
    }
    else
    {
      tjslv_swidth = ("tTJSVariant::operator tjs_int() const")(&v136);
    }
    v112 = tjslv_swidth;
    if ( FIND_AND_GET_MEMBER(L"sheight", v11, &v136) )
    {
      ("tTJSVariant::operator tjs_int() const")(&v136); // not used
    }
    if ( FIND_AND_GET_MEMBER(L"dest", v11, &v136) )
    {
      tjslv_dest = ("tTJSVariant::operator iTJSDispatch2 *()")(&v136);
      sub_10001A70(tjslv_dest, &tjsldv_dest_width, &tjsldv_dest_height, &tjsldv_dest_buffer, &tjsldv_dest_pitch);
    }
    if ( !FIND_AND_GET_MEMBER(L"dleft", v11, &v136) )
    {
      LODWORD(tjslv_dleft) = 0;
    }
    else
    {
      LODWORD(tjslv_dleft) = ("tTJSVariant::operator tjs_int() const")(&v136);
    }
    if ( !FIND_AND_GET_MEMBER(L"dtop", v11, &v136) )
    {
      tjslv_dtop = 0.0;
    }
    else
    {
      tjslv_dtop = COERCE_FLOAT(("tTJSVariant::operator tjs_int() const")(&v137));
    }
    v135 = tjslv_dtop;
    if ( !FIND_AND_GET_MEMBER(L"dwidth", v11, &v136) )
    {
      tjslv_dwidth = tjsldv_dest_height;
    }
    else
    {
      tjslv_dwidth = ("tTJSVariant::operator tjs_int() const")(&v137);
    }
    v131 = tjslv_dwidth;
    if ( !FIND_AND_GET_MEMBER(L"dheight", v11, &v136) )
    {
      tjslv_dheight = tjsldv_dest_buffer;
    }
    else
    {
      tjslv_dheight = ("tTJSVariant::operator tjs_int() const")(&v137);
    }
    v124 = tjslv_dheight;
    if ( !FIND_AND_GET_MEMBER(L"opa", v11, &v136) )
    {
      tjslv_opa = 255;
    }
    else
    {
      tjslv_opa = ("tTJSVariant::operator tjs_int() const")(&v137);
    }
    ("tTJSVariant::~ tTJSVariant()")(&v136);
    v30 = (double)v114;
    v31 = (double)tjslv_dwidth;
    v136 = v30 / v31;
    v32 = (double)v112;
    v33 = (double)tjslv_dheight;
    *(float *)&v139 = v32 / v33;
    v34 = v31 / v30;
    v116 = v33 / v32;
    if ( (int32_t)tjslv_dleft < 0 )
    {
      v35 = -(int32_t)tjslv_dleft;
      LODWORD(tjslv_dleft) = 0;
      v114 -= v35;
      v36 = (int64_t)((double)v35 * v34);
      tjslv_dwidth -= v36;
      HIDWORD(tjslv_dleft) += v36;
      v131 = tjslv_dwidth;
    }
    v37 = v130;
    if ( v130 < 0 )
    {
      v38 = (double)-v130;
      v39 = v112 + v130;
      v37 = 0;
      v130 = 0;
      v112 = v39;
      v40 = (int64_t)(v38 * v116);
      LODWORD(tjslv_dtop) += v40;
      tjslv_dheight -= v40;
      v135 = tjslv_dtop;
      v124 = tjslv_dheight;
    }
    if ( (int32_t)tjslv_dleft + v114 > tjsldv_src_height - 1 )
    {
      v41 = v114 - tjsldv_src_height + tjslv_dleft + 1;
      v114 = -(tjslv_dleft - tjsldv_src_height + 1);
      tjslv_dwidth -= (int64_t)((double)v41 * v34);
      v131 = tjslv_dwidth;
    }
    if ( v37 + v112 > tjsldv_src_buffer - 1 )
    {
      v42 = v112 - tjsldv_src_buffer + v37 + 1;
      v112 = -(v37 - tjsldv_src_buffer + 1);
      tjslv_dheight -= (int64_t)((double)v42 * v116);
      v124 = tjslv_dheight;
    }
    if ( tjslv_dleft < 0 )
    {
      tjslv_dwidth += HIDWORD(tjslv_dleft);
      v131 = tjslv_dwidth;
      v43 = (int64_t)((double)-HIDWORD(tjslv_dleft) * v136);
      tjslv_dleft = (uint32_t)(v43 + tjslv_dleft);
      v114 -= v43;
    }
    if ( tjslv_dtop < 0.0 )
    {
      v44 = (double)-LODWORD(tjslv_dtop);
      tjslv_dheight += LODWORD(tjslv_dtop);
      tjslv_dtop = 0.0;
      v135 = 0.0;
      v124 = tjslv_dheight;
      v45 = (int64_t)(v44 * *(float *)&v139);
      v130 = v45 + v37;
      v112 -= v45;
    }
    if ( tjslv_dwidth + HIDWORD(tjslv_dleft) > tjsldv_dest_height )
    {
      v46 = HIDWORD(tjslv_dleft) + tjslv_dwidth - tjsldv_dest_height;
      tjslv_dwidth = tjsldv_dest_height - HIDWORD(tjslv_dleft);
      v131 = tjsldv_dest_height - HIDWORD(tjslv_dleft);
      v114 -= (int64_t)((double)v46 * v136);
    }
    if ( tjslv_dheight + LODWORD(tjslv_dtop) > tjsldv_dest_buffer )
    {
      v124 = tjsldv_dest_buffer - LODWORD(tjslv_dtop);
      v112 -= (int64_t)((double)(LODWORD(tjslv_dtop) + tjslv_dheight - tjsldv_dest_buffer) * *(float *)&v139);
    }
    v47 = v157 >> 2;
    v111 = v157 >> 2;
    v48 = (v114 << 8) / tjslv_dwidth;
    v147 = tjsldv_src_pitch;
    tjslv_dest = (int *)(tjslv_dwidth + HIDWORD(tjslv_dleft));
    v49 = LODWORD(tjslv_dtop) + v124;
    v139 = (v114 << 8) / tjslv_dwidth;
    v113 = (v112 << 8) / v124;
    v50 = tjsldv_src_width >> 2;
    LODWORD(v136) = tjsldv_src_width >> 2;
    if ( tjslv_opa >= 255 )
    {
      if ( v48 > 256 || v113 > 256 )
      {
        v119 = tjsldv_dest_pitch + 4 * LODWORD(tjslv_dtop) * v50;
        v123 = v130 << 8;
        if ( SLODWORD(tjslv_dtop) < v49 )
        {
          v101 = (uint32_t)tjslv_dleft << 8;
          for (size_t i = 0; i < v124; i += 1)
          {
            v138 = v147 + 4 * v47 * (v123 >> 8);
            v102 = (uint32_t *)(v119 + 4 * HIDWORD(tjslv_dleft));
            v103 = (uint32_t)&v102[tjslv_dwidth];
            v104 = v138;
            for (; v102 < v103; v102 += 1)
            {
              v102[0] = ((uint32_t *)(v104 + 4 * (v101 >> 8)))[0];
              v101 += v139;
            }
            tjslv_dtop = v135;
            v50 = LODWORD(v136);
            v119 += 4 * v50;
            v123 += v113;
            v101 = (uint32_t)tjslv_dleft << 8;
          }
        }
      }
      else
      {
        v77 = LODWORD(tjslv_dtop) * v50;
        v78 = v130 << 8;
        v79 = tjsldv_dest_pitch + 4 * v77;
        v143 = (uint32_t *)(tjsldv_dest_pitch + 4 * v77);
        v118 = v130 << 8;
        if ( SLODWORD(tjslv_dtop) < v49 )
        {
          v80 = (uint32_t)tjslv_dleft << 8;
          for (size_t i = 0; i < v124; i += 1)
          {
            tjsldv_dest_width = v147 + 4 * v47 * (v78 >> 8);
            v81 = (uint8_t)v78 << 8;
            v82 = 65280 - v81;
            v140 = v81;
            v138 = 65280 - v81;
            v134 = (int *)(v79 + 4 * HIDWORD(tjslv_dleft));
            v122 = (uint32_t)&v134[tjslv_dwidth];
            v145 = v80;
            for (; v134 < v122; v134 += 1)
            {
              v83 = (uint32_t *)(tjsldv_dest_width + 4 * (v80 >> 8));
              v84 = 255 - (uint8_t)v80;
              v85 = v82 * v84;
              v86 = v81 * v84 >> 8;
              v87 = v81 - v86;
              v88 = v86 >> 8;
              v89 = v83[0];
              v148 = v83[1];
              v85 >>= 8;
              v144 = v87 >> 8;
              v90 = v83[v47 + 1];
              v91 = v83[v47];
              v146 = (v82 - v85) >> 8;
              v85 >>= 8;
              v92 = v88 * ((v91 >> 8) & 0xFF00FF) + v146 * ((v148 >> 8) & 0xFF00FF) + v85 * ((v89 >> 8) & 0xFF00FF);
              v93 = v85 * (v89 & 0xFF00FF);
              v94 = (v144 * ((v90 >> 8) & 0xFF00FF) + v92) & 0xFF00FF00;
              v95 = v88 * (v91 & 0xFF00FF) + v146 * (v148 & 0xFF00FF) + v93;
              v47 = v111;
              v96 = v144 * (v90 & 0xFF00FF) + v95;
              v97 = v139;
              v98 = (v96 >> 8) & 0xFF00FF | v94;
              v99 = v145;
              v134[0] = v98;
              v80 = v97 + v99;
              v145 = v80;
              v82 = v138;
              v81 = v140;
            }
            v100 = v118;
            tjslv_dwidth = v131;
            v79 = (int)&v143[LODWORD(v136)];
            v78 = v113 + v100;
            v143 += LODWORD(v136);
            v118 = v78;
            v80 = (uint32_t)tjslv_dleft << 8;
          }
          tjslv_dtop = v135;
        }
      }
    }
    else
    {
      v117 = 255 - tjslv_opa;
      if ( v48 > 256 || v113 > 256 )
      {
        v70 = tjsldv_dest_pitch + 4 * LODWORD(tjslv_dtop) * v50;
        v128 = v70;
        v121 = v130 << 8;
        if ( SLODWORD(tjslv_dtop) < v49 )
        {
          v71 = (uint32_t)tjslv_dleft << 8;
          for (size_t i = 0; i < v124; i += 1)
          {
            v138 = v147 + 4 * v47 * (v121 >> 8);
            v72 = (uint32_t *)(v70 + 4 * HIDWORD(tjslv_dleft));
            v140 = (uint32_t)&v72[tjslv_dwidth];
            v73 = v71;
            if ( v72 < &v72[tjslv_dwidth] )
            {
              v74 = v117;
              for (; (uint32_t)v72 < v140; v72 += 1)
              {
                v75 = tjslv_opa * ((*(uint32_t *)(v138 + 4 * (v73 >> 8)) >> 8) & 0xFF00FF) + v74 * ((v72[0] >> 8) & 0xFF00FF);
                v74 = v117;
                v76 = v139;
                v72[0] = ((tjslv_opa * (*(uint32_t *)(v138 + 4 * (v73 >> 8)) & 0xFF00FF) + v117 * (v72[0] & 0xFF00FF)) >> 8) & 0xFF00FF | v75 & 0xFF00FF00;
                v73 += v76;
              }
              v70 = v128;
              tjslv_dwidth = v131;
              v71 = (uint32_t)tjslv_dleft << 8;
            }
            v70 += 4 * LODWORD(v136);
            v128 = v70;
            v121 += v113;
          }
          tjslv_dtop = v135;
        }
      }
      else
      {
        v120 = tjsldv_dest_pitch + 4 * LODWORD(tjslv_dtop) * v50;
        v51 = v130 << 8;
        v132 = v130 << 8;
        if ( SLODWORD(tjslv_dtop) < v49 )
        {
          v125 = (uint32_t)tjslv_dleft << 8;
          for (size_t i = 0; i < (v49 - LODWORD(tjslv_dtop)); i += 1)
          {
            v144 = v147 + 4 * v47 * (v51 >> 8);
            v52 = tjslv_opa * (255 - (uint8_t)v51);
            tjsldv_dest_width = tjslv_opa * (uint8_t)v51;
            v53 = (int *)(v120 + 4 * HIDWORD(tjslv_dleft));
            v54 = v125;
            v148 = tjslv_opa * (255 - (uint8_t)v51);
            v138 = (uint32_t)&v53[tjslv_dwidth];
            v145 = v125;
            for (; (uint32_t)v53 < v138; v53 += 1)
            {
              v55 = (int *)(v144 + 4 * (v54 >> 8));
              v56 = 255 - (uint8_t)v54;
              v57 = v52 * (255 - (uint8_t)v54);
              v58 = v52;
              v59 = (int32_t)(tjsldv_dest_width * v56) >> 8;
              v60 = tjsldv_dest_width - v59;
              v61 = v59 >> 8;
              v62 = v55[1];
              v142 = v53[0];
              v63 = v55[0];
              v64 = (uint32_t)v55[0] >> 8;
              v146 = v60 >> 8;
              v65 = v55[v47 + 1];
              v66 = v55[v47];
              v57 >>= 8;
              v140 = (v58 - v57) >> 8;
              *v53 = ((v117 * (v142 & 0xFF00FF)
                    + v146 * (v65 & 0xFF00FF)
                    + v61 * (v66 & 0xFF00FF)
                    + v140 * (v62 & 0xFF00FF)
                    + (v57 >> 8) * (v63 & 0xFF00FF)) >> 8) & 0xFF00FF | (v117 * ((v142 >> 8) & 0xFF00FF)
                                                                       + v146 * ((v65 >> 8) & 0xFF00FF)
                                                                       + v61 * ((v66 >> 8) & 0xFF00FF)
                                                                       + v140 * ((v62 >> 8) & 0xFF00FF)
                                                                       + (v57 >> 8) * (v64 & 0xFF00FF)) & 0xFF00FF00;
              v54 = v139 + v145;
              v145 += v139;
              v52 = v148;
              v47 = v111;
            }
            tjslv_dwidth = v131;
            v51 = v132;
            v47 = v111;
          }
          v120 += 4 * LODWORD(v136);
          v51 += v113;
          v132 = v51;
          tjslv_dtop = v135;
        }
      }
    }
    //UPDATE: tjslv_dest tjslv_dleft tjslv_dtop v157 v113
  }
  return TJS_S_OK;
}

//----- (10006CA0) --------------------------------------------------------
int32_t tjsf_Vortex(double tjslv_rad, int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  int v12; // esi
  int *tjslv_src; // eax
  int v18; // edi
  int v19; // ebx
  double v20; // st6
  int v21; // esi
  int v22; // ecx
  long double tjslv_dtop; // st5
  long double v24; // st5
  double v25; // st4
  double v26; // st3
  int v27; // esi
  int64_t v28; // rax
  int v29; // ecx
  int v30; // esi
  int v31; // ST38_4
  int v32; // ebx
  int v33; // esi
  int v34; // edi
  int v35; // ST3C_4
  uint32_t *v36; // ecx
  uint32_t v37; // ST38_4
  uint32_t v38; // ebx
  int v39; // ecx
  bool v40; // sf
  uint8_t v41; // of
  int v51; // [esp+28h] [ebp-C0h]
  int v52; // [esp+2Ch] [ebp-BCh]
  uint32_t *v53; // [esp+2Ch] [ebp-BCh]
  int v54; // [esp+30h] [ebp-B8h]
  int v55; // [esp+30h] [ebp-B8h]
  int v56; // [esp+38h] [ebp-B0h]
  uint32_t * v57; // [esp+3Ch] [ebp-ACh]
  long double v58; // [esp+40h] [ebp-A8h]
  int v61; // [esp+50h] [ebp-98h]
  int v62; // [esp+54h] [ebp-94h]
  int v63; // [esp+58h] [ebp-90h]
  int v64; // [esp+5Ch] [ebp-8Ch]
  int v65; // [esp+60h] [ebp-88h]
  int v66; // [esp+6Ch] [ebp-7Ch]
  uint32_t v67; // [esp+70h] [ebp-78h]
  int *tjslv_dest; // [esp+74h] [ebp-74h]
  int v69; // [esp+78h] [ebp-70h]
  int v70; // [esp+7Ch] [ebp-6Ch]
  int v71; // [esp+80h] [ebp-68h]
  int tjsldv_src_width; // [esp+84h] [ebp-64h]
  int tjsldv_src_height; // [esp+88h] [ebp-60h]
  int tjsldv_src_buffer; // [esp+8Ch] [ebp-5Ch]
  uint32_t tjsldv_src_pitch; // [esp+90h] [ebp-58h]
  int v76; // [esp+94h] [ebp-54h]
  int tjsldv_dest_width; // [esp+98h] [ebp-50h]
  int tjsldv_dest_height; // [esp+9Ch] [ebp-4Ch]
  int tjsldv_dest_buffer; // [esp+A0h] [ebp-48h]
  uint32_t tjsldv_dest_pitch; // [esp+A4h] [ebp-44h]
  uint32_t v81; // [esp+A8h] [ebp-40h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  ("tjs_uint64 ::TVPGetTickCount()")();
  v12 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  if ( v12 )
  {
    ("tTJSVariant::tTJSVariant()")(&v65);
    if ( FIND_AND_GET_MEMBER(L"src", v12, &v65) )
    {
      tjslv_src = ("tTJSVariant::operator iTJSDispatch2 *()")(&v65);
      sub_10001A70(tjslv_src, &tjsldv_src_width, &tjsldv_src_height, &tjsldv_src_buffer, &tjsldv_src_pitch);
    }
    if ( FIND_AND_GET_MEMBER(L"dest", v12, &v65) )
    {
      tjslv_dest = ("tTJSVariant::operator iTJSDispatch2 *()")(&v65);
      sub_10001A70(tjslv_dest, &tjsldv_dest_width, &tjsldv_dest_height, &tjsldv_dest_buffer, &tjsldv_dest_pitch);
    }
    if ( !FIND_AND_GET_MEMBER(L"rad", v12, &v65) )
    {
      tjslv_rad = 0.0;
    }
    else
    {
      tjslv_rad = ("tTJSVariant::operator tTVReal() const")(&v65);
    }
    ("tTJSVariant::~ tTJSVariant()")(&v65);
    v69 = tjsldv_src_buffer;
    v81 = tjsldv_src_pitch >> 2;
    v67 = tjsldv_dest_pitch >> 2;
    v18 = tjsldv_src_width >> 1;
    v57 = tjsldv_dest_buffer;
    v61 = tjsldv_src_width >> 1;
    v62 = tjsldv_src_height >> 1;
    v52 = tjsldv_src_width;
    if ( tjsldv_src_width >= tjsldv_src_height )
      v52 = tjsldv_src_height;
    v54 = tjsldv_src_width >> 1;
    if ( v18 <= tjsldv_src_height >> 1 )
      v54 = tjsldv_src_height >> 1;
    v64 = (int64_t)((double)v54 * (double)v54);
    v19 = tjsldv_src_height - 1;
    v71 = tjsldv_src_height - 1;
    v20 = (double)(tjsldv_src_width * tjsldv_src_width) / (double)(v52 * v52);
    v21 = tjsldv_src_width - 1;
    v76 = tjsldv_src_width - 1;
    for (size_t i = 0; i < (tjsldv_src_height - 1); i += 1)
    {
      v70 = i - v62;
      v66 = (int64_t)((double)((i - v62) * (i - v62)) * v20);
      v53 = v57;
      for (size_t j = 0; j < v21; j += 1)
      {
        v56 = j - v18;
        LODWORD(v58) = v66 + v56 * v56;
        if ( SLODWORD(v58) < v64 )
        {
          tjslv_dtop = (1.0 - (double)SLODWORD(v58) / (double)v64)
              * ((1.0 - (double)SLODWORD(v58) / (double)v64)
               * (1.0 - (double)SLODWORD(v58) / (double)v64))
              * tjslv_rad;
          v58 = cos(tjslv_dtop);
          v24 = sin(tjslv_dtop);
          v25 = (double)v56;
          v26 = (double)v70;
          v27 = (int64_t)((v26 * v24 + v25 * v58 + (double)v61) * 32767.0);
          v28 = (int64_t)((v26 * v58 - v25 * v24 + (double)v62) * 32767.0);
          LODWORD(v58) = v27 >> 15;
          v29 = v28;
          HIDWORD(v28) = v28 & 0x7FFF;
          v30 = v27 & 0x7FFF;
          v31 = v30;
          LODWORD(v28) = 0x7FFF - HIDWORD(v28);
          v32 = 0x7FFF - v30;
          v33 = (0x7FFF - v30) * v28;
          v34 = v32 * HIDWORD(v28);
          v63 = v31 * (int32_t)v28 >> 22;
          v35 = v31 * HIDWORD(v28) >> 22;
          v36 = (uint32_t *)(v69 + 4 * (LODWORD(v58) + v81 * (v29 >> 15)));
          HIDWORD(v28) = v36[v81 + 1];
          v37 = v36[v81];
          LODWORD(v28) = v36[1];
          LODWORD(v58) = v36[0];
          v34 >>= 22;
          v33 >>= 22;
          v38 = v33 * ((LODWORD(v58) >> 8) & 0xFF00FF)
              + v35 * ((HIDWORD(v28) >> 8) & 0xFF00FF)
              + v34 * ((v37 >> 8) & 0xFF00FF)
              + v63 * (((uint32_t)v28 >> 8) & 0xFF00FF);
          LODWORD(v28) = v34 * (v37 & 0xFF00FF) + v63 * (v28 & 0xFF00FF);
          v18 = v61;
          v39 = v33 * (LODWORD(v58) & 0xFF00FF);
          v21 = v76;
          LODWORD(v28) = v39 + v35 * (HIDWORD(v28) & 0xFF00FF) + v28;
          v53[0] = ((uint32_t)v28 >> 8) & 0xFF00FF | v38 & 0xFF00FF00;
        }
        ++v53;
      }
      v57 += v67;
    }
    //UPDATE: tjslv_dest 0 0 tjsldv_dest_width tjsldv_dest_height
  }
  return TJS_S_OK;
}
