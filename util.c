#include <windows.h>
#include <defs.h>


//-------------------------------------------------------------------------
// Function declarations

// int __userpurge Proc(int a1, const LOGFONTA *a2, const TEXTMETRICA *a3, DWORD a4, LPARAM a5);
HANDLE __cdecl sub_10001140(HWND hWndNewOwner, UINT format);
int __cdecl sub_100011A0(LPCSTR lpMultiByteStr); // idb
IMalloc *__cdecl sub_10001210(int a1);
int __stdcall sub_10001240(HWND hWnd, int, int, LPCSTR lpMultiByteStr); // idb
BOOL __cdecl tjsgdf_get_dictionary_member(int a1, int a2, int a3);
// _BYTE *__usercall sub_100012F0(int a1, int a2);
// int __userpurge V2Link(int a1, int a2, int a3);
// signed int __usercall tjsf_enumFont(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int *a9);
// signed int __usercall tjsf_DeleteFile(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int *a8);
// signed int __usercall tjsf_CopyFile(int a1, int a2, int a3, int a4, int a5, int a6, signed int a7, int *a8);
// signed int __usercall tjsf_MoveFile(int a1, int a2, int a3, int a4, int a5, int a6, signed int a7, int *a8);
// signed int __usercall tjsf_CreateDirectory(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int *a8);
// signed int __usercall tjsf_RemoveDirectory(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int *a8);
// signed int __usercall tjsf_ExistDirectory(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int *a9);
// signed int __usercall tjsf_GetFileSize(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int *a9);
// signed int __usercall tjsf_GetFileTime(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int *a9);
signed int __cdecl tjsf_GetClipboardText(int a1, int a2, int a3, int a4, int a5);
// signed int __usercall tjsf_GetClipboardTextWithoutControlChar(int a1, int a2, int a3, int a4, int a5, int a6);
signed int __cdecl tjsf_SetClipboardText(int a1, int a2, int a3, int a4, int a5, int a6, int *a7);
// signed int __usercall tjsf_ShowWindow(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int *a9);
// signed int __usercall tjsf_IsWindowShow(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int *a8);
// signed int __usercall tjsf_GetDatetime(int a1, int a2, int a3, int a4, int a5, int a6, int a7);
// signed int __usercall tjsf_IsActiveWindow(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int *a8);
signed int __cdecl tjsf_CalcPCID(int a1, int a2, int a3, int a4, int a5, int a6, int *a7);
// signed int __usercall tjsf_SelectDirectory(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int *a11);

//----- (10001000) --------------------------------------------------------
int __userpurge Proc(int a1, const LOGFONTA *a2, const TEXTMETRICA *a3, DWORD a4, LPARAM a5)
{
  int *v6; // ST14_4
  int v7; // edx
  int v8; // ST00_4
  int v10; // esi
  char v15; // [esp+10h] [ebp-18h]
  char v16; // [esp+14h] [ebp-14h]

  if ( (!(*(_BYTE *)a5 & 1) || !(a2->lfPitchAndFamily & 2))
    && a2->lfCharSet == -128
    && a2->lfFaceName[0] != 64
    && a4 & 4 )
  {
    ("tTJSVariant::tTJSVariant()")(&v15);
    v6 = *(int **)(a5 + 4);
    v7 = *v6;
    v8 = *(_DWORD *)(a5 + 4);
    (*(void (__cdecl **)(int, _DWORD, wchar_t *, _DWORD, char *, int *))(v7 + 16))(v8, 0, L"count", 0, &v15, v6);
    v10 = ("tTJSVariant::operator tjs_int() const")(&v15, a1);
    ("tTJSVariant & tTJSVariant::operator =(const tjs_nchar *)")(&v16);
    (*(void (__cdecl **)(_DWORD, signed int, int, char *, _DWORD))(**(_DWORD **)(a5 + 4) + 28))(
      *(_DWORD *)(a5 + 4),
      512,
      v10,
      &v15,
      *(_DWORD *)(a5 + 4));
    ("tTJSVariant::~ tTJSVariant()")(&v15);
  }
  return 1;
}

//----- (10001140) --------------------------------------------------------
HANDLE __cdecl sub_10001140(HWND hWndNewOwner, UINT format)
{
  signed int v2; // esi
  HANDLE result; // eax

  if ( IsClipboardFormatAvailable(format) )
  {
    for (v2 = 0; v2 < 50; v2 += 1)
    {
      if ( OpenClipboard(hWndNewOwner) )
      {
        result = GetClipboardData(format);
        if ( result )
          return result;
        CloseClipboard();
      }
      Sleep(0xAu);
    }
  }
  return 0;
}

//----- (100011A0) --------------------------------------------------------
int __cdecl sub_100011A0(LPCSTR lpMultiByteStr)
{
  int result; // eax
  char v2; // [esp+18h] [ebp-214h]
  int v3; // [esp+1Ch] [ebp-210h]
  IShellFolder *ppshf; // [esp+20h] [ebp-20Ch]
  WCHAR WideCharStr; // [esp+24h] [ebp-208h]

  if ( SHGetDesktopFolder(&ppshf) < 0
    || (MultiByteToWideChar(0, 1u, lpMultiByteStr, -1, &WideCharStr, 260),
        ppshf->lpVtbl->ParseDisplayName(ppshf, 0, 0, &WideCharStr, (ULONG *)&v2, (LPITEMIDLIST *)&v3, (ULONG *)&v2) < 0) )
  {
    result = 0;
  }
  else
  {
    result = v3;
  }
  return result;
}

//----- (10001210) --------------------------------------------------------
IMalloc *__cdecl sub_10001210(int a1)
{
  IMalloc *v1; // ecx
  IMalloc *result; // eax
  IMalloc *ppMalloc; // [esp+0h] [ebp-4h]

  ppMalloc = v1;
  result = (IMalloc *)SHGetMalloc(&ppMalloc);
  if ( !result )
  {
    result = ppMalloc;
    if ( ppMalloc )
      result = (IMalloc *)((int (__stdcall *)(IMalloc *, int))ppMalloc->lpVtbl->Free)(ppMalloc, a1);
  }
  return result;
}

//----- (10001240) --------------------------------------------------------
int __stdcall sub_10001240(HWND hWnd, int a2, int a3, LPCSTR lpMultiByteStr)
{
  LPARAM v4; // eax
  int v5; // esi

  if ( a2 != 1 )
    return 0;
  v4 = sub_100011A0(lpMultiByteStr);
  v5 = v4;
  if ( v4 )
  {
    SendMessageA(hWnd, 0x466u, 0, v4);
    sub_10001210(v5);
  }
  SetWindowPos(hWnd, HWND_MESSAGE|0x2, 0, 0, 0, 0, 3u);
  return 0;
}

//----- (100012A0) --------------------------------------------------------
BOOL __cdecl tjsgdf_get_dictionary_member(int a1, int a2, int a3)
{
  if ( !a1 )
    return 0;
  if ( (*(int (__cdecl **)(int, _DWORD, int, _DWORD, int))(*(_DWORD *)a1 + 64))(a1, 0, a2, 0, a1) == 2 )
    return 0;
  return (*(int (__cdecl **)(int, _DWORD, int, _DWORD, int, int))(*(_DWORD *)a1 + 16))(a1, 0, a2, 0, a3, a1) >= 0;
}

//----- (100012F0) --------------------------------------------------------
_BYTE *__usercall sub_100012F0(int a1, int a2)
{
  wchar_t *v6; // eax
  _BYTE *v7; // esi
  int v11; // [esp+1Ch] [ebp-20h]
  char v12; // [esp+20h] [ebp-1Ch]
  char v13; // [esp+24h] [ebp-18h]
  char v14; // [esp+28h] [ebp-14h]

  ("tTJSString::tTJSString(const tTJSVariant &)")(&v11, a2);
  ("void ::TVPGetLocalName(ttstr &)")(&v11);
  ("tTJSVariant::tTJSVariant(const tTJSString &)")(&v13, &v11);
  v6 = (wchar_t *)("const tjs_char * tTJSVariant::GetString() const")(&v13, &v12);
  v7 = sub_10004310(v6, a1);
  ("tTJSVariant::~ tTJSVariant()")(&v14);
  ("tTJSString::~ tTJSString()")(&v12);
  return v7;
}

//----- (10002180) --------------------------------------------------------
signed int __usercall tjsf_enumFont(int a1, int a2, int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  int v12; // esi
  int v15; // eax
  int v16; // esi
  HWND v19; // eax
  HDC v20; // eax
  LPARAM lParam; // [esp+Ch] [ebp-5Ch]
  int v26; // [esp+10h] [ebp-58h]
  char v27; // [esp+14h] [ebp-54h]
  struct tagLOGFONTA Logfont; // [esp+20h] [ebp-48h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v12 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(param[0]);
  ("tTJSVariant::tTJSVariant()")(&v27);
  if ( !tjsgdf_get_dictionary_member(v12, (int)L"showFixedPitchOnlyInFontSelector", (int)&v27) )
    v15 = 0;
  else {
    v15 = ("tTJSVariant::operator tjs_int() const")(&v27);
    if ( v15 != 1 )
      v15 = 0;
  }
  v16 = param[1];
  v26 = v15;
  v26 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(v16);
  v19 = (HWND)("HWND ::TVPGetApplicationWindowHandle()")();
  Logfont.lfCharSet = 1;
  Logfont.lfFaceName[0] = 0;
  Logfont.lfPitchAndFamily = 0;
  v20 = GetDC(v19);
  EnumFontFamiliesExA(v20, &Logfont, (FONTENUMPROCA)Proc, (LPARAM)&lParam, 0);
  ("tTJSVariant::~ tTJSVariant()")(&v27);
  return TJS_S_OK;
}

//----- (10002320) --------------------------------------------------------
signed int __usercall tjsf_DeleteFile(int a1, int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  CHAR *v9; // esi

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v9 = sub_100012F0(a1, param[0]);
  DeleteFileA(v9);
  free(v9);
  return TJS_S_OK;
}

//----- (10002370) --------------------------------------------------------
signed int __usercall tjsf_CopyFile(int a1, int this, int flag, int membername, int hint, int result, signed int numparams, int *param)
{
  CHAR *v9; // esi
  CHAR *v10; // edi

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( numparams < 2 )
    return TJS_E_BADPARAMCOUNT;
  v9 = sub_100012F0(a1, param[0]);
  v10 = sub_100012F0((int)v9, param[1]);
  CopyFileA(v9, v10, 1);
  free(v10);
  free(v9);
  return TJS_S_OK;
}

//----- (100023D0) --------------------------------------------------------
signed int __usercall tjsf_MoveFile(int a1, int this, int flag, int membername, int hint, int result, signed int numparams, int *param)
{
  CHAR *v9; // esi
  CHAR *v10; // edi

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( numparams < 2 )
    return TJS_E_BADPARAMCOUNT;
  v9 = sub_100012F0(a1, param[0]);
  v10 = sub_100012F0((int)v9, param[1]);
  MoveFileA(v9, v10);
  free(v10);
  free(v9);
  return TJS_S_OK;
}

//----- (10002430) --------------------------------------------------------
signed int __usercall tjsf_CreateDirectory(int a1, int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  CHAR *v9; // esi

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v9 = sub_100012F0(a1, param[0]);
  CreateDirectoryA(v9, 0);
  free(v9);
  return TJS_S_OK;
}

//----- (10002480) --------------------------------------------------------
signed int __usercall tjsf_RemoveDirectory(int a1, int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  CHAR *v9; // esi

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v9 = sub_100012F0(a1, param[0]);
  RemoveDirectoryA(v9);
  free(v9);
  return TJS_S_OK;
}

//----- (100024D0) --------------------------------------------------------
signed int __usercall tjsf_ExistDirectory(int a1, int a2, int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  CHAR *v10; // esi
  DWORD v11; // edi

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v10 = sub_100012F0(a2, param[0]);
  v11 = GetFileAttributesA(v10);
  free(v10);
  if ( v11 == -1 )
  {
    ("tTJSVariant & tTJSVariant::operator =(const tTVInteger)")(result, 0, 0);
    return TJS_S_OK;
  }
  if ( (v11 & 0x10) != 16 )
  {
    ("tTJSVariant & tTJSVariant::operator =(const tTVInteger)")(result, 0, 0);
    return TJS_S_OK;
  }
  ("tTJSVariant & tTJSVariant::operator =(const tTVInteger)")(result, 1, 0);
  return TJS_S_OK;
}

//----- (10002580) --------------------------------------------------------
signed int __usercall tjsf_GetFileSize(int a1, int a2, int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  signed int result; // eax
  const CHAR *v10; // eax
  HANDLE v12; // eax
  DWORD v14; // edi

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v10 = sub_100012F0(a2, param[0]);
  v12 = CreateFileA(v10, 0x80000000, 1u, 0, 3u, 0x80u, 0);
  if ( v12 == (HANDLE)-1 )
  {
    ("tTJSVariant & tTJSVariant::operator =(const tTVInteger)")(result, -1, -1);
    free(v10);
  }
  else
  {
    v14 = GetFileSize(v12, 0);
    ("tTJSVariant & tTJSVariant::operator =(const tTVInteger)")(result, v14, 0);
    CloseHandle(v12);
    free(v10);
  }
  return TJS_S_OK;
}

//----- (10002650) --------------------------------------------------------
signed int __usercall tjsf_GetFileTime(int a1, int a2, int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  signed int result; // eax
  const CHAR *v10; // eax
  HANDLE v12; // eax
  int v14; // edi
  int v15; // ebx
  struct _FILETIME LastWriteTime; // [esp+0h] [ebp-8h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v10 = sub_100012F0(a2, param[0]);
  v12 = CreateFileA(v10, 0x80000000, 1u, 0, 3u, 0x80u, 0);
  if ( v12 == (HANDLE)-1 )
  {
    ("tTJSVariant & tTJSVariant::operator =(const tTVInteger)")(result, -1, -1);
    free(v10);
  }
  else
  {
    GetFileTime(v12, 0, 0, &LastWriteTime);
    v14 = *(unsigned __int64 *)&LastWriteTime / 0x989680 + 1240396484;
    v15 = (*(unsigned __int64 *)&LastWriteTime / 0x989680 - 11644505404ll) >> 32;
    CloseHandle(v12);
    ("tTJSVariant & tTJSVariant::operator =(const tTVInteger)")(result, v14, v15);
    free(v10);
  }
  return TJS_S_OK;
}

//----- (10002760) --------------------------------------------------------
signed int __cdecl tjsf_GetClipboardText(int this, int flag, int membername, int hint, int result)
{
  signed int result; // eax
  HWND v7; // eax
  HANDLE v8; // edi
  LPVOID v9; // esi
  char v14; // [esp+10h] [ebp-18h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  v7 = (HWND)("HWND ::TVPGetApplicationWindowHandle()")();
  v8 = sub_10001140(v7, 1u);
  v9 = GlobalLock(v8);
  if ( v9 )
  {
    ("tTJSVariant::tTJSVariant(const tjs_nchar *)")(&v14, v9);
    ("tTJSVariant & tTJSVariant::operator =(const tTJSVariant &)")(result, &v14);
    GlobalUnlock(v8);
    CloseClipboard();
    ("tTJSVariant::~ tTJSVariant()")(&v14);
  }
  else
  {
    CloseClipboard();
  }
  return TJS_S_OK;
}

//----- (10002890) --------------------------------------------------------
signed int __usercall tjsf_GetClipboardTextWithoutControlChar(int a1, int this, int flag, int membername, int hint, int result)
{
  signed int result; // eax
  HWND v8; // eax
  HANDLE v9; // eax
  const char *v11; // eax
  size_t v13; // esi
  char *v14; // edi
  char v15; // al
  signed int v16; // edx
  char v17; // al
  int i; // ecx

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  v8 = (HWND)("HWND ::TVPGetApplicationWindowHandle()")();
  v9 = sub_10001140(v8, 1u);
  v11 = (const char *)GlobalLock(v9);
  if ( v11 )
  {
    v13 = strlen(v11) + 1;
    v14 = (char *)malloc(v13);
    strncpy(v14, v11, v13);
    v15 = *v14;
    v16 = 0;
    if ( (unsigned __int8)*v14 < 0x20u && v15 != 10 && v15 != 13 || v15 == 127 )
      v16 = -1;
    v17 = v14[1];
    for ( i = (int)(v14 + 1); v17; ++i )
    {
      if ( ((unsigned __int8)v17 >= 0x20u || v17 == 10 || v17 == 13) && v17 != 127 || *(_BYTE *)(i - 1) >= 0x80u )
        *(_BYTE *)(v16 + i) = v17;
      else
        --v16;
      v17 = *(_BYTE *)(i + 1);
    }
    *(_BYTE *)(v16 + i) = 0;
    ("tTJSVariant::tTJSVariant(const tjs_nchar *)")(&v24, v14);
    free(v14);
    ("tTJSVariant & tTJSVariant::operator =(const tTJSVariant &)")(result, &v24);
    GlobalUnlock(v9);
    CloseClipboard();
    ("tTJSVariant::~ tTJSVariant()")(&v24);
  }
  else
  {
    CloseClipboard();
  }
  return TJS_S_OK;
}

//----- (10002A30) --------------------------------------------------------
signed int __cdecl tjsf_SetClipboardText(int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  int v8; // esi
  int v10; // esi
  int v12; // esi
  wchar_t *v15; // eax
  char *v16; // edi
  HGLOBAL v17; // eax
  signed int i; // edi
  HWND v24; // eax

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v8 = param[0];
  v10 = ("tTJSVariantString * tTJSVariant::AsStringNoAddRef() const")(v8);
  v12 = ("tTJSVariantString::operator const tjs_char *() const")(v10);
  ("tTJSString::tTJSString(const tjs_char *)")(&membername, v12);
  v15 = (wchar_t *)("const tjs_char * tTJSString::c_str() const")(&membername);
  v16 = sub_10004310(v15, (int)&numparams);
  v17 = GlobalAlloc(0x42u, numparams + 1);
  if ( v17 )
  {
    v20 = (char *)GlobalLock(v17);
    if ( v20 )
    {
      strncpy(v20, v16, numparams + 1);
      free(v16);
      GlobalUnlock(v17);
      for ( i = 500; ; i -= 10 )
      {
        v24 = (HWND)("HWND ::TVPGetApplicationWindowHandle()")();
        if ( OpenClipboard(v24) )
          break;
        if ( i < 0 )
        {
          CloseClipboard();
          GlobalFree(v17);
          ("tTJSString::~ tTJSString()")(&membername);
          return TJS_E_FAIL;
        }
        Sleep(0xAu);
      }
      if ( !EmptyClipboard() )
      {
        CloseClipboard();
        GlobalFree(v17);
        ("tTJSString::~ tTJSString()")(&membername);
        return TJS_E_FAIL;
      }
      if ( SetClipboardData(1u, v17) )
      {
        CloseClipboard();
        ("tTJSString::~ tTJSString()")(&membername);
        return TJS_S_OK;
      }
      CloseClipboard();
      GlobalFree(v17);
      ("tTJSString::~ tTJSString()")(&membername);
      return TJS_E_FAIL;
    }
    else
    {
      GlobalFree(v17);
      ("tTJSString::~ tTJSString()")(&membername);
      return TJS_E_FAIL;
    }
    ("tTJSString::~ tTJSString()")(&membername);
    return TJS_E_FAIL;
  }
  ("tTJSString::~ tTJSString()")(&membername);
  return TJS_E_FAIL;
}

//----- (10002D30) --------------------------------------------------------
signed int __usercall tjsf_ShowWindow(int a1, int a2, int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  int v11; // esi
  int v12; // esi
  int v15; // eax
  int v16; // edi
  HWND v17; // esi
  int v19; // eax
  int v23; // [esp+10h] [ebp-18h]
  char v24; // [esp+18h] [ebp-10h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v11 = param[0];
  v12 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(v11);
  ("tTJSVariant::tTJSVariant()")(&v24);
  tjsgdf_get_dictionary_member(v12, (int)L"HWND", (int)&v24);
  v15 = ("tTVInteger tTJSVariant::AsInteger() const")();
  v16 = param[1];
  v17 = (HWND)v15;
  v19 = ("tTVInteger tTJSVariant::AsInteger() const")();
  ShowWindow(v17, v19);
  ("tTJSVariant::~ tTJSVariant()")(&v23);
  return TJS_S_OK;
}

//----- (10002E70) --------------------------------------------------------
signed int __usercall tjsf_IsWindowShow(int a1, int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  int v9; // esi
  int v11; // esi
  HWND v14; // esi
  int v19; // [esp+10h] [ebp-1Ch]
  int v20; // [esp+14h] [ebp-18h]
  char v21; // [esp+18h] [ebp-14h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v19 = a1;
  v9 = param[0];
  v11 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(v9, v19);
  ("tTJSVariant::tTJSVariant()")(&v21);
  tjsgdf_get_dictionary_member(v11, (int)L"HWND", (int)&v21);
  v14 = (HWND)("tTVInteger tTJSVariant::AsInteger() const")(&v21);
  if ( IsIconic(v14) )
  {
    ("tTJSVariant & tTJSVariant::operator =(tjs_int32)")(numparams);
  }
  else
  {
    if ( !IsZoomed(v14) )
    {
      if ( IsWindowVisible(v14) == 0 )
      {
        ("tTJSVariant & tTJSVariant::operator =(tjs_int32)")(numparams);
        ("tTJSVariant::~ tTJSVariant()")(&v20);
        return TJS_S_OK;
      }
    }
    ("tTJSVariant & tTJSVariant::operator =(tjs_int32)")(numparams);
  }
  ("tTJSVariant::~ tTJSVariant()")(&v20);
  return TJS_S_OK;
}

//----- (10003030) --------------------------------------------------------
signed int __usercall tjsf_GetDatetime(int a1, int a2, int this, int flag, int membername, int hint, int result)
{
  int v13; // esi
  int v16; // edi
  int v18; // edi
  int v20; // edi
  int v22; // edi
  int v24; // edi
  int v26; // edi
  int v28; // edi
  int v29; // edi
  char *v34; // [esp+68h] [ebp-48h]
  int v35; // [esp+6Ch] [ebp-44h]
  int v36; // [esp+70h] [ebp-40h]
  char v37; // [esp+78h] [ebp-38h]
  char v38; // [esp+7Ch] [ebp-34h]
  char v39; // [esp+84h] [ebp-2Ch]
  char v40; // [esp+88h] [ebp-28h]
  char v41; // [esp+8Ch] [ebp-24h]
  struct _SYSTEMTIME SystemTime; // [esp+9Ch] [ebp-14h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  ("tTJSVariant::tTJSVariant()")(&v38);
  ("tTJSString::tTJSString(const tjs_char *)")(&membername, L"%[]");
  ("void ::TVPExecuteExpression(const ttstr &,tTJSVariant *)")(&membername, &v38);
  ("tTJSString::~ tTJSString()")(&membername);
  v13 = ("iTJSDispatch2 * tTJSVariant::AsObject() const")(&v38, a1, a2);
  ("tTJSVariant::~ tTJSVariant()")(&v39);
  GetLocalTime(&SystemTime);
  v16 = SystemTime.wYear;
  ("tTJSVariant::tTJSVariant(tjs_int32)")(&v37, v16);
  (*(void (__cdecl **)(int, signed int, wchar_t *, _DWORD, char *, int))(*(_DWORD *)v13 + 24))(
    v13,
    512,
    L"year",
    0,
    &v37,
    v13);
  v18 = SystemTime.wMonth;
  ("tTJSVariant & tTJSVariant::operator =(tjs_int32)")(&v37, v18);
  (*(void (__cdecl **)(int, signed int, wchar_t *, _DWORD, char *, int))(*(_DWORD *)v13 + 24))(
    v13,
    512,
    L"mon",
    0,
    &v37,
    v13);
  v20 = SystemTime.wDayOfWeek;
  ("tTJSVariant & tTJSVariant::operator =(tjs_int32)")(&v37, v20);
  (*(void (__cdecl **)(int, signed int, wchar_t *, _DWORD, char *, int))(*(_DWORD *)v13 + 24))(
    v13,
    512,
    L"wday",
    0,
    &v37,
    v13);
  v22 = SystemTime.wDay;
  ("tTJSVariant & tTJSVariant::operator =(tjs_int32)")(&v37, v22);
  (*(void (__cdecl **)(int, signed int, wchar_t *, _DWORD, char *, int))(*(_DWORD *)v13 + 24))(
    v13,
    512,
    L"day",
    0,
    &v37,
    v13);
  v24 = SystemTime.wHour;
  ("tTJSVariant & tTJSVariant::operator =(tjs_int32)")(&v37, v24);
  (*(void (__cdecl **)(int, signed int, wchar_t *, _DWORD, char *, int))(*(_DWORD *)v13 + 24))(
    v13,
    512,
    L"hour",
    0,
    &v37,
    v13);
  v26 = SystemTime.wMinute;
  ("tTJSVariant & tTJSVariant::operator =(tjs_int32)")(&v37, v26);
  (*(void (__cdecl **)(int, signed int, wchar_t *, _DWORD, char *, int))(*(_DWORD *)v13 + 24))(
    v13,
    512,
    L"minute",
    0,
    &v37,
    v13);
  v28 = SystemTime.wSecond;
  v35 = v28;
  v34 = &v37;
  ("tTJSVariant & tTJSVariant::operator =(tjs_int32)")();
  (*(void (__cdecl **)(int, signed int, wchar_t *, _DWORD, int *, int))(*(_DWORD *)v13 + 24))(
    v13,
    512,
    L"second",
    0,
    &v36,
    v13);
  v29 = result;
  if ( result )
  {
    ("tTJSVariant::tTJSVariant(iTJSDispatch2 *,iTJSDispatch2 *)")(&v40, v13, v13, v34, v35);
    ("tTJSVariant & tTJSVariant::operator =(const tTJSVariant &)")(v29);
    ("tTJSVariant::~ tTJSVariant()")(&v41);
  }
  ("tTJSVariant::~ tTJSVariant()")(&v36);
  return TJS_S_OK;
}

//----- (10003420) --------------------------------------------------------
signed int __usercall tjsf_IsActiveWindow(int a1, int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  int v9; // esi
  int v11; // esi
  HWND v14; // esi
  int v19; // [esp+10h] [ebp-18h]
  char v20; // [esp+14h] [ebp-14h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v9 = param[0];
  v11 = ("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(v9);
  ("tTJSVariant::tTJSVariant()")(&v20);
  if ( tjsgdf_get_dictionary_member(v11, (int)L"HWND", (int)&v20) )
  {
    v14 = (HWND)("tTVInteger tTJSVariant::AsInteger() const")(&v20);
    if ( GetActiveWindow() != v14 )
    {
      ("tTJSVariant & tTJSVariant::operator =(tjs_int32)")(numparams);
      ("tTJSVariant::~ tTJSVariant()")(&v19);
      return TJS_S_OK;
    }
  }
  ("tTJSVariant & tTJSVariant::operator =(tjs_int32)")(numparams);
  ("tTJSVariant::~ tTJSVariant()")(&v19);
  return TJS_S_OK;
}

//----- (100035C0) --------------------------------------------------------
signed int __cdecl tjsf_CalcPCID(int this, int flag, int membername, int hint, int result, int numparams, int *param)
{
  signed int result; // eax
  int v9; // edi

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  v9 = param[0];
  if ( (("tTJSVariantType tTJSVariant::Type()")(v9) != 1) || (("iTJSDispatch2 * tTJSVariant::AsObjectNoAddRef() const")(v9)) )
  {
    if ( !("tTJSVariantType tTJSVariant::Type()")(v9) )
    {
      ("tTJSVariant & tTJSVariant::operator =(const tjs_char *)")(result, L"PCID");
    }
    result = 0;
  }
  else
  {
    ("void ::TVPThrowExceptionMessage(const tjs_char *)")(L"null");
    result = -1;
  }
  return result;
}

//----- (100036B0) --------------------------------------------------------
signed int __usercall tjsf_SelectDirectory(int a1, int a2, int this, int flag, int membername, int hint, int result, int numparams, int param, int a10, int *a11)
{
  const CHAR *v14; // eax
  int v15; // esi
  int v17; // esi
  const WCHAR *v22; // esi
  int v23; // eax
  const ITEMIDLIST *v24; // esi
  char v30; // [esp+34h] [ebp-540h]
  char v31; // [esp+38h] [ebp-53Ch]
  CHAR MultiByteStr[4]; // [esp+3Ch] [ebp-538h]
  struct _browseinfoA bi; // [esp+40h] [ebp-534h]
  CHAR pszPath; // [esp+60h] [ebp-514h]
  char v35; // [esp+164h] [ebp-410h]
  int cbMultiByte; // [esp+168h] [ebp-40Ch]
  char v37; // [esp+16Ch] [ebp-408h]

  if ( membername )
    return TJS_E_MEMBERNOTFOUND;
  if ( !numparams )
    return TJS_E_BADPARAMCOUNT;
  ("tTJSString::tTJSString()")(&v31);
  memset(&bi.pszDisplayName, 0, sizeof(struct _browseinfoA));
  bi.pidlRoot = 0;
  bi.pszDisplayName = (LPSTR)("HWND ::TVPGetApplicationWindowHandle()")();
  if ( numparams >= 3 )
  {
    v14 = sub_100012F0(numparams, a11[2]);
    bi.pidlRoot = (LPCITEMIDLIST)sub_100011A0(v14);
  }
  v15 = *a11;
  bi.lpszTitle = (LPCSTR)bi.pidlRoot;
  bi.ulFlags = 0;
  v17 = ("tTJSVariantString * tTJSVariant::AsStringNoAddRef() const")(v15);
  ("tTJSString::tTJSString(tTJSVariantString *)")(MultiByteStr, v17);
  ("tTJSString & tTJSString::operator =(const tTJSString &)")(&bi, MultiByteStr);
  ("tTJSString::~ tTJSString()")(MultiByteStr);
  memset(&v37, 0, 0x400u);
  v22 = (const WCHAR *)("const tjs_char * tTJSString::c_str() const")(&bi);
  v23 = ("tjs_int tTJSString::length() const")();
  WideCharToMultiByte(0, 0, v22, v23, MultiByteStr, (int)&cbMultiByte, (LPCSTR)0x400, 0);
  bi.lpszTitle = &v35;
  bi.ulFlags = 1;
  if ( numparams < 2 )
    pszPath = 0;
  else
    strcpy(&pszPath, sub_100012F0((int)v22, a11[1]));
  bi.lpfn = (BFFCALLBACK)sub_10001240;
  bi.lParam = (LPARAM)&pszPath;
  v24 = SHBrowseForFolderA(&bi);
  if ( v24 )
  {
    if ( SHGetPathFromIDListA(v24, &pszPath) != 1 )
      pszPath = 0;
    sub_10001210((int)v24);
  }
  else
  {
    pszPath = 0;
  }
  sub_10001210(*(int *)MultiByteStr);
  ("tTJSString::tTJSString(const tjs_nchar *)")(&v30, &pszPath);
  ("tTJSVariant & tTJSVariant::operator =(const tTJSString &)")(result, &v30);
  ("tTJSString::~ tTJSString()")(&v30);
  ("tTJSString::~ tTJSString()")(&v31);
  return TJS_S_OK;
}
